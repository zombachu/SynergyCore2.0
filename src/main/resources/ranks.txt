&7&lPrimary ranks
&8[&7A&8] &9Admin&7 - &rWe don't do things.
&8[&7H&8] &bHelper&7 - &rWe do things.
&8[&7T&8] &3Trusted&7 - &rBe extremely active and trusted by staff.
&8[&7D&8] &6Devoted&7 - &rContinue to be active.
&8[&7M&8] &7Member&7 - &rBuild on your plot and do &b/apply&7.
&8[&7G&8] &8Guest&7 - &rThe default rank.

&7&lDonator ranks
&8[&a$&8] &aSubscription Donator&7 - &rDonate at least $5 and be on a subscription.
&8[&2$&8] &2Full Donator&7 - &rDonate at least $25.
&8[&2$&8] &2Funder&7 - &rSupported our server while it was in beta.

&7&lOther ranks
&8[&5R&8] &3Retired&7 - &rThey used to do things or not do things.

&7For more information, check out our website: &bhttps://synergyserver.net/help/ranks