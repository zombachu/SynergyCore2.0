package net.synergyserver.synergycore;

import net.synergyserver.synergycore.configs.Message;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a ChatColor. Most code taken from Bukkit's implementation.
 * Should be used instead of Bukkit's implementation for its compatibility with messages.yml remapping.
 */
public enum ChatColor {
    BLACK('0', "v0"),
    DARK_BLUE('1', "v1"),
    DARK_GREEN('2', "v2"),
    DARK_AQUA('3', "v3"),
    DARK_RED('4', "v4"),
    DARK_PURPLE('5', "v5"),
    GOLD('6', "v6"),
    GRAY('7', "v7"),
    DARK_GRAY('8', "v8"),
    BLUE('9', "v9"),
    GREEN('a', "va"),
    AQUA('b', "vb"),
    RED('c', "vc"),
    LIGHT_PURPLE('d', "vd"),
    YELLOW('e', "ve"),
    WHITE('f', "vf"),
    MAGIC('k'),
    BOLD('l'),
    STRIKETHROUGH('m'),
    UNDERLINE('n'),
    ITALIC('o'),
    RESET('r', "vr");

    private final char code;
    private final boolean isFormat;
    private final String key;

    private static final Map<Character, ChatColor> BY_CHAR = new HashMap<>();
    static {
        for (ChatColor color : values()) {
            BY_CHAR.put(color.code, color);
        }
    }

    ChatColor(char code) {
        this(code, null, true);
    }

    ChatColor(char code, String key) {
        this(code, key, false);
    }

    ChatColor(char code, String key, boolean isFormat) {
        this.code = code;
        this.key = key;
        this.isFormat = isFormat;
    }

    public String toString() {
        if (key == null) {
            return "§" + code;
        }
        return Message.getColor(key);
    }

    public boolean isColor() {
        return !isFormat && this != RESET;
    }

    public static String stripColor(String input) {
        return org.bukkit.ChatColor.stripColor(input);
    }

    public static String getLastColors(String input) {
        // Note: Paper handles hex colors properly with this method but Spigot does not
        return org.bukkit.ChatColor.getLastColors(input);
    }

    public static ChatColor getByChar(char code) {
        return BY_CHAR.get(code);
    }

    public net.md_5.bungee.api.ChatColor toBungeeColor() {
        return net.md_5.bungee.api.ChatColor.of(toHex());
    }

    public String toHex() {
        return Message.getColorRaw(key).substring(1);
    }

    public static String convertToVanillaColors(String message) {
        // Replaces the <§x§1§2§3§4§5>§6 part of a hex code sequence. Is assumed the guidelines in message.yml are
        // being followed, so the remaining §6 corresponds to the appropriate vanilla code.
        return message.replaceAll("(?i)(§x(?:§[A-F0-9]){5})(?=§[A-F0-9])", "");
    }
}
