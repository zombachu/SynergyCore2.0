package net.synergyserver.synergycore;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import dev.morphia.query.Query;
import net.synergyserver.synergycore.commands.WarpCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataEntity;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

/**
 * Represents a location that players can teleport to.
 */
@Entity(value = "warps")
public class Warp implements DataEntity {

    @Id
    private ObjectId id;
    @Property("n")
    private String name;
    @Property("o")
    private UUID owner;
    @Property("l")
    private SerializableLocation location;
    @Property("p")
    private PrivacyLevel privacy;
    @Property("w")
    private HashSet<UUID> whitelist;
    @Property("b")
    private HashSet<UUID> blacklist;
    @Property("u")
    private int uses;

    @Transient
    private DataManager dm = DataManager.getInstance();


    /**
     * Default constructor for Morphia.
     */
    private Warp() {}

    public Warp(String name, UUID owner, Location location) {
        this(name, owner, location, PrivacyLevel.PRIVATE);
    }

    /**
     * Creates a new <code>Warp</code> with the given parameters.
     *
     * @param name The name of the warp.
     * @param owner The UUID of the warp's owner.
     * @param location The location of the warp.
     * @param privacy The privacy level of the warp.
     */
    public Warp(String name, UUID owner, Location location, PrivacyLevel privacy) {
        this.id = new ObjectId();
        this.name = name;
        this.owner = owner;
        this.location = new SerializableLocation(location);
        this.privacy = privacy;
        this.whitelist = new HashSet<>();
        this.blacklist = new HashSet<>();
        this.uses = 0;
    }

    @Override
    public ObjectId getID() {
        return id;
    }

    /**
     * Gets the name of the warp.
     *
     * @return The name of the warp.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the UUID of the owner of this warp.
     *
     * @return The owner of this warp.
     */
    public UUID getOwner() {
        return owner;
    }

    /**
     * Gets the formatted "full" name of this warp, which is in the format ownerName#warpName.
     *
     * @return The full name of this warp.
     */
    public String getFullName() {
        return PlayerUtil.getName(owner) + "#" + name;
    }

    /**
     * Gets the location of this warp.
     *
     * @return The location of this warp.
     */
    public Location getLocation() {
        return location.getLocation();
    }

    /**
     * Sets the location of this warp.
     *
     * @param location The new location of this warp.
     */
    public void setLocation(Location location) {
        this.location.setLocation(location);
        dm.updateField(this, Warp.class, "l", this.location);
    }

    /**
     * Gets the <code>SerializableLocation</code> of this warp.
     *
     * @return The <code>SerializableLocation</code> of this warp.
     */
    public SerializableLocation getSerializableLocation() {
        return location;
    }

    /**
     * Gets the privacy level of this warp.
     *
     * @return The <code>PrivacyLevel</code> of this warp.
     */
    public PrivacyLevel getPrivacy() {
        return privacy;
    }

    /**
     * Sets the privacy level of this warp.
     *
     * @param privacy The new <code>PrivacyLevel</code> of this warp.
     */
    public void setPrivacy(PrivacyLevel privacy) {
        this.privacy = privacy;
        dm.updateField(this, Warp.class, "p", privacy);
    }

    /**
     * Gets the set of the UUIDs of players that can access this warp when the
     * <code>privacy</code> is set to <code>PrivacyLevel.PRIVATE</code>.
     *
     * @return The set of whitelisted players.
     */
    public HashSet<UUID> getWhitelist() {
        if (whitelist == null) {
            whitelist = new HashSet<>();
        }
        return whitelist;
    }

    /**
     * Sets the UUIDs of players that can access this warp when the
     * <code>privacy</code> is set to <code>PrivacyLevel.PRIVATE</code>.
     *
     * @param whitelist The new set of whitelisted players.
     */
    public void setWhitelist(HashSet<UUID> whitelist) {
        this.whitelist = whitelist;
        dm.updateField(this, Warp.class, "w", whitelist);
    }

    /**
     * Adds a player to the set of whitelisted players.
     *
     * @param pID The UUID of the player to whitelist.
     * @return True if the player was added to the whitelist.
     */
    public boolean addWhitelistedPlayer(UUID pID) {
        if (whitelist == null) {
            whitelist = new HashSet<>();
        }

        if (whitelist.contains(pID)) {
            return false;
        }

        whitelist.add(pID);
        dm.updateField(this, Warp.class, "w", whitelist);
        return true;
    }

    /**
     * Removes a player from the set of whitelisted players.
     *
     * @param pID The UUID of the player to unwhitelist.
     * @return True if the player was removed from the whitelsit.
     */
    public boolean removeWhitelistedPlayer(UUID pID) {
        if (whitelist == null || !whitelist.contains(pID)) {
            return false;
        }

        whitelist.remove(pID);
        dm.updateField(this, Warp.class, "w", whitelist);
        return true;
    }

    /**
     * Sets the UUIDs of players that are unable to access this warp when the
     * <code>privacy</code> is set to <code>PrivacyLevel.PUBLIC</code>.
     *
     * @return The set of blacklisted players.
     */
    public HashSet<UUID> getBlacklist() {
        if (blacklist == null) {
            blacklist = new HashSet<>();
        }
        return blacklist;
    }

    /**
     * Sets the UUIDs of players that are unable to access this warp when the
     * <code>privacy</code> is set to <code>PrivacyLevel.PUBLIC</code>.
     *
     * @param blacklist The new set of blacklisted players.
     */
    public void setBlacklist(HashSet<UUID> blacklist) {
        this.blacklist = blacklist;
        dm.updateField(this, Warp.class, "b", blacklist);
    }

    /**
     * Adds a player to the set of blacklisted players.
     *
     * @param pID The UUID of the player to blacklist.
     * @return True if the player was added to the blacklist.
     */
    public boolean addBlacklistedPlayer(UUID pID) {
        if (blacklist == null) {
            blacklist = new HashSet<>();
        }

        if (blacklist.contains(pID)) {
            return false;
        }

        blacklist.add(pID);
        dm.updateField(this, Warp.class, "b", blacklist);
        return true;
    }

    /**
     * Removes a player from the set of blacklist players.
     *
     * @param pID The UUID of the player to unblacklist.
     * @return True if the player was removed from the blacklist.
     */
    public boolean removeBlacklistedPlayer(UUID pID) {
        if (blacklist == null || !blacklist.contains(pID)) {
            return false;
        }

        blacklist.remove(pID);
        dm.updateField(this, Warp.class, "b", blacklist);
        return true;
    }

    /**
     * Checks if this warp is accessible by the given player.
     *
     * @param pID The UUID of the player to check.
     * @return True if the player can access this warp.
     */
    public boolean isAccessibleBy(UUID pID) {
        // Return true if it's the server
        if (pID.equals(SynergyCore.SERVER_ID)) {
            return true;
        }

        // Return false if the world of the warp appears to be unloaded
        if (Bukkit.getWorld(location.getWorldName()) == null) {
            return false;
        }

        Player player = Bukkit.getPlayer(pID);

        // Return false if they don't have permission to access the world
        if (!player.hasPermission("multiverse.access." + location.getWorldName())) {
            return false;
        }

        // Return true if they own the warp
        if (owner.equals(pID)) {
            return true;
        }

        // Return true if they have bypass permissions
        if (player.hasPermission("syn.warp.access.bypass")) {
            return true;
        }

        // Return true if the warp is public and they are not in the blacklist
        if (privacy.equals(PrivacyLevel.PUBLIC) && (blacklist == null || !blacklist.contains(pID))) {
            return true;
        }

        // Return true if the warp is private but they are in the whitelist
        if (privacy.equals(PrivacyLevel.PRIVATE) && whitelist != null && whitelist.contains(pID)) {
            return true;
        }

        return false;
    }

    /**
     * Gets the number of times this warp has been used.
     *
     * @return The number of uses.
     */
    public int getUses() {
        return uses;
    }

    /**
     * Increments the number of times this warp has been used.
     */
    public void incrementUses() {
        uses++;
        dm.updateField(this, Warp.class, "u", uses);
    }

    public static List<Warp> getWarps(String name, UUID user) {
        return getWarps(name, user, WarpCommand.WarpOwnerType::getTargetPriority);
    }

    public static List<Warp> getWarps(String name, UUID user, ToIntFunction<WarpCommand.WarpOwnerType> sortingFunction) {
        return getWarps(name, user, sortingFunction, false, true);
    }

    /**
     * Gets warps from the database with the given name, using the given user to determine the priority of warps
     * returned, with higher-priority warps first in the list. This method also filters out any warps that
     * are not accessible by the user.
     *
     * @param name The name of the warps to get.
     * @param user The user that is getting the warps, or SERVER_ID. to get all warps. If a player is provided, they must be online.
     * @param sortingFunction The function that returns an integer to sort the returned warps by.
     * @param looseMatch True to return warps that loosely match the given name.
     * @param onlyOwned True to only return warps that the user owns.
     * @return The list of matched warps, or null if an error occurred.
     */
    public static List<Warp> getWarps(String name, UUID user, ToIntFunction<WarpCommand.WarpOwnerType> sortingFunction, boolean looseMatch, boolean onlyOwned) {
        Query<Warp> warpQuery = MongoDB.getInstance().getDatastore().find(Warp.class);

        if (name != null) {
            // If the name appears to be given in the owner#warp format
            if (name.contains("#")) {
                Player player = Bukkit.getPlayer(user);
                String[] parts = name.split("#");

                // If the name is malformed then give the user an error
                if (parts.length != 2) {
                    if (player != null) {
                        BukkitUtil.sendMessage(player, Message.format("commands.warp.error.warp_not_found", name));
                    }
                    return null;
                }

                UUID ownerID = PlayerUtil.getUUID(parts[0], true, player != null && player.hasPermission("vanish.see"), true);

                // Give an error if onlyOwned is true and the owner given isn't the user
                if (onlyOwned && !ownerID.equals(user)) {
                    if (player != null) {
                        BukkitUtil.sendMessage(player, Message.format("commands.warp.error.warp_not_found", name));
                    }
                    return null;
                }

                // If no owner was able to be found then give the player an error
                if (ownerID == null) {
                    if (player != null) {
                        BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", parts[0]));
                    }
                    return null;
                }

                // Build the query
                warpQuery = warpQuery.field("o").equal(ownerID);

                // Correct the name of the warp
                name = parts[1];
            } else if (onlyOwned) {
                warpQuery = warpQuery.field("o").equal(user);
            }

            if (looseMatch) {
                warpQuery = warpQuery.field("n").containsIgnoreCase(name);
            } else {
                warpQuery = warpQuery.field("n").equalIgnoreCase(name);
            }
        } else if (onlyOwned) {
            warpQuery = warpQuery.field("o").equal(user);
        }

        return warpQuery.stream()
                // Ignore inaccessible warps
                .filter(warp -> warp.isAccessibleBy(user))
                // Sort by the WarpOwnerType sorting function, then by privacy level, then by owner name, then by warp name
                .sorted(Comparator
                        .comparingInt((Warp warp) -> sortingFunction.applyAsInt(WarpCommand.WarpOwnerType.parseWarpOwnerType(warp, user)))
                        .thenComparing(Warp::getPrivacy)
                        .thenComparing(warp -> PlayerUtil.getName(warp.getOwner()))
                        .thenComparing(Warp::getName))
                .collect(Collectors.toList());
    }

    /**
     * Represents a privacy level for a warp.
     */
    public enum PrivacyLevel {
        PRIVATE, PUBLIC
    }
}