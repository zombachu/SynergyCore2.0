package net.synergyserver.synergycore;

/**
 * Types of <code>PrivateMessage</code>s that can be sent.
 */
public enum PrivateMessageType {
    NORMAL, OFFICIAL
}
