package net.synergyserver.synergycore.guis;

public interface GUI {

    /**
     * Used to get the <code>GUIView</code> to show to the player on the initial opening of this <code>GUI</code>.
     *
     * @return The initial <code>GUIView</code> of this <code>GUI</code>.
     */
    GUIView getView();

}
