package net.synergyserver.synergycore.guis;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.settings.Setting;
import net.synergyserver.synergycore.settings.SettingCategory;
import net.synergyserver.synergycore.settings.SettingDiffs;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.settings.SettingPreset;
import net.synergyserver.synergycore.utils.ItemGUIUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.EnumMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.UUID;

/**
 * Represents the GUI to interact with a player's settings.
 */
public class SettingGUI implements GUI {

    private UUID pID;
    private SettingPreferences settingPreferences;
    private EnumMap<SettingCategory, GUIView> views;

    /**
     * Creates a new <code>SettingGUI</code>, based on the given <code>SettingPreferences</code>
     * and permission checking done against the given <code>Player</code>.
     *
     * @param player The <code>Player</code> to check permissions against.
     * @param settingPreferences The <code>SettingPreferences</code> to generate a GUI from.
     */
    public SettingGUI(Player player, SettingPreferences settingPreferences) {
        this.pID = player.getUniqueId();
        this.settingPreferences = settingPreferences;
        this.views = new EnumMap<>(SettingCategory.class);

        // Categorize settings
        SettingManager sm = SettingManager.getInstance();
        LinkedHashSet<Setting> accessibleSettings = sm.getAccessibleSettings(player);
        EnumMap<SettingCategory, LinkedHashSet<Setting>> categorizedSettings = sm.categorizeSettings(accessibleSettings);

        // Generate views and store it in the map
        for (SettingCategory category : categorizedSettings.keySet()) {
            views.put(category, generateView(category, categorizedSettings));
        }
    }

    /**
     * Generates a <code>GUIView</code> and populates it with the
     * settings found for the given <code>SettingCategory</code>.
     *
     * @param category The <code>SettingCategory</code> to make an inventory for.
     * @param categorizedSettings The categorized settings to create inventories from.
     */
    private GUIView generateView(SettingCategory category, EnumMap<SettingCategory, LinkedHashSet<Setting>> categorizedSettings) {
        Player player = Bukkit.getPlayer(pID);

        // Create a new GUIView, inventory, and GUIInventoryHolder
        GUIInventoryHolder inventoryHolder = new GUIInventoryHolder();
        String inventoryTitle = Message.format("gui.title_with_category", "Settings", category.getDisplayName());
        Inventory inventory = Bukkit.createInventory(inventoryHolder, 54, inventoryTitle);
        GUIView view = new GUIView(this, new Itemizable[6][9], inventory);
        // Now that the inventory has been linked to the view, set the fields of the GUIInventoryHolder
        inventoryHolder.setGui(this);
        inventoryHolder.setView(view);

        LinkedHashMap<String, SettingPreset> presets = settingPreferences.getPresets();
        LinkedHashSet<Setting> settings = categorizedSettings.get(category);

        SettingDiffs currentSettings = settingPreferences.getCurrentSettings();

        // Create iterators
        Iterator<String> presetIterator = presets.keySet().iterator();
        Iterator<Setting> settingIterator = settings.iterator();
        Iterator<SettingCategory> categoriesIterator = categorizedSettings.keySet().iterator();

        boolean[] presetRowTemplate = ItemGUIUtil.centerClumpElements(9, presets.size() + 1);
        boolean[][] gridTemplate = ItemGUIUtil.arrangeGrid(9, 4, settings.size());
        boolean[] categoryRowTemplate = ItemGUIUtil.centerClumpElements(9, categorizedSettings.size());

        // Create the preset row at the top
        for (int i = 0; i < presetRowTemplate.length && (i <= 4 || presetIterator.hasNext()); i++) {
            // Center the preset information
            if (i == presetRowTemplate.length / 2) {
                // Add the itemizable to the view
                view.setItemizable(GUIItem.PRESET_INFO, null, i);
                continue;
            }
            if (presetRowTemplate[i]) {
                // Add the itemizable to the view
                view.setItemizable(presets.get(presetIterator.next()), null, i);
            }
        }
        // Create the body
        for (int i = 0; i < gridTemplate.length && settingIterator.hasNext(); i++) {
            for (int j = 0; j < gridTemplate[0].length && settingIterator.hasNext(); j++) {
                if (gridTemplate[i][j]) {
                    // Add the itemizable to the view
                    Setting setting = settingIterator.next();
                    view.setItemizable(setting, setting.getValue(currentSettings, player), i + 1, j);
                }
            }
        }
        // Create the category navigation at the bottom
        for (int i = 0; i < categoryRowTemplate.length && categoriesIterator.hasNext(); i++) {
            if (categoryRowTemplate[i]) {
                // Add the itemizable to the view
                view.setItemizable(categoriesIterator.next(), null, 5, i);
            }
        }

        return view;
    }

    /**
     * Gets the <code>SettingPreferences</code> that this <code>SettingGUI</code> was based off of.
     *
     * @return The <code>SettingPreferences</code> of this <code>SettingGUI</code>.
     */
    public SettingPreferences getSettingPreferences() {
        return settingPreferences;
    }

    /**
     * Gets a <code>GUIView</code> from the cache.
     *
     * @param category The The <code>SettingCategory</code> of the view to get.
     * @return The view requested, if found, or null.
     */
    public GUIView getGUIView(SettingCategory category) {
        return views.get(category);
    }

    public GUIView getView() {
        if (views.size() > 0) {
            return views.values().iterator().next();
        }
        return null;
    }

}
