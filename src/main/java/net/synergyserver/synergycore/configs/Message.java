package net.synergyserver.synergycore.configs;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.synergyserver.synergycore.MessageArgument;
import net.synergyserver.synergycore.SynergyPlugin;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.permissions.Permissible;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Used to access messages in the messages.yml configuration file in this plugin's directory.
 */
public class Message implements Config {

    private static Message instance = null;
    private static HashMap<String, YamlConfiguration> configs;
    private static HashMap<String, String> colors;
    private static HashMap<String, String> colorsRaw;
    private static HashMap<String, String> prefixes;
    private static HashMap<String, String> messages;
    private static final DecimalFormat decimalFormat = new DecimalFormat("#.#");
    private static final Pattern HEX_CODE = Pattern.compile("[a-f0-9]{6}");
    private static final String COLOR_CODE = "0123456789abcdef";

    /**
     * Creates a new <code>Message</code> object.
     */
    private Message() {
        configs = new HashMap<>();
        colors = new HashMap<>();
        colorsRaw = new HashMap<>();
        prefixes = new HashMap<>();
        messages = new HashMap<>();
    }

    /**
     * Returns the object representing this <code>Message</code>.
     *
     * @return The object of this class.
     */
    public static Message getInstance() {
        if (instance == null) {
            instance = new Message();
        }
        return instance;
    }

    public void load(SynergyPlugin plugin) {
        File file = new File(plugin.getDataFolder(), "messages.yml");

        // If the file doesn't exist, make one with the default values
        if (!file.exists()) {
            plugin.saveResource("messages.yml", false);
        }

        try {
            // Load the file from disk
            InputStreamReader input = new InputStreamReader(plugin.getResource("messages.yml"));
            YamlConfiguration defaults = YamlConfiguration.loadConfiguration(input);
            input.close();

            // Add any missing values to the yaml configuration before saving it
            YamlConfiguration yaml = new YamlConfiguration();
            yaml.load(file);
            yaml.setDefaults(defaults);
            yaml.options().copyDefaults(true);

            // Store the config in the HashMap
            configs.put(plugin.getName(), yaml);

            // Update the file on disk
            save(plugin);

            // Put the messages in an easier-to-access HashMap
            loadIntoMap(plugin);
        } catch (IOException|InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void unload(SynergyPlugin plugin) {
        // First remove all values from the plugin in the maps
        YamlConfiguration yaml = getConfig(plugin);
        ConfigurationSection sc = yaml.getConfigurationSection("standard_colors");
        ConfigurationSection cs = yaml.getConfigurationSection("colors");
        ConfigurationSection ps = yaml.getConfigurationSection("prefixes");
        ConfigurationSection ms = yaml.getConfigurationSection("messages");

        // Unload the standard colors
        if (sc != null) {
            for (String key : sc.getKeys(true)) {
                colors.remove(key);
                colorsRaw.remove(key);
            }
        }
        // Unload the colors
        if (cs != null) {
            for (String key : cs.getKeys(true)) {
                colors.remove(key);
                colorsRaw.remove(key);
            }
        }

        // Unload the prefixes
        if (ps != null) {
            for (String key : ps.getKeys(true)) {
                prefixes.remove(key);
            }
        }

        // Unload the messages
        if (ms != null) {
            for (String key : ms.getKeys(true)) {
                messages.remove(key);
            }
        }

        // Unload the YAML config itself
        configs.remove(plugin.getName());
    }

    public void save(SynergyPlugin plugin) {
        try {
            getConfig(plugin).save(new File(plugin.getDataFolder(), "messages.yml"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the <code>YamlConfiguration</code> object containing the given plugin's configuration.
     *
     * @param plugin The <code>SynergyPlugin</code> to get the config of.
     * @return The <code>YamlConfiguration</code> object containing this plugin's configuration.
     */
    public static YamlConfiguration getConfig(SynergyPlugin plugin) {
        return configs.get(plugin.getName());
    }

    /**
     * Puts the messages of a <code>SynergyPlugin</code> into an easier-to-access HashMap.
     *
     * @param plugin The <code>SynergyPlugin</code> to stash the messages of.
     */
    private void loadIntoMap(SynergyPlugin plugin) {
        YamlConfiguration yaml = getConfig(plugin);
        ConfigurationSection sc = yaml.getConfigurationSection("standard_colors");
        ConfigurationSection cs = yaml.getConfigurationSection("colors");
        ConfigurationSection ps = yaml.getConfigurationSection("prefixes");
        ConfigurationSection ms = yaml.getConfigurationSection("messages");

        // Load the standard colors
        if (sc != null) {
            for (String key : sc.getKeys(true)) {
                if (!sc.isString(key)) {
                    continue;
                }

                // Make sure it's a valid color key
                if (!key.startsWith("v")) {
                    continue;
                }
                if (key.length() <= 1) {
                    continue;
                }
                colorsRaw.put(key, sc.getString(key));
                String color = Message.translateCodes(sc.getString(key));
                colors.put(key, color);
            }
        }
        // Load the colors
        if (cs != null) {
            for (String key : cs.getKeys(true)) {
                if (!cs.isString(key)) {
                    continue;
                }

                // Make sure it's a valid color key
                if (!key.startsWith("c")) {
                    continue;
                }
                if (key.length() <= 1) {
                    continue;
                }
                if (!MathUtil.isPositiveInteger(key.substring(1))) {
                    continue;
                }
                colorsRaw.put(key, cs.getString(key));
                String color = Message.translateCodes(cs.getString(key));
                colors.put(key, color);
            }
        }

        // Load the prefixes
        if (ps != null) {
            for (String key : ps.getKeys(true)) {
                if (!ps.isString(key)) {
                    continue;
                }
                String prefix = Message.translateCodes(replaceColorPlaceholders(ps.getString(key)));
                prefixes.put(key, prefix);
            }
        }

        // Load the messages
        if (ms != null) {
            for (String key : ms.getKeys(true)) {
                if (!ms.isString(key)) {
                    continue;
                }

                // Get the category of this message to try to match a prefix to
                String[] path = key.split("\\.");
                String category = "";
                if (path.length >= 2) {
                    category = path[path.length - 2];
                }

                // Store the message with its prefix if it's found, or by itself if it has no prefix
                String message = Message.translateCodes(replaceColorPlaceholders(ms.getString(key)));
                if (prefixes.containsKey(category) && !message.isEmpty()) {
                    messages.put(key, prefixes.get(category) + message);
                } else {
                    messages.put(key, message);
                }
            }
        }
    }

    /**
     * Replaces color placeholders in the format {c...} with the
     * corresponding value in the <code>colors</code> HashMap.
     *
     * @param message The message to replace the color placeholders in.
     * @return The message with the color placeholders replaced.
     */
    private String replaceColorPlaceholders(String message) {
        String output = message;
        for (String key : colors.keySet()) {
            String placeholder = "{" + key + "}";
            output = output.replace(placeholder, colors.get(key));
        }

        return output;
    }

    public static String translateCodes(String str) {
        return translateCodes(str, colors.get("vr"), true, true, true, true, true);
    }

    public static String translateCodes(String str, String reset) {
        return translateCodes(str, reset, true, true, true, true, true);
    }

    public static String translateCodes(String str, Permissible permissible, String permissionRoot) {
        return translateCodes(
                str,
                colors.get("vr"),
                permissible.hasPermission(permissionRoot + ".colors"),
                permissible.hasPermission(permissionRoot + ".format"),
                permissible.hasPermission(permissionRoot + ".magic"),
                permissible.hasPermission(permissionRoot + ".hex"),
                permissible.hasPermission(permissionRoot + ".vanilla"));
    }

    public static String translateCodes(String str, String reset, Permissible permissible, String permissionRoot) {
        return translateCodes(
                str,
                reset,
                permissible.hasPermission(permissionRoot + ".colors"),
                permissible.hasPermission(permissionRoot + ".format"),
                permissible.hasPermission(permissionRoot + ".magic"),
                permissible.hasPermission(permissionRoot + ".hex"),
                permissible.hasPermission(permissionRoot + ".vanilla"));
    }

    /**
     * Translates color codes, allowing the given string to be formatted as desired in Minecraft.
     * Should be used instead of ChatColor#translateAlternateColorCodes.
     *
     * @param str The string to format.
     * @param reset The formatting to reset to when &r is used.
     * @param translateStandardColors True to translate standard color codes.
     * @param translateFormatting True to translate formatting.
     * @param translateMagic True to translate magic.
     * @param translateHex True to translate hex color codes.
     * @param translateVanilla True to translate color codes as vanilla colors.
     * @return The formatted string.
     */
    public static String translateCodes(String str, String reset, boolean translateStandardColors, boolean translateFormatting,
                                        boolean translateMagic, boolean translateHex, boolean translateVanilla) {
        StringBuilder sb = new StringBuilder(str.length());
        int unappendedStart = 0;
        int amp = str.indexOf('&');
        // Continue looking for formatting codes until the entire relevant string is searched
        while (amp != -1 && amp < str.length() - 1 && unappendedStart < str.length()) {
            char c = Character.toLowerCase(str.charAt(amp + 1));

            if (translateStandardColors) {
                if (COLOR_CODE.indexOf(c) != -1) {
                    // Append everything since the last code and get the color from the config
                    sb.append(str, unappendedStart, amp);
                    sb.append(colors.get("v" + c));

                    // Start the next search after the formatting code
                    unappendedStart = amp + 2;
                    amp = str.indexOf('&', amp + 2);
                    continue;
                }
                if (c == 'r') {
                    // Append everything since the last code and add the reset code
                    sb.append(str, unappendedStart, amp);
                    sb.append(reset);

                    // Start the next search after the formatting code
                    unappendedStart = amp + 2;
                    amp = str.indexOf('&', amp + 2);
                    continue;
                }
            }
            if (translateFormatting) {
                if ("lmno".indexOf(c) != -1) {
                    // Append everything since the last code and add the formatting code
                    sb.append(str, unappendedStart, amp);
                    sb.append("§");
                    sb.append(c);

                    // Start the next search after the formatting code
                    unappendedStart = amp + 2;
                    amp = str.indexOf('&', amp + 2);
                    continue;
                }
            }
            if (translateMagic) {
                if (c == 'k') {
                    // Append everything since the last code and add the formatting code
                    sb.append(str, unappendedStart, amp);
                    sb.append("§k");

                    // Start the next search after the formatting code
                    unappendedStart = amp + 2;
                    amp = str.indexOf('&', amp + 2);
                    continue;
                }
            }
            if (translateHex) {
                if (c == '#' && amp <= str.length() - 8) {
                    String hexRaw = str.substring(amp + 2, amp + 8).toLowerCase();
                    if (HEX_CODE.matcher(hexRaw).matches()) {
                        // Append everything since the last code and add the hex code in Spigot's aids §x§#§#§#§#§#§# format
                        sb.append(str, unappendedStart, amp);
                        sb.append("§x§");
                        sb.append(hexRaw.charAt(0));
                        sb.append('§');
                        sb.append(hexRaw.charAt(1));
                        sb.append('§');
                        sb.append(hexRaw.charAt(2));
                        sb.append('§');
                        sb.append(hexRaw.charAt(3));
                        sb.append('§');
                        sb.append(hexRaw.charAt(4));
                        sb.append('§');
                        sb.append(hexRaw.charAt(5));

                        // Start the next search after the formatting code
                        unappendedStart = amp + 8;
                        amp = str.indexOf('&', amp + 8);
                        continue;
                    }
                }
            }
            if (translateVanilla) {
                if (c == '\\' && amp <= str.length() - 3) {
                    char colorCode = Character.toLowerCase(str.charAt(amp + 2));
                    if (COLOR_CODE.indexOf(colorCode) != -1) {
                        // Append everything since the last code and add the vanilla color code
                        sb.append(str, unappendedStart, amp);
                        sb.append('§');
                        sb.append(colorCode);

                        // Start the next search after the formatting code
                        unappendedStart = amp + 3;
                        amp = str.indexOf('&', amp + 2);
                        continue;
                    }
                }
            }

            // Since no valid formatting code was found, treat it like a regular ampersand and start the search at the next character
            amp = str.indexOf('&', amp + 1);
        }

        // Append the remaining characters and return
        if (unappendedStart < str.length()) {
            sb.append(str, unappendedStart, str.length());
        }
        return sb.toString();
    }

    /**
     * Gets a message by the given key, without modifying the output.
     *
     * @param key The key of the message to return.
     * @return The message with the given key.
     */
    public static String get(String key) {
        return messages.get(key);
    }

    /**
     * Gets a color by the given key, ready to be used to format messages.
     *
     * @param key The key of the color to return.
     * @return The color with the given key.
     */
    public static String getColor(String key) {
        return colors.get(key);
    }

    /**
     * Gets a color by the given key, without it being translated to format Minecraft messages.
     *
     * @param key The key of the color to return.
     * @return The raw color with the given key.
     */
    public static String getColorRaw(String key) {
        return colorsRaw.get(key);
    }

    /**
     * Gets a prefix by the given key.
     *
     * @param key The key of the prefix to return.
     * @return The prefix with the given key.
     */
    public static String getPrefix(String key) {
        return prefixes.get(key);
    }

    /**
     * Gets a message by its key and replaces the placeholder values with the given arguments.
     *
     * @param key The key of the message to return.
     * @param args The arguments to replace the placeholder values.
     * @return The formatted message.
     */
    public static String format(String key, Object... args) {
        return formatMessage(messages.get(key), args);
    }

    /**
     * Formats the given message and replaces the placeholder values with the given arguments.
     *
     * @param message The message to format.
     * @param args The arguments to replace the placeholder values.
     * @return The formatted message.
     */
    public static String formatMessage(String message, Object... args) {
        // Turn the arguments into MessageArguments for use in pluralization
        List<MessageArgument> arguments = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            String arg;
            if (args[i] instanceof Double) {
                arg = decimalFormat.format(args[i]);
            } else if (args[i] instanceof String[]) {
                arg = createList((String[]) args[i]);
            } else if (args[i] instanceof Collection) {
                arg = createList((Collection) args[i]);
            } else {
                arg = args[i].toString();
            }

            // Create a new MessageArgument for every occurrence in the message
            int index = message.indexOf("{" + (i + 1) + "}");
            while (index != -1) {
                // Find the ending index, which is the index of the } in the placeholder
                int endingIndex = index + getPlaceholderSize(i) - 1;
                // Add a new MessageArgument and find the start of the next one
                arguments.add(new MessageArgument(index, endingIndex, arg));
                index = message.indexOf("{" + (i + 1) + "}", index + 1);
            }
        }

        // Sort the list according to startingIndex for use in pluralization
        arguments.sort(Comparator.comparingInt(MessageArgument::getStartingIndex));

        // Pluralize the message
        StringBuilder output = new StringBuilder(pluralize(message, arguments));

        // Replace the arguments in the message before returning
        int offset = 0;
        for (MessageArgument argument : arguments) {
            // Offset the index of each argument by the change in position from the original, unformatted message
            output.replace(argument.getStartingIndex() + offset, argument.getEndingIndex() + 1 + offset, argument.getText());

            // Subtract the length of the placeholder from the offset since it was already in the original message
            int placeHolderSize = argument.getEndingIndex() - argument.getStartingIndex() + 1;
            // Update the offset
            offset += argument.getText().length() - placeHolderSize;
        }

        return output.toString();
    }

    /**
     * Gets a message by its key and replaces the placeholder values with the given arguments. This method can accept
     * <code>BaseComponent</code>s as arguments.
     *
     * @param key The key of the message to return.
     * @param args The arguments to replace the placeholder values.
     * @return The formatted message.
     */
    public static BaseComponent[] formatTextComponent(String key, Object... args) {
        // Turn the arguments into MessageArguments for use in pluralization
        List<MessageArgument> arguments = new ArrayList<>();
        String message = messages.get(key);
        // Handle all non-BaseComponents first
        for (int i = 0; i < args.length; i++) {
            String arg;
            if (args[i] instanceof BaseComponent) {
                continue;
            } else if (args[i] instanceof Double) {
                arg = decimalFormat.format(args[i]);
            } else if (args[i] instanceof String[]) {
                arg = createList((String[]) args[i]);
            } else if (args[i] instanceof Collection) {
                arg = createList((Collection) args[i]);
            } else {
                arg = args[i].toString();
            }

            message = message.replace("{" + (i + 1) + "}", arg);

            // Create a new MessageArgument for every occurrence in the message
            int index = message.indexOf("{" + (i + 1) + "}  ");
            while (index != -1) {
                // Find the ending index, which is the index of the } in the placeholder
                int endingIndex = index + getPlaceholderSize(i) - 1;
                // Add a new MessageArgument and find the start of the next one
                arguments.add(new MessageArgument(index, endingIndex, arg));
                index = message.indexOf("{" + (i + 1) + "}", index + 1);
            }
        }

        // Sort the list according to startingIndex for use in pluralization
        arguments.sort(Comparator.comparingInt(MessageArgument::getStartingIndex));

        // Pluralize the message
        StringBuilder output = new StringBuilder(pluralize(message, arguments));

        // Replace the arguments in the message before continuing
        for (MessageArgument argument : arguments) {
            output.replace(argument.getStartingIndex(), argument.getEndingIndex() + 1, argument.getText());
        }

        message = output.toString();

        // Handle all BaseComponents next
        ComponentBuilder outputBuilder = new ComponentBuilder("");
        int sliceStart = 0;
        int argIndex = 0;
        while (argIndex < args.length && sliceStart < message.length()) {
            // If the argument was already handled then continue
            if (!(args[argIndex] instanceof BaseComponent)) {
                argIndex++;
                continue;
            }

            // End the slice where the first instance of the corresponding placeholder is
            String placeholder = "{" + (argIndex + 1) + "}";
            int sliceEnd = message.indexOf(placeholder, sliceStart);

            // If the corresponding placeholder was not found then move to the next argument
            if (sliceEnd == -1) {
                argIndex++;
                continue;
            }

            // Append any text before the BaseComponent
            if (sliceEnd > sliceStart) {
                outputBuilder.append(TextComponent.fromLegacyText(message.substring(sliceStart, sliceEnd)), ComponentBuilder.FormatRetention.NONE);
            }

            // Append the BaseComponent
            outputBuilder.append((BaseComponent) args[argIndex]);

            // Start the new slice after the placeholder
            sliceStart = sliceEnd + getPlaceholderSize(argIndex);
        }

        // Append the remaining slice
        outputBuilder.append(TextComponent.fromLegacyText((message.substring(sliceStart, message.length()))), ComponentBuilder.FormatRetention.NONE);

        return outputBuilder.create();
    }

    /**
     * Pluralizes all instances of {s} of the given message. This method first finds the position of the pluralization
     * placeholder value ({s}) and then finds the nearest number before it. If the number is not 1 then it replaces the
     * placeholder with an "s".
     *
     * @param message The message to pluralize.
     * @param args The list of arguments that is being referred to for pluralization.
     * @return The pluralized message.
     */
    public static String pluralize(String message, List<MessageArgument> args) {
        StringBuilder output = new StringBuilder(message);

        // Simple pluralization by adding an "s" to the end of a word
        while (output.lastIndexOf("{s}") != -1) {
            int placeholderStartIndex = output.lastIndexOf("{s}");
            String number = MathUtil.findPrecedingNumber(args, placeholderStartIndex);

            // If no number was found, break out to avoid an error
            if (number == null) {
                break;
            }

            // Keep track of how many characters were changed to update the indexes of following MessageArguments
            int originalSize = 3;
            int finalSize;

            // Otherwise, pluralize the placeholder
            if (StringUtil.equalsIgnoreCase(number, "1", "1.", "1.0")) {
                output.replace(placeholderStartIndex, placeholderStartIndex + 3, "");
                finalSize = 0;
            } else {
                output.replace(placeholderStartIndex, placeholderStartIndex + 3, "s");
                finalSize = 1;
            }

            // Update the indexes of following MessageArguments
            for (MessageArgument arg : args) {
                if (arg.getStartingIndex() >= placeholderStartIndex) {
                    arg.setStartingIndex(arg.getStartingIndex() + (finalSize - originalSize));
                    arg.setEndingIndex(arg.getEndingIndex() + (finalSize - originalSize));
                }
            }
        }
        // Simple pluralization by adding an "s" to the end of a word while looking forwards
        while (output.indexOf("{s>}") != -1) {
            int placeholderEndIndex = output.indexOf("{s>}") + 3;
            String number = MathUtil.findFollowingNumber(args, placeholderEndIndex);

            // If no number was found, break out to avoid an error
            if (number == null) {
                break;
            }

            // Keep track of how many characters were changed to update the indexes of following MessageArguments
            int originalSize = 3;
            int finalSize;

            // Otherwise, pluralize the placeholder
            if (StringUtil.equalsIgnoreCase(number, "1", "1.", "1.0")) {
                output.replace(placeholderEndIndex - 3, placeholderEndIndex + 1, "");
                finalSize = 0;
            } else {
                output.replace(placeholderEndIndex - 3, placeholderEndIndex + 1, "s");
                finalSize = 1;
            }

            // Update the indexes of following MessageArguments
            for (MessageArgument arg : args) {
                if (arg.getStartingIndex() >= placeholderEndIndex) {
                    arg.setStartingIndex(arg.getStartingIndex() + (finalSize - originalSize));
                    arg.setEndingIndex(arg.getEndingIndex() + (finalSize - originalSize));
                }
            }
        }

        // Advanced choices given in the format {s>:Choice1|Choice2} where the first is the non-pluralized
        // form and the optional forwards arrow indicates to search to the right instead
        while (output.indexOf("{s") != -1) {
            int placeholderStartIndex = output.indexOf("{s");
            int placeholderEndIndex = output.indexOf("}", placeholderStartIndex + 3);

            // If no end to the placeholder could be found them break out to avoid a later error
            if (placeholderEndIndex == -1) {
                break;
            }

            boolean following = output.charAt(placeholderStartIndex + 2) == '>';
            String[] choices = output.substring(placeholderStartIndex + 3, placeholderEndIndex).split("\\|");

            // Remove the colon before the choices, if it exists
            if (choices[0].startsWith(":")) {
                choices[0] = choices[0].substring(1);
            }

            // If there are not 2 choices then continue to avoid a later IndexOutOfBounds error
            if (choices.length != 2) {
                break;
            }

            // If following is true then find the nearest following number, otherwise look backwards
            String number;
            if (following) {
                number = MathUtil.findFollowingNumber(args, placeholderEndIndex);
            } else {
                number = MathUtil.findPrecedingNumber(args, placeholderStartIndex);
            }

            // If no number was found, break out to avoid an error
            if (number == null) {
                break;
            }

            // Keep track of how many characters were changed to update the indexes of following MessageArguments
            int originalSize = placeholderEndIndex - placeholderStartIndex + 1;
            int finalSize;

            // Do the advanced pluralization
            if (StringUtil.equalsIgnoreCase(number, "1", "1.", "1.0")) {
                output.replace(placeholderStartIndex, placeholderEndIndex + 1, choices[0]);
                finalSize = choices[0].length();
            } else {
                output.replace(placeholderStartIndex, placeholderEndIndex + 1, choices[1]);
                finalSize = choices[1].length();
            }

            // Update the indexes of following MessageArguments
            for (MessageArgument arg : args) {
                if (arg.getStartingIndex() >= placeholderEndIndex) {
                    arg.setStartingIndex(arg.getStartingIndex() + (finalSize - originalSize));
                    arg.setEndingIndex(arg.getEndingIndex() + (finalSize - originalSize));
                }
            }
        }

        return output.toString();
    }


    public static String getTimeMessage(long milliseconds, int units, int places) {
        return getTimeMessage(milliseconds, units, places, "", "");
    }

    public static String getTimeMessage(long milliseconds, int units, int places, String timeColor, String grammarColor) {
        return getTimeMessage(milliseconds, units, places, timeColor, timeColor, grammarColor);
    }

    /**
     * Creates a message that converts the given milliseconds to larger units, to the specified number of units and
     * with the last unit having the specified number of places. (i.e. if 3 units and 4 places are specified, then
     * "10 days, 5 hours, and 7.5204 minutes" would be a possible response)
     *
     * Alternates the color of time units between <code>timeColor1</code> and <code>timeColor2</code>, and sets the
     * color of commas and "and" to <code>grammarColor</code>.
     *
     * @param milliseconds The milliseconds to convert.
     * @param units The number of units to have, starting from the largest non-fractional unit.
     * @param places The number of decimal places the last unit should have.
     * @return The formatted message.
     */
    public static String getTimeMessage(long milliseconds, int units, int places, String timeColor1, String timeColor2, String grammarColor) {
        TimeUtil.TimeUnit[] timeUnits = TimeUtil.TimeUnit.values();
        TimeUtil.TimeUnit largestUnit = TimeUtil.TimeUnit.GAME_TICK;
        TimeUtil.TimeUnit smallestUnit;

        // Find the largest non-fractional unit
        for (int i = timeUnits.length - 1; i >= 0; i--) {
            TimeUtil.TimeUnit unit = timeUnits[i];
            if (TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MILLI, unit, milliseconds) > 0) {
                largestUnit = unit;
                break;
            }
        }

        // Find the smallest unit that will be returned, based on the number of units requested
        // The largest unit is already counted so don't include it when getting the offset
        smallestUnit = largestUnit.getOffset(0 - (units - 1));

        int largeOrd = largestUnit.ordinal();
        int smallOrd = smallestUnit.ordinal();

        // Create an array to temporarily hold the number-word pairs we create (i.e. 5 days)
        String[] times = new String[largeOrd - smallOrd + 1];
        // Populate the array
        for (int i = 0; i < times.length; i++) {
            // The largest units go first in the array
            int ordinal = largeOrd - i;
            // Prepend {1} and append {s} for use with Message#formatMessage
            String time = "{1} " + timeUnits[ordinal].getName() + "{s}";

            // If it's the smallest unit then add decimal places
            if (ordinal == smallOrd) {
                // Call Message#formatMessage so the message is pluralized properly
                times[i] = formatMessage(time, TimeUtil.toRoundedUnitRemainder(TimeUtil.TimeUnit.MILLI, timeUnits[ordinal], milliseconds, places));
            }
            // If it's the largest unit then get the full amount to the unit rounded down and not the remainder
            else if (ordinal == largeOrd) {
                // Call Message#formatMessage so the message is pluralized properly
                times[i] = formatMessage(time, TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MILLI, timeUnits[ordinal], milliseconds));
            }
            // Otherwise get the remainder to the nearest unit, rounded down
            else {
                // Call Message#formatMessage so the message is pluralized properly
                times[i] = formatMessage(time, TimeUtil.toFlooredUnitRemainder(TimeUtil.TimeUnit.MILLI, timeUnits[ordinal], milliseconds));
            }
        }

        // Grammarterize the array and turn it into a string to return
        return createFormattedList(times, timeColor1, timeColor2, grammarColor);
    }

    public static String createFormattedList(Collection<String> items) {
        return createFormattedList(items, "", "");
    }

    public static String createFormattedList(Collection<String> items, String itemColor, String grammarColor) {
        return createFormattedList(items, itemColor, itemColor, grammarColor);
    }

    public static String createFormattedList(Collection<String> items, String itemColor1, String itemColor2, String grammarColor) {
        return createFormattedList(items.toArray(new String[items.size()]), itemColor1, itemColor2, grammarColor);
    }

    public static String createFormattedList(String[] items) {
        return createFormattedList(items, "", "");
    }

    public static String createFormattedList(String[] items, String itemColor, String grammarColor) {
        return createFormattedList(items, itemColor, itemColor, grammarColor);
    }

    /**
     * Turns the given collection of strings into a comma-separated grammatically-correct list.
     *
     * Alternates the color of items between <code>itemColor1</code> and <code>itemColor2</code>,
     * and sets the color of commas and "and" to <code>grammarColor</code>.
     *
     * @param items The array containing strings to iterate over.
     * @param itemColor1 The string to prepend to every-other item, starting with the first item.
     * @param itemColor2 The string to prepend to every-other item, starting with the second item.
     * @param grammarColor The string to prepend to the grammar added by this method.
     * @return A comma-separated representation of the input.
     */
    public static String createFormattedList(String[] items, String itemColor1, String itemColor2, String grammarColor) {
        String message = "";
        int length = items.length;

        for (int i = 0; i < length; i++) {
            // Set the item's color
            if (i % 2 == 0) {
                message += itemColor1;
            } else {
                message += itemColor2;
            }

            // Add the item to the message
            message += items[i];

            // Add the grammar color if some will follow
            if (length >= 2 && i < length - 1) {
                message += grammarColor;
            }

            // Appropriately add commas and/or "and" before the rest of the message
            if (length == 2 && i == 0) {
                message += " and ";
            } else if (length > 2) {
                if (i < length - 1) {
                    message += ", ";
                }
                if (i == length - 2) {
                    message += "and ";
                }
            }
        }

        return message;
    }

    public static String createList(Collection items) {
        return createList(items, "", "");
    }

    public static String createList(Collection items, String itemColor, String commaColor) {
        return createList(items, itemColor, itemColor, commaColor);
    }

    public static String createList(Collection items, String itemColor1, String itemColor2, String commaColor) {
        return createList((String[]) items.stream().map(Object::toString).toArray(String[]::new), itemColor1, itemColor2, commaColor);
    }

    public static String createList(String[] items) {
        return createList(items, "", "");
    }

    public static String createList(String[] items, String itemColor, String commaColor) {
        return createList(items, itemColor, itemColor, commaColor);
    }

    /**
     * Turns the given array of items into a comma-separated list.
     *
     * Alternates the color of items between <code>itemColor1</code> and
     * <code>itemColor2</code>, and sets the color of commas to <code>commaColor</code>.
     *
     * @param items The array containing strings to iterate over.
     * @param itemColor1 The string to prepend to every-other item, starting with the first item.
     * @param itemColor2 The string to prepend to every-other item, starting with the second item.
     * @param commaColor The string to prepend to the commas added by this method.
     * @return A comma-separated representation of the input.
     */
    public static String createList(String[] items, String itemColor1, String itemColor2, String commaColor) {
        String message = "";

        for (int i = 0; i < items.length; i++) {
            // Set the item's color
            if (i % 2 == 0) {
                message += itemColor1;
            } else {
                message += itemColor2;
            }

            // Add the item to the message
            message += items[i];

            // Add a comma if it isn't the last item in the list
            if (i < items.length - 1) {
                message += commaColor + ", ";
            }
        }

        return message;
    }

    /**
     * Calculates the size of the placeholder with the given index.
     *
     * @param index The index of the placeholder.
     * @return The number of characters that the placeholder takes up.
     */
    private static int getPlaceholderSize(int index) {
        return (int) Math.floor(Math.log10(index + 1)) + 3;
    }

}
