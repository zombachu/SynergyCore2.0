package net.synergyserver.synergycore;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import net.synergyserver.synergycore.database.DataEntity;
import org.bson.types.ObjectId;

import java.util.UUID;

@Entity(value = "mail")
public class Mail extends PrivateMessage implements DataEntity {

    @Id
    private ObjectId mailID;

    /**
     * Required constructor for Morphia to work.
     */
    private Mail() {}

    public Mail(UUID sender, UUID receiver, long timeSent, String message, PrivateMessageType type) {
        super(sender, receiver, timeSent, message, type);
        this.mailID = new ObjectId();
    }

    @Override
    public ObjectId getID() {
        return mailID;
    }
}
