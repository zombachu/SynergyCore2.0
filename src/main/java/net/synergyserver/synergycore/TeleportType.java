package net.synergyserver.synergycore;

/**
 * Types of teleport requests.
 */
public enum TeleportType {
    TO_RECEIVER, TO_SENDER, EVERYONE_TO_SENDER
}
