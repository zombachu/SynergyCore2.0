package net.synergyserver.synergycore;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TranslatableComponent;
import net.md_5.bungee.api.chat.hover.content.Content;
import net.md_5.bungee.api.chat.hover.content.Entity;
import net.md_5.bungee.chat.ComponentSerializer;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.events.AFKStatusChangeEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.CoreMultiOptionSetting;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.List;
import java.util.UUID;

/**
 * Represents the server's chat.
 */
public class Chat implements Listener {

    private boolean locked;
    private static Chat instance = null;

    /**
     * Creates a new <code>Chat</code> object.
     */
    private Chat() {
        locked = false;
    }

    /**
     * Returns the object representing this <code>Chat</code>.
     *
     * @return The object of this class.
     */
    public static Chat getInstance() {
        if (instance == null) {
            instance = new Chat();
        }
        return instance;
    }

    /**
     * Checks if the chat is currently locked.
     *
     * @return True if chat is locked.
     */
    public boolean isLocked() {
        return locked;
    }

    /**
     * Sets the locked status of chat.
     *
     * @param locked Set to true to lock chat.
     */
    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    /**
     * Clears the chat of every player online.
     */
    public void clear() {
        for (Player p : Bukkit.getOnlinePlayers()){
            for (int i = 0; i < 100; i++) {
                BukkitUtil.sendMessage(p, " ");
            }
        }
    }

    private void fixPlayerNameColors(BaseComponent... components) {
        if (components == null) {
            return;
        }
        
        for (BaseComponent component : components) {
            if (component instanceof TranslatableComponent) {
                fixPlayerNameColors(((TranslatableComponent) component).getWith());
            } else if (component.getHoverEvent() != null) {
                for (Content content : component.getHoverEvent().getContents()) {
                    if (content instanceof Entity) {
                        Entity entity = (Entity) content;

                        if (entity.getType().equals("minecraft:player")) {
                            Player player = Bukkit.getPlayer(component.toPlainText());

                            if (player != null) {
                                component.setColor(PlayerUtil.getPrimaryRankSync(player).getNameColor().toBungeeColor());
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private void fixPlayerNameColors(List<BaseComponent> components) {
        if (components == null) {
            return;
        }

        for (BaseComponent component : components) {
            if (component instanceof TranslatableComponent) {
                fixPlayerNameColors(((TranslatableComponent) component).getWith());
            } else if (component.getHoverEvent() != null) {
                for (Content content : component.getHoverEvent().getContents()) {
                    if (content instanceof Entity) {
                        Entity entity = (Entity) content;

                        if (entity.getType().equals("minecraft:player")) {
                            Player player = Bukkit.getPlayer(component.toPlainText());

                            if (player != null) {
                                component.setColor(PlayerUtil.getPrimaryRankSync(player).getNameColor().toBungeeColor());
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public void broadcastDeathMessage(Player player, String deathMessage) {
        broadcastDeathMessage(player, ComponentSerializer.parse(deathMessage));
    }

    /**
     * Broadcasts the given death message in chat, respecting the settings of players.
     *
     * @param player The player that died.
     * @param deathMessage The death message to broadcast.
     */
    public void broadcastDeathMessage(Player player, BaseComponent... deathMessage) {
        fixPlayerNameColors(deathMessage);

        for (Player p : Bukkit.getOnlinePlayers()) {
            // Don't send the death message if they can't see the player who died
            if (!PlayerUtil.canSee(p, player.getUniqueId())) {
                continue;
            }

            MinecraftProfile mcp = PlayerUtil.getProfile(p);

            // Don't send the death message if they ignored the player that died
            if (mcp.hasIgnored(player.getUniqueId())) {
                continue;
            }

            // Check the status of their death_messages value before sending them a message
            String deathMessagesValue = CoreMultiOptionSetting.DEATH_MESSAGES.getValue(mcp);
            switch (deathMessagesValue) {
                case "all":
                    p.spigot().sendMessage(deathMessage);
                    break;
                case "same_world_group":
                    String deathWorldGroup = SynergyCore.getWorldGroupName(player.getWorld().getName());
                    String currentPlayerWorldGroup = SynergyCore.getWorldGroupName(p.getWorld().getName());
                    if (deathWorldGroup.equals(currentPlayerWorldGroup)) {
                        p.spigot().sendMessage(deathMessage);
                    }
                    break;
                case "none":
                    break;
                default:
                    break;
            }
        }

        // Send the plaintext message to console as well
        Bukkit.getConsoleSender().spigot().sendMessage(deathMessage);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onStaffChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if the player doesn't have permissions to staffchat
        if (!player.hasPermission("syn.staffchat")) {
            return;
        }

        // If the player has staff_chat toggled or if they started the message with a comma/period, initiate staffchat
        MinecraftProfile senderMCP = PlayerUtil.getProfile(player);
        String message = event.getMessage();

        // If the message starts with a comma or period, remove it. Otherwise,
        // the setting needs to be set for staffchat to be used.
        if (message.startsWith(",")) {
            message = message.replaceFirst(",", "");
        } else if (message.startsWith(".")) {
            message = message.replaceFirst(".", "");
        } else if (!CoreToggleSetting.STAFF_CHAT.getValue(senderMCP)) {
            return;
        }

        // Remove extra whitespace
        message = message.trim().replaceAll("\\s+", " ");
        // Translate color codes
        message = Message.translateCodes(message, Message.get("chat.staff_chat.reset"));

        // Send the message to all staff members
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (!p.hasPermission("syn.staffchat")) {
                continue;
            }

            // If alert_sounds is set to true then make a sound
            MinecraftProfile mcp = PlayerUtil.getProfile(p);
            if (!p.equals(player) && CoreToggleSetting.ALERT_SOUNDS.getValue(mcp)) {
                p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1, 0);
            }

            // Send the message to the player
            BukkitUtil.sendMessage(p, Message.format("chat.staff_chat.message", player.getName(), message));
        }

        // Send the message to console
        BukkitUtil.sendMessage(
                Bukkit.getConsoleSender(),
                Message.format("chat.staff_chat.message", player.getName(), message));

        // Don't send the message to other players online
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        UUID senderID = player.getUniqueId();
        MinecraftProfile senderMCP = PlayerUtil.getProfile(player);

        // If chat is locked, cancel the message and give them feedback
        if (locked && !player.hasPermission("syn.chat.lock.bypass")) {
            BukkitUtil.sendMessage(player, Message.get("chat.error.locked"));
            event.setCancelled(true);
            return;
        }

        // If they're a Guest and have chatted too recently, cancel the message and give them feedback
        if (!player.hasPermission("syn.chat.rate-limit-bypass")) {
            long millisSinceChatted = System.currentTimeMillis() - senderMCP.getLastChatted();
            long rateLimitMillis = TimeUtil.toFlooredUnit(
                    TimeUtil.TimeUnit.SECOND,
                    TimeUtil.TimeUnit.MILLI,
                    PluginConfig.getConfig(SynergyCore.getPlugin()).getInt("guest_message_rate_seconds")
            );
            if (millisSinceChatted < rateLimitMillis) {
                String timeMessage = Message.getTimeMessage(
                        rateLimitMillis - millisSinceChatted,
                        3,
                        0
                );
                BukkitUtil.sendMessage(
                        player,
                        Message.format("chat.error.guest_rate_limit", rateLimitMillis, timeMessage));
                event.setCancelled(true);
                return;
            }
        }

        // If it's a minechat join message and they have minechat messages disabled, cancel it
        if (senderMCP.getRecentChatMessages() == null || senderMCP.getRecentChatMessages().size() == 0) {
            if (!CoreToggleSetting.MINECHAT_JOIN_MESSAGE.getValue(senderMCP)) {
                if (event.getMessage().startsWith("Connected") || event.getMessage().startsWith("connected")) {
                    event.setCancelled(true);
                    return;
                }
            }
        }

        // If the message is sent while the player is vanished, check chat_while_vanished
        if (PlayerUtil.isVanished(player) && !CoreToggleSetting.CHAT_WHILE_VANISHED.getValue(senderMCP)) {
            // Warn the player that their message wasn't sent
            BukkitUtil.sendMessage(player, Message.format("events.event_cancelled.warning.chat_while_vanished"));
            event.setCancelled(true);
            return;
        }

        // Format the message
        String message = Message.translateCodes(event.getMessage(), Message.get("chat.reset"), player, "syn.chat");

        // Customize the message for each recipient
        for (Player p : event.getRecipients()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(p);

            // If they logged off since the chat message was sent then ignore the current player
            if (mcp == null) {
                continue;
            }

            // Don't send the player a message if they're ignoring the sender
            if (mcp.hasIgnored(senderID) && !player.hasPermission("syn.ignore.exempt")) {
                continue;
            }
            // If hide_spam is set to true and the sender sent a duplicate message, don't send them a message
            if (CoreToggleSetting.HIDE_SPAM.getValue(mcp) && senderMCP.getRecentChatMessages().contains(message)) {
                continue;
            }

            String customMessage = message;

            // If colored_chat is set to false then remove color codes
            if (!CoreToggleSetting.COLORED_CHAT.getValue(mcp)) {
                customMessage = ChatColor.stripColor(customMessage);
            }

            // If remove_excessive_caps is set to true then count the capital letters in the message
            if (CoreToggleSetting.REMOVE_EXCESSIVE_CAPS.getValue(mcp)) {
                int caseCount = 0;
                for (int i = 0; i < customMessage.length(); i++) {
                    if (Character.isUpperCase(customMessage.charAt(i))) {
                        caseCount++;
                    }
                }

                // If the amount of caps is too high then lowercase the entire message
                float percent = ((float) caseCount / customMessage.length()) * 100;
                if (percent >= PluginConfig.getConfig(SynergyCore.getPlugin()).getInt("chat.excessive_caps_percent")) {
                    customMessage = customMessage.toLowerCase();
                }
            }

            // Split the message into words for analysis
            String[] words = ChatColor.stripColor(message).split("[^a-zA-Z0-9_]");

            UUID pID = p.getUniqueId();
            boolean mentionedPlayer = false;

            // Analyze the words of the message to apply settings to
            for (String word : words) {
                if (PlayerUtil.isMatch(mcp, word, 3)) {
                    // If name_highlighting is set to true then highlight the matched words
                    if (CoreToggleSetting.NAME_HIGHLIGHTING.getValue(mcp)) {
                        String regex = "(?i)(?<=\\b|[^A-Z0-9_]|§[0-9A-FK-OR])" + word + "(?=[^A-Z0-9_]|\\b)";
                        customMessage = customMessage.replaceAll(regex, Message.get("chat.name_highlighting_color") + word + ChatColor.RESET);
                    }
                    // If alert_sounds is set to true then make a sound ONCE if a match was found
                    if (CoreToggleSetting.ALERT_SOUNDS.getValue(mcp) && !mentionedPlayer) {
                        p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1, 0);
                    }
                    mentionedPlayer = true;
                }
            }

            // If minimal_chat is set to true and the player wasn't
            // mentioned, don't send them a message if it wasn't by them
            if (CoreToggleSetting.MINIMAL_CHAT.getValue(mcp) && !mentionedPlayer && !p.equals(player)) {
                continue;
            }

            // Send the player their customized message
            BukkitUtil.sendMessage(p, Message.format("chat.format", player.getDisplayName(), customMessage));
        }

        // If the sender is afk, unafk them
        WorldGroupProfile wgp = senderMCP.getCurrentWorldGroupProfile();
        if (senderMCP.isAFK()) {
            wgp.setAfktimeEnd(System.currentTimeMillis());
            AFKStatusChangeEvent afkEvent = new AFKStatusChangeEvent(true, senderMCP, false);
            Bukkit.getPluginManager().callEvent(afkEvent);
        }

        // Set the sender's last action to the current time
        wgp.setLastAction(System.currentTimeMillis());
        // Add the message to the sender's recent messages
        senderMCP.addRecentChatMessage(message);

        // Send console the message as well
        BukkitUtil.sendMessage(
                Bukkit.getConsoleSender(),
                Message.format("chat.format", player.getDisplayName(), message));

        // Cancel the event being handled by Bukkit as we already sent out custom messages
        event.setCancelled(true);
    }
}
