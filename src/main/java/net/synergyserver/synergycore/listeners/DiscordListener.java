package net.synergyserver.synergycore.listeners;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberUpdateEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.user.update.UserUpdateOnlineStatusEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.synergyserver.synergycore.ServiceToken;
import net.synergyserver.synergycore.ServiceType;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.ServiceConnectEvent;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.DiscordUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Listens to events that occur on the Discord server.
 */
public class DiscordListener extends ListenerAdapter {

    private static DiscordListener instance = null;

    /**
     * Creates a new <code>DiscordListener</code> object.
     */
    private DiscordListener() {}

    /**
     * Returns the object representing this <code>DiscordListener</code>.
     *
     * @return The object of this class.
     */
    public static DiscordListener getInstance() {
        if (instance == null) {
            instance = new DiscordListener();
        }
        return instance;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        net.dv8tion.jda.api.entities.Message message = event.getMessage();
        User user = event.getAuthor();
        Member member = event.getMember();
        Guild guild = message.getGuild();

        DataManager dm = DataManager.getInstance();


        // Ignore the event if it occurred in the wrong channel
        if (message.getChannel().getIdLong() != SynergyCore.getPluginConfig().getLong("discord.gatechannel")) {
            return;
        }

        // Ignore this event if the Discord account is already connected to a user
        if (dm.isDataEntityInDatabase(DiscordProfile.class, user.getId())) {
            return;
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(player);
            SynUser synUser = dm.getDataEntity(SynUser.class, mcp.getSynID());
            ServiceToken token = synUser.getDiscordToken();

            if (token != null && token.getToken().equals(message.getContentRaw())) {
                // If the token is expired then remove the token from the database
                if (token.isExpired()) {
                    synUser.setDiscordToken(null);
                    break;
                }

                // Send a success message
                try {
                    // Create their DiscordProfile and connect it
                    String newNickname = mcp.getCurrentName();

                    if (!DiscordUtil.isStaff(member, guild)) {
                        guild.addRoleToMember(member, guild.getRolesByName("Player", true).get(0)).queue();
                        guild.modifyNickname(member, newNickname).queue();
                    }

                    DiscordUtil.sendMessage(guild.getTextChannelById(SynergyCore.getPluginConfig().getLong("discord.mainchannel")),
                            Message.format("service_connection.discord.join", newNickname));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                synUser.setDiscordID(user.getId());

                DiscordProfile discordProfile = new DiscordProfile(
                        synUser.getID(),
                        user.getId(),
                        user.getName()
                );

                dm.saveDataEntity(discordProfile);

                // Emit an event
                ServiceConnectEvent serviceConnectEvent = new ServiceConnectEvent(synUser, ServiceType.DISCORD, discordProfile);
                Bukkit.getPluginManager().callEvent(serviceConnectEvent);
                break;
            }
        }

        // Delete the message after everything is done
        message.delete().queue();
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        DataManager dm = DataManager.getInstance();
        Member member = event.getMember();
        DiscordProfile discordProfile = dm.getDataEntity(DiscordProfile.class, member.getId());

        if (discordProfile == null) {
            return;
        }

        // Update the DiscordProfile's known names if they're using a new name
        if (!discordProfile.getCurrentName().equals(member.getEffectiveName())) {
            discordProfile.setCurrentName(member.getEffectiveName());
            discordProfile.addKnownName(member.getEffectiveName());
        }

        // Update the player's nickname based on their Minecraft username
        UUID pID = dm.getDataEntity(SynUser.class, discordProfile.getSynID()).getMinecraftID();
        MinecraftProfile mcp = dm.getPartialDataEntity(MinecraftProfile.class, pID, "n");

        if (mcp == null) {
            return;
        }

        try {
            if (!mcp.getCurrentName().equals(member.getNickname())) {
                member.modifyNickname(mcp.getCurrentName()).queue();
            }
        } catch (Exception e) {
            // Ignore errors caused by trying to modify a staff member
        }

        // Update the login time
        discordProfile.setLastLogIn(System.currentTimeMillis());
    }

    @Override
    public void onGuildMemberRemove(GuildMemberRemoveEvent event) {
        DataManager dm = DataManager.getInstance();
        User user = event.getUser();
        DiscordProfile discordProfile = dm.getDataEntity(DiscordProfile.class, user.getId());

        if (discordProfile == null) {
            return;
        }

        discordProfile.setLastLogOut(System.currentTimeMillis());
    }

    @Override
    public void onUserUpdateOnlineStatus(UserUpdateOnlineStatusEvent event) {
        DataManager dm = DataManager.getInstance();
        Member member = event.getMember();
        DiscordProfile discordProfile = dm.getDataEntity(DiscordProfile.class, member.getId());

        if (discordProfile == null) {
            return;
        }

        switch (event.getNewValue()) {
            case ONLINE:
                // Update the login time
                discordProfile.setLastLogIn(System.currentTimeMillis());
                break;
            case OFFLINE: case INVISIBLE:
                // Update the log out time
                discordProfile.setLastLogOut(System.currentTimeMillis());
                break;
            default: break;
        }
    }

    @Override
    public void onGuildMemberUpdate(GuildMemberUpdateEvent event) {
        DataManager dm = DataManager.getInstance();
        Member member = event.getMember();
        Guild server = SynergyCore.getDiscordServer();
        DiscordProfile discordProfile = dm.getDataEntity(DiscordProfile.class, member.getId());

        if (discordProfile == null) {
            return;
        }

        // Update the DiscordProfile's known names if they're using a new name
        if (!discordProfile.getCurrentName().equals(member.getEffectiveName())) {
            discordProfile.setCurrentName(member.getEffectiveName());
            discordProfile.addKnownName(member.getEffectiveName());
        }

        // Update the player's nickname based on their Minecraft username
        UUID pID = dm.getDataEntity(SynUser.class, discordProfile.getSynID()).getMinecraftID();
        MinecraftProfile mcp = dm.getPartialDataEntity(MinecraftProfile.class, pID, "n");

        if (mcp == null) {
            return;
        }

        try {
            if (!mcp.getCurrentName().equals(member.getNickname())) {
                member.modifyNickname(mcp.getCurrentName()).queue();
            }
        } catch (Exception e) {
            // Ignore errors caused by trying to modify a staff member
        }
    }
}
