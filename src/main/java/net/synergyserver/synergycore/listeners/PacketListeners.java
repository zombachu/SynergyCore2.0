package net.synergyserver.synergycore.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.reflect.StructureModifier;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

/**
 * Listens to packets.
 */
public class PacketListeners {

    // Prevent Guests from spawning in items with metadata
    public static final PacketListener nbtStripper = new PacketAdapter(SynergyCore.getPlugin(), ListenerPriority.NORMAL, PacketType.Play.Client.SET_CREATIVE_SLOT) {
        @Override
        public void onPacketReceiving(PacketEvent event) {
            StructureModifier<ItemStack> itemModified = event.getPacket().getItemModifier();
            ItemStack itemStack = itemModified.readSafely(0);

            if (itemStack == null) {
                return;
            }

            switch (itemStack.getType()) {
                case COMMAND_BLOCK, CHAIN_COMMAND_BLOCK, REPEATING_COMMAND_BLOCK, COMMAND_BLOCK_MINECART:
                    if (!event.getPlayer().hasPermission("syn.command-block")) {
                        event.setCancelled(true);
                        BukkitUtil.sendMessage(event.getPlayer(), Message.format("events.spawn_item.error.item_no_permission", "command blocks"));
                        BukkitUtil.sendMessage(Bukkit.getConsoleSender(), Message.format("events.spawn_item.warning.console_item_attempted", event.getPlayer().getName(), "command block"));
                    }
                    return;
                default:
                    break;
            }

            String itemName = itemStack.getType().name().toLowerCase();

            // Strip spawner and spawn egg data
            if (itemName.contains("spawn") && !event.getPlayer().hasPermission("syn.nbt.spawners")) {
                itemStack.setItemMeta(null);
                itemModified.write(0, itemStack);
                BukkitUtil.sendMessage(event.getPlayer(), Message.format("events.spawn_item.warning.nbt_item_no_permission", itemName));
                return;
            }

            if (!event.getPlayer().hasPermission("syn.nbt")) {
                // If the item is a shulker box then prevent it entirely
                if (itemName.contains("shulker_box")) {
                    event.setCancelled(true);
                    BukkitUtil.sendMessage(event.getPlayer(), Message.format("events.spawn_item.error.item_no_permission", "shulker boxes"));
                    BukkitUtil.sendMessage(Bukkit.getConsoleSender(), Message.format("events.spawn_item.warning.console_item_attempted", event.getPlayer().getName(), "shulker box"));
                    return;
                }

                // If the item has metadata then strip it and warn them
                if (itemStack.hasItemMeta()) {
                    itemStack.setItemMeta(null);
                    itemModified.write(0, itemStack);
                    BukkitUtil.sendMessage(event.getPlayer(), Message.format("events.spawn_item.warning.nbt_no_permission"));
                }
            }
        }
    };

    // DEBUG
    public static boolean entityMetadataDebuggerEnabled = false;
    public static final PacketListener entityMetadataDebugger = new PacketAdapter(SynergyCore.getPlugin(), ListenerPriority.NORMAL, PacketType.Play.Server.ENTITY_METADATA) {
        public void onPacketSending(PacketEvent event) {
            if (entityMetadataDebuggerEnabled) {
                int entityId = event.getPacket().getIntegers().readSafely(0);

                for (Entity e : Bukkit.getWorld("Plots2").getEntities()) {
                    if (e.getEntityId() == entityId) {
                        Location loc = e.getLocation();
                        BukkitUtil.sendMessage(Bukkit.getConsoleSender(), "DEBUG Item type for metadata packet: " + e.getType() +
                                " at coords " + loc.getX() + " " + loc.getY() + " " + loc.getZ());
                    }
                }
            }
        }
    };

}
