package net.synergyserver.synergycore.listeners;

import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.event.EventBus;
import net.luckperms.api.event.node.NodeAddEvent;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.NodeType;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.function.Consumer;

public class PermissionsListener {

    private static PermissionsListener instance = null;

    public static PermissionsListener getInstance() {
        if (instance == null) {
            instance = new PermissionsListener();
        }
        return instance;
    }

    private Consumer<NodeAddEvent> rankChangedListener = event -> {
        // Ignore the event if it's not a player's rank changing
        if (!event.isUser()) {
            return;
        }
        if (!event.getNode().getType().equals(NodeType.INHERITANCE)) {
            return;
        }


        UUID pID = ((User) event.getTarget()).getUniqueId();
        Player player = Bukkit.getPlayer(pID);

        if (player != null && player.isOnline()) {
            // Update the player's display name
            PlayerUtil.updateDisplayName(player);

            // Update their SettingGUI to reflect their new permissions
            PlayerUtil.getProfile(player).getCurrentWorldGroupProfile().createSettingGUI();
        }
    };

    public void registerListeners() {
        EventBus eventBus = LuckPermsProvider.get().getEventBus();
        eventBus.subscribe(NodeAddEvent.class, rankChangedListener);
    }

}
