package net.synergyserver.synergycore;

import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Property;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Represents a location intended to be used for serialization by MongoDB.
 */
@Embedded
public class SerializableLocation {

    @Property("w")
    private String worldName;
    @Property("x")
    private double x;
    @Property("y")
    private double y;
    @Property("z")
    private double z;
    @Property("pi")
    private float pitch;
    @Property("ya")
    private float yaw;

    /**
     * Required constructor for Morphia to work.
     */
    public SerializableLocation() {}

    /**
     * Creates a new <code>SerializableLocation</code> with the given variables.
     *
     * @param location The <code>Location</code> to turn into a <code>SerializableLocation</code>.
     */
    public SerializableLocation(Location location) {
        this.worldName = location.getWorld().getName();
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.pitch = location.getPitch();
        this.yaw = location.getYaw();
    }

    /**
     * Gets the name of the world of this location.
     *
     * @return The name of the world of this location.
     */
    public String getWorldName() {
        return worldName;
    }

    /**
     * Sets the name of the world of this location.
     *
     * @param worldName The name of the world to set.
     */
    public void setWorldName(String worldName) {
        this.worldName = worldName;
    }

    /**
     * Gets the x position of this location.
     *
     * @return The x position of this location.
     */
    public double getX() {
        return x;
    }

    /**
     * Sets the x position of this location.
     *
     * @param x The x position to set.
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Gets the y position of this location.
     *
     * @return The y position of this location.
     */
    public double getY() {
        return y;
    }

    /**
     * Sets the y position of this location.
     *
     * @param y The y position to set.
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Gets the z position of this location.
     *
     * @return The z position of this location.
     */
    public double getZ() {
        return z;
    }

    /**
     * Sets the z position of this location.
     *
     * @param z The z position to set.
     */
    public void setZ(double z) {
        this.z = z;
    }

    /**
     * Gets the pitch of this location.
     *
     * @return The pitch of this location.
     */
    public float getPitch() {
        return pitch;
    }

    /**
     * Sets the pitch of this location.
     *
     * @param pitch The pitch to set.
     */
    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    /**
     * Gets the yaw of this location.
     *
     * @return The yaw of this location.
     */
    public float getYaw() {
        return yaw;
    }

    /**
     * Sets the yaw of this location.
     *
     * @param yaw The yaw to set.
     */
    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    /**
     * Convenience method for getting this location as a <code>Location</code> object.
     *
     * @return A <code>Location</code> object with the same information as this <code>SerializableLocation</code>.
     */
    public Location getLocation() {
        return new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch);
    }

    /**
     * Convenience method for setting this location with <code>Location</code> object.
     *
     * @param location A <code>Location</code> object to set this <code>SerializableLocation</code> as.
     */
    public void setLocation(Location location) {
        this.worldName = location.getWorld().getName();
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.pitch = location.getPitch();
        this.yaw = location.getYaw();
    }

}
