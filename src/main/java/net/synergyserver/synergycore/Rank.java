package net.synergyserver.synergycore;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.group.GroupManager;
import net.luckperms.api.node.types.InheritanceNode;
import net.synergyserver.synergycore.utils.ListUtil;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Represents a player's rank.
 */
public enum Rank {
    ADMIN("Admin", "admin", ChatColor.BLUE, "A"),
    HELPER("Helper", "helper", ChatColor.AQUA, "H"),
    FUNDER("Funder", "funder", ChatColor.DARK_GREEN),
    FULL_DONATOR("Full Donator", "donatorfull", ChatColor.DARK_GREEN),
    SUBSCRIPTION_DONATOR("Subscription Donator", "donatorsubscribed", ChatColor.GREEN),
    RETIRED("Retired", ChatColor.DARK_AQUA, "retired", ChatColor.DARK_AQUA, "R", ChatColor.DARK_PURPLE, false),
    CRIMSONSTONER("Crimsonstoner", ChatColor.DARK_AQUA, "crimsonstoner"),
    TRUSTED("Trusted", "trusted", ChatColor.DARK_AQUA, "T"),
    DEVOTED("Devoted", "devoted", ChatColor.GOLD, "D"),
    MEMBER("Member", "member", ChatColor.GRAY, "M"),
    GUEST("Guest", "guest", ChatColor.DARK_GRAY, "G");

    private String displayName;
    private ChatColor displayColor;
    private String luckPermsName;
    private ChatColor nameColor;
    private String prefix;
    private ChatColor prefixColor;
    private boolean isDonator;
    public static final Map<String, Rank> PRIMARY_RANKS =
            Arrays.stream(values()).filter(Rank::isPrimaryRank).collect(Collectors.toMap(
                    Rank::getLuckPermsName, Function.identity(), (a, b) -> b, LinkedHashMap::new));
    public static final Map<String, Rank> DONATOR_RANKS =
            Arrays.stream(values()).filter(Rank::isDonator).collect(Collectors.toMap(
                    Rank::getLuckPermsName, Function.identity(), (a, b) -> b, LinkedHashMap::new));

    Rank(String displayName, ChatColor displayColor, String luckPermsName) {
        this(displayName, displayColor, luckPermsName, null, null, null, false);
    }

    Rank(String displayName, String luckPermsName, ChatColor nameColor, String prefix) {
        this(displayName, nameColor, luckPermsName, nameColor, prefix, null, false);
    }

    Rank(String displayName, String luckPermsName, ChatColor prefixColor) {
        this(displayName, prefixColor, luckPermsName, null, null, prefixColor, true);
    }

    Rank(String displayName, ChatColor displayColor, String luckPermsName, ChatColor nameColor, String prefix, ChatColor prefixColor, boolean isDonator) {
        this.displayName = displayName;
        this.displayColor = displayColor;
        this.luckPermsName = luckPermsName;
        this.nameColor = nameColor;
        this.prefix = prefix;
        this.prefixColor = prefixColor;
        this.isDonator = isDonator;
    }

    /**
     * Gets the display name of this <code>Rank</code>, which is used in info messages.
     *
     * @return The display name of this <code>Rank</code>.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Gets the color of this <code>Rank</code> that is used in info messages.
     *
     * @return The display name of this <code>Rank</code>.
     */
    public ChatColor getDisplayColor() {
        return displayColor;
    }

    /**
     * Gets the display name of this <code>Rank</code> with its display color.
     *
     * @return The colored display name of this <code>Rank</code>.
     */
    public String getDisplay() {
        return getDisplayColor() + getDisplayName();
    }

    /**
     * Gets the name that this <code>Rank</code> is registered under in LuckPerms.
     *
     * @return The PEX name of this <code>Rank</code>.
     */
    public String getLuckPermsName() {
        return luckPermsName;
    }

    public String getLuckPermsNodeString() {
        return "group." + luckPermsName;
    }

    /**
     * Gets the color that is used for player names. If this is null then this <code>Rank</code> is a secondary rank.
     *
     * @return The color of this <code>Rank</code>.
     */
    public ChatColor getNameColor() {
        return nameColor;
    }

    /**
     * Gets the string that is used as a prefix for player names. If this is null then this <code>Rank</code> is a secondary rank.
     *
     * @return The prefix of this <code>Rank</code>.
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Gets the color that is used for the prefix of this <code>Rank</code>. If none is provided
     * by the rank itself then the default value of <code>ChatColor.GRAY</code> is used.
     *
     * @return The color of this <code>Rank</code>'s prefix.
     */
    public ChatColor getPrefixColor() {
        return Optional.ofNullable(prefixColor).orElse(ChatColor.GRAY);
    }

    /**
     * Checks if this <code>Rank</code> is a donator rank.
     *
     * @return True if this <code>Rank</code> is a donator rank.
     */
    public boolean isDonator() {
        return isDonator;
    }

    /**
     * Attempts to parse a <code>Rank</code> from the given string.
     *
     * @param str The string to parse.
     * @return The matched <code>Rank</code> if found, or null.
     */
    public static Rank parseRank(String str) {
        for (Rank rank : values()) {
            if (rank.getLuckPermsName().equalsIgnoreCase(str) || rank.getDisplayName().equalsIgnoreCase(str)) {
                return rank;
            }
        }
        return null;
    }

    /**
     * Gets the LuckPerms <code>InheritanceNode</code> representation of this <code>Rank</code>.
     *
     * @return The <code>InheritanceNode</code> of this <code>Rank</code>.
     */
    public InheritanceNode toInheritanceNode() {
        LuckPerms luckPerms = LuckPermsProvider.get();
        GroupManager groupManager = luckPerms.getGroupManager();
        return InheritanceNode.builder(groupManager.getGroup(luckPermsName)).build();
    }

    /**
     * Checks if this <code>Rank</code> is in the primary hierarchy of ranks.
     *
     * @return True if this <code>Rank</code> is a primary rank.
     */
    public boolean isPrimaryRank() {
        return nameColor != null;
    }

    /**
     * Gets the name of this <code>Rank</code> that is used in scoreboards. Rank names are lowercased
     * and prefixed with a capital letter that is used to sort the tab list by rank hierarchy.
     *
     * @return The scoreboard name of this <code>Rank</code>, or null if this <code>Rank</code> is not visible in the scoreboard.
     */
    public String getScoreBoardName() {
        Rank[] primaryRanks = Arrays.stream(values()).filter(Rank::isPrimaryRank).toArray(Rank[]::new);
        int index = ListUtil.indexOf(primaryRanks, this);

        if (index == -1) {
            return null;
        } else {
            return Character.toString((char) (index + 65)) + luckPermsName.toLowerCase();
        }
    }
}
