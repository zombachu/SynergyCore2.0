package net.synergyserver.synergycore.profiles;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import dev.morphia.query.FindOptions;
import dev.morphia.query.experimental.filters.Filters;
import net.synergyserver.synergycore.ServiceToken;
import net.synergyserver.synergycore.database.DataEntity;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Represents the central profile for users on Synergy. Objects of this class contain the IDs of connected services,
 * which are also used as keys in the database.
 */
@Entity(value = "synusers")
public class SynUser implements DataEntity {

    @Id
    private ObjectId synID;

    @Property("mc")
    private UUID minecraftID;
    @Property("d")
    private String discordID;
    @Property("dt")
    private String dubtrackID;
    @Property("w")
    private String websiteID;
    @Property("gb")
    private boolean globallyBanned;
    @Property("a")
    private Set<ObjectId> altSynIDs;
    @Property("dto")
    private ServiceToken discordToken;
    @Property("dtto")
    private ServiceToken dubtrackToken;
    @Property("ad")
    private int amountDonated;
    @Property("sd")
    private boolean isSubscribedDonator;

    @Transient
    private DataManager dm = DataManager.getInstance();

    /**
     * Required constructor for Morphia to work.
     */
    public SynUser(UUID minecraftID) {
        this.synID = new ObjectId();
        this.minecraftID = minecraftID;
    }

    /**
     * Gets the synID of this user.
     *
     * @return The ObjectId that represents a user's synID. This ID is
     * also used by the database to store and load data for this object.
     */
    public ObjectId getID() {
        return synID;
    }

    /**
     * Gets the UUID of the Minecraft account registered to this <code>SynUser</code>.
     *
     * @return The Minecraft UUID of this <code>SynUser</code>.
     */
    public UUID getMinecraftID() {
        return minecraftID;
    }

    /**
     * Gets the Discord ID registered to this <code>SynUser</code>,
     * which is used by both Discord and the database.
     *
     * @return The Discord ID of this <code>SynUser</code>.
     */
    public String getDiscordID() {
        return discordID;
    }

    /**
     * Sets the Discord ID that is registered to this <code>SynUser</code>,
     * which is used by both Discord and the database.
     *
     * @param discordID The Discord ID to register to this <code>SynUser</code>.
     */
    public void setDiscordID(String discordID) {
        this.discordID = discordID;
        dm.updateField(this, SynUser.class, "d", discordID);
    }

    /**
     * Gets the Dubtrack ID registered to this <code>SynUser</code>,
     * which is used by both Dubtrack and the database.
     *
     * @return The Dubtrack ID of this <code>SynUser</code>.
     */
    public String getDubtrackID() {
        return dubtrackID;
    }

    /**
     * Sets the Dubtrack ID that is registered to this <code>SynUser</code>,
     * which is used by both Dubtrack and the database.
     *
     * @param dubtrackID The Dubtrack ID to register to this <code>SynUser</code>.
     */
    public void setDubtrackID(String dubtrackID) {
        this.dubtrackID = dubtrackID;
        dm.updateField(this, SynUser.class, "dt", dubtrackID);
    }

    /**
     * Gets the website ID registered to this <code>SynUser</code>,
     * which is used by both the website and the database.
     *
     * @return The website ID of this <code>SynUser</code>.
     */
    public String getWebsiteID() {
        return websiteID;
    }

    /**
     * Sets the website ID that is registered to this <code>SynUser</code>,
     * which is used by both the website and the database.
     *
     * @param websiteID The website ID of this <code>SynUser</code>.
     */
    public void setWebsiteID(String websiteID) {
        this.websiteID = websiteID;
        dm.updateField(this, SynUser.class, "w", websiteID);
    }

    /**
     * Checks if the player is globally banned, meaning that they
     * cannot use any of the connected services or connect new ones.
     *
     * @return True if this <code>SynUser</code> is globally banned.
     */
    public boolean isGloballyBanned() {
        return globallyBanned;
    }

    /**
     * Sets this player to be globally banned or not, meaning that they
     * cannot use any of the connected services or connect new ones.
     *
     * @param globallyBanned Set to true to globally ban this <code>SynUser</code>.
     */
    public void setGloballyBanned(boolean globallyBanned) {
        this.globallyBanned = globallyBanned;
        dm.updateField(this, SynUser.class, "gb", globallyBanned);
    }

    /**
     * Gets the <code>ServiceToken</code> used for Discord.
     *
     * @return The token used for connecting to Discord.
     */
    public ServiceToken getDiscordToken() {
        return discordToken;
    }

    /**
     * Sets the <code>ServiceToken</code> used for Discord.
     *
     * @param token The new token used for connecting to Discord.
     */
    public void setDiscordToken(ServiceToken token) {
        this.discordToken = token;
        dm.updateField(this, SynUser.class, "dto", token);
    }

    /**
     * Gets the <code>ServiceToken</code> used for Dubtrack.
     *
     * @return The token used for connecting to Discord.
     */
    public ServiceToken getDubtrackToken() {
        return dubtrackToken;
    }

    /**
     * Sets the <code>ServiceToken</code> used for Dubtrack.
     *
     * @param token The new token used for connecting to Dubtrack.
     */
    public void setDubtrackToken(ServiceToken token) {
        this.dubtrackToken = token;
        dm.updateField(this, SynUser.class, "dtto", token);
    }

    /**
     * Gets the <code>MinecraftProfile</code> connected to this <code>SynUser</code>.
     *
     * @return The <code>MinecraftProfile</code> connected to this <code>SynUser</code>.
     */
    public MinecraftProfile getMinecraftProfile() {
        return dm.getDataEntity(MinecraftProfile.class, minecraftID);
    }

    /**
     * Gets the <code>DiscordProfile</code> connected to this <code>SynUser</code>.
     *
     * @return The <code>DiscordProfile</code> connected to this <code>SynUser</code>.
     */
    public DiscordProfile getDiscordProfile() {
        return dm.getDataEntity(DiscordProfile.class, discordID);
    }

    /**
     * Gets the <code>DubtrackProfile</code> connected to this <code>SynUesr</code>.
     *
     * @return The <code>DubtrackProfile</code> connected to this <code>SynUesr</code>.
     */
    public DubtrackProfile getDubtrackProfile() {
        return dm.getDataEntity(DubtrackProfile.class, dubtrackID);
    }

    /**
     * Gets the <code>WebsiteProfile</code> connected to this <code>SynUser</code>.
     *
     * @return The <code>WebsiteProfile</code> connected to this <code>SynUser</code>.
     */
    public WebsiteProfile getWebsiteProfile() {
        return dm.getDataEntity(WebsiteProfile.class, websiteID);
    }

    /**
     * Registers a synID as an alt of this <code>SynUser</code>.
     *
     * @param synID The synID to register as an alt.
     * @return True if the given id was not already registered as an alt.
     */
    public boolean addAlt(ObjectId synID) {
        if (altSynIDs == null) {
            altSynIDs = new HashSet<>();
        }

        boolean success = altSynIDs.add(synID);
        if (success) {
            dm.updateField(this, SynUser.class, "a", altSynIDs);
        }
        return success;
    }

    /**
     * Unregisters a synID as being an alt of this <code>SynUser</code>.
     *
     * @param synID The synID to unregister.
     * @return True if the given id was previously an alt.
     */
    public boolean removeAlt(ObjectId synID) {
        if (altSynIDs == null) {
            return false;
        }

        boolean success = altSynIDs.remove(synID);
        if (success) {
            dm.updateField(this, SynUser.class, "a", altSynIDs);
        }
        return success;
    }

    /**
     * Registers the given synID as an alt to all alts of this <code>SynUser</code>.
     *
     * @param synID The synID to register.
     * @return True if the given id was not already registered as an alt to any of the alts.
     */
    public boolean addAltToAlts(ObjectId synID) {
        // Don't continue if the id given is not a SynUser object in the database
        if (!dm.isDataEntityInDatabase(SynUser.class, synID)) {
            return false;
        }

        boolean success = true;
        SynUser altToAdd = dm.getPartialDataEntity(SynUser.class, synID, "a");
        // Add the new id to all previously registered alts
        for (SynUser synUser : getAlts()) {
            // Also add all current alts as alts to the new SynUser
            altToAdd.addAlt(synUser.getID());
            if (!synUser.addAlt(synID) && success) {
                success = false;
            }
        }
        return success;
    }

    /**
     * Unregisters the given synID as an alt from all alts of this <code>SynUser</code>.
     *
     * @param synID The synID to unregister.
     * @return True if the given id was previously an alt of all of the alts.
     */
    public boolean removeAltFromAlts(ObjectId synID) {
        // Don't continue if the id given is not a SynUser object in the database
        if (!dm.isDataEntityInDatabase(SynUser.class, synID)) {
            return false;
        }

        boolean success = true;
        SynUser altToRemove = dm.getPartialDataEntity(SynUser.class, synID, "a");
        // Remove the given id from all currently registered alts
        for (SynUser synUser : getAlts()) {
            // Also remove all current alts as alts from the new SynUser
            altToRemove.removeAlt(synUser.getID());
            if (!synUser.removeAlt(synID) && success) {
                success = false;
            }
        }
        return success;
    }

    /**
     * Gets a list of all <code>MinecraftProfile</code>s of this <code>SynUser</code> and its alts.
     *
     * @return A list containing all <code>MinecraftProfile</code>s of this <code>SynUser</code>.
     */
    public List<MinecraftProfile> getMinecraftProfiles() {
        List<MinecraftProfile> mcps = new ArrayList<>();

        if (minecraftID != null) {
            mcps.add(dm.getDataEntity(MinecraftProfile.class, minecraftID));
        }
        if (altSynIDs != null) {
            mcps.addAll(MongoDB.getInstance().getDatastore()
                    .find(MinecraftProfile.class)
                    .filter(Filters.in("sid", altSynIDs)).stream().toList());
        }
        return mcps;
    }

    /**
     * Gets a list of all <code>SynUser</code>s that are registered as alts of this <code>SynUser</code>.
     *
     * @return The list containing all <code>SynUser</code>s that are registered as alts.
     */
    public List<SynUser> getAlts() {
        return dm.getDataEntities(SynUser.class, altSynIDs);
    }

    /**
     * Gets a list of all <code>MinecraftProfile</code>s of this <code>SynUser</code>
     * and its alts and limits the fields retrieved to only those given.
     *
     * @param fields The names of the fields to retrieve.
     * @return A list containing the partial <code>MinecraftProfile</code>s of this <code>SynUser</code>.
     */
    public List<MinecraftProfile> getPartialMinecraftProfiles(String... fields) {
        List<MinecraftProfile> mcps = new ArrayList<>();

        if (minecraftID != null) {
            mcps.add(dm.getPartialDataEntity(MinecraftProfile.class, minecraftID, fields));
        }
        if (altSynIDs != null) {
            mcps.addAll(MongoDB.getInstance().getDatastore().find(MinecraftProfile.class)
                    .filter(Filters.in("sid", altSynIDs))
                    .stream(new FindOptions().projection().include(fields)).toList());
        }
        return mcps;
    }

    /**
     * Gets all current names of all <code>MinecraftProfile</code>s connected to this <code>SynUser</code>.
     *
     * @return A list containing all current Minecraft usernames of this <code>SynUser</code>.
     */
    public List<String> getAllCurrentMinecraftNames() {
        List<String> names = new ArrayList<>();

        for (MinecraftProfile profile : getPartialMinecraftProfiles("n")) {
            if (!names.contains(profile.getCurrentName())) {
                names.add(profile.getCurrentName());
            }
        }
        return names;
    }

    /**
     * Gets all names, past and present, of all <code>MinecraftProfile</code>s connected to this <code>SynUser</code>.
     *
     * @return A list containing all Minecraft usernames, past and present, of this <code>SynUser</code>.
     */
    public List<String> getAllMinecraftNames() {
        List<String> names = new ArrayList<>();

        for (MinecraftProfile mcp : getPartialMinecraftProfiles("nl")) {
            for (String name : mcp.getKnownNames()) {
                if (!names.contains(name)) {
                    names.add(name);
                }
            }
        }
        return names;
    }

    /**
     * Gets all IPs recently used by this <code>SynUser</code> to connect to the Minecraft server.
     *
     * @return A list containing all IPs this <code>SynUser</code> recently connected with to the Minecraft server.
     */
    public List<String> getAllRecentIPs() {
        List<String> ips = new ArrayList<>();

        for (MinecraftProfile mcp : getPartialMinecraftProfiles("il")) {
            for (String ip : mcp.getRecentIPs()) {
                if (!ips.contains(ip)) {
                    ips.add(ip);
                }
            }
        }
        return ips;
    }

    /**
     * Gets the total playtime of this <code>SynUser</code>.
     *
     * @return The sum of the playtimes of all <code>MinecraftProfile</code>s of this <code>SynUser</code>.
     */
    public long getTotalPlaytime() {
        long sum = 0;

        for (MinecraftProfile mcp : getPartialMinecraftProfiles("wp")) {
            sum += mcp.getTotalPlaytime();
        }
        return sum;
    }

    /**
     * Gets the total calculated playtime of this <code>SynUser</code>.
     *
     * @return The sum of the calculated playtimes of all <code>MinecraftProfile</code>s of this <code>SynUser</code>.
     */
    public long getTotalCalculatedPlaytime() {
        long sum = 0;

        for (MinecraftProfile mcp : getPartialMinecraftProfiles("wp")) {
            sum += mcp.getTotalCalculatedPlaytime();
        }
        return sum;
    }

    /**
     * Gets the amount this player has donated to Synergy, in cents.
     *
     * @return The amount this player has donated to Synergy.
     */
    public int getAmountDonated() {
        return amountDonated;
    }

    /**
     * Sets the amount this player has donated to Synergy.
     *
     * @param amountDonated The player's new donation amount, in cents.
     */
    public void setAmountDonated(int amountDonated) {
        this.amountDonated = amountDonated;
        dm.updateField(this, SynUser.class, "ad", amountDonated);
    }

    /**
     * Checks whether or not this player has donated to the server..
     *
     * @return True if the player has donated.
     */
    public boolean hasDonated() {
        return amountDonated > 0;
    }

    /**
     * Gets whether or not this player is currently on a subscription donation plan.
     *
     * @return True if the player is on a subscription.
     */
    public boolean isSubscribedDonator() {
        return isSubscribedDonator;
    }

    /**
     * Sets whether this player is on a subscription donation plan.
     *
     * @param isSubscribedDonator True if the player is on a subscription.
     */
    public void setSubscribedDonator(boolean isSubscribedDonator) {
        this.isSubscribedDonator = isSubscribedDonator;
        dm.updateField(this, SynUser.class, "sd", isSubscribedDonator);
    }
}
