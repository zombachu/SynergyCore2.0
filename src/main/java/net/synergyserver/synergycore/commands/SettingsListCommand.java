package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.settings.Setting;
import net.synergyserver.synergycore.settings.SettingCategory;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

@CommandDeclaration(
        commandName = "list",
        aliases = {"show", "display", "current"},
        permission = "syn.settings.list",
        usage = "/settings list",
        description = "Displays all settings that you can alter and their current values.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER,
        parentCommandName = "settings"
)
public class SettingsListCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        SettingManager sm = SettingManager.getInstance();

        // Get the settings that the player can see
        LinkedHashSet<Setting> accessibleSettings = sm.getAccessibleSettings(player);

        // Categorize the settings
        EnumMap<SettingCategory, LinkedHashSet<Setting>> categorizedSettings = sm.categorizeSettings(accessibleSettings);

        // Get the currently set values of those settings
        EnumMap<SettingCategory, LinkedHashMap<Setting, Object>> appliedSettings = new EnumMap<>(SettingCategory.class);
        for (SettingCategory category : categorizedSettings.keySet()) {
            if (!appliedSettings.containsKey(category)) {
                appliedSettings.put(category, new LinkedHashMap<>());
            }
            for (Setting setting : categorizedSettings.get(category)) {
                appliedSettings.get(category).put(setting, setting.getValue(mcp));
            }
        }

        // Get the things to display
        LinkedHashMap<String, List<String>> toDisplay = new LinkedHashMap<>();
        for (SettingCategory category : appliedSettings.keySet()) {
            List<String> currentCategory = new ArrayList<>();
            for (Setting setting : appliedSettings.get(category).keySet()) {
                // Use the name of the setting and the value set
                String settingName = setting.getDisplayName();
                String settingValue = appliedSettings.get(category).get(setting).toString();
                String element = Message.format("commands.settings.list.item", settingName, settingValue);
                currentCategory.add(element);
            }
            toDisplay.put(category.getDisplayName(), currentCategory);
        }

        // Send the output to the user
        for (String categoryName : toDisplay.keySet()) {
            BukkitUtil.sendMessage(player, Message.format("commands.settings.list.category", categoryName));

            String colorizedList = Message.createList(
                    toDisplay.get(categoryName),
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.item_color_2"),
                    Message.get("info_colored_lists.grammar_color")
            );

            // Turn the array into a string and return it to the player
            BukkitUtil.sendMessage(player, colorizedList);
        }
        return true;
    }
}
