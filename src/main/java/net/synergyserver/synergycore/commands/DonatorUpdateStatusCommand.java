package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.DonatorStatusUpdateEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "updatestatus",
        aliases = {"us", "update"},
        permission = "syn.donator.update-status",
        usage = "/donator updatestatus <player>",
        description = "Forcefully updates the donator rank and prefix of a player.",
        minArgs = 1,
        maxArgs = 1,
        parentCommandName = "donator"
)
public class DonatorUpdateStatusCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID donatorID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        // Give the sender an error if the name provided isn't valid
        if (donatorID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Get their current MinecraftProfile
        MinecraftProfile mcp;
        if (PlayerUtil.isOnline(donatorID)) {
            mcp = PlayerUtil.getProfile(donatorID);
        } else {
            mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, donatorID, "sid");
        }

        // Get the SynUser
        SynUser synUser = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());

        // Emit an event
        DonatorStatusUpdateEvent event = new DonatorStatusUpdateEvent(synUser);
        Bukkit.getPluginManager().callEvent(event);

        // Tell the sender what players were affected
        List<String> playerNames = new ArrayList<>();
        for (MinecraftProfile altMCP : synUser.getPartialMinecraftProfiles("n")) {
            playerNames.add(altMCP.getCurrentName());
        }
        String playerNameList = Message.createList(
                playerNames,
                Message.get("info_colored_lists.item_color_1"),
                Message.get("info_colored_lists.item_color_2"),
                Message.get("info_colored_lists.grammar_color")
        );
        BukkitUtil.sendMessage(sender, Message.format("commands.donator.update_status.info.affected_players", playerNameList));
        return true;
    }
}
