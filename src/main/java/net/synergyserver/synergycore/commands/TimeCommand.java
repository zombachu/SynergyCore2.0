package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "time",
        permission = "syn.time",
        usage = "/time [time]",
        description = "Checks or sets the current world's time.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class TimeCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        World world = player.getWorld();

        // If no argument was provided then return the world's current time
        if (args.length == 0) {
            BukkitUtil.sendMessage(player, Message.format("commands.time.info.current_time", world.getName(), TimeUtil.formatGameTime(world.getTime()), world.getTime()));
            return true;
        }

        // Check if they have permission to set the time
        if (!player.hasPermission("syn.time.set")) {
            BukkitUtil.sendMessage(player, Message.format("commands.time.error.no_permission"));
            return false;
        }

        // Parse the time
        long time = TimeUtil.parseGameTime(args[0]);

        // If the time was unable to be parsed then give an error
        if (time == -1) {
            BukkitUtil.sendMessage(player, Message.format("commands.time.error.invalid_time_format", args[0]));
            return false;
        }

        world.setTime(time);
        BukkitUtil.sendMessage(player, Message.format("commands.time.info.time_set", world.getName(), TimeUtil.formatGameTime(time), time));
        return true;
    }
}
