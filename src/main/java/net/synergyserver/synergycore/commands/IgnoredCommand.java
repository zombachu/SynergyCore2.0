package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "ignored",
        aliases = "blocked",
        permission = "syn.ignored",
        usage = "/ignored",
        description = "Lists all players that you have ignored.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class IgnoredCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If no players are ignored by them, then give them special feedback
        if (mcp.getIgnoredPlayers().isEmpty()) {
            BukkitUtil.sendMessage(player, Message.get("commands.ignore.info.none_ignored"));
            return true;
        }

        // Turn the collection of IDs into a list of names
        List<String> names = new ArrayList<>(mcp.getIgnoredPlayers().size());
        for (UUID pID : mcp.getIgnoredPlayers()) {
            names.add(PlayerUtil.getName(pID));
        }
        String list = Message.createFormattedList(
                names,
                Message.get("info_colored_lists.item_color_1"),
                Message.get("info_colored_lists.item_color_2"),
                Message.get("info_colored_lists.grammar_color")
        );

        // Send the player the message
        BukkitUtil.sendMessage(player, Message.format("commands.ignore.info.ignored", list));
        return true;
    }
}
