package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Page;
import net.synergyserver.synergycore.Pagination;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

@CommandDeclaration(
        commandName = "page",
        permission = "syn.page",
        usage = "/page <next|previous|current|page number>",
        description = "Navigates the pages of a message.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class PageCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        Pagination pagination = PlayerUtil.getProfile(player).getCurrentPagination();
        Page page;

        // Get the specified page
        if (StringUtil.equalsIgnoreCase(args[0], "next", "n")) {
            page = pagination.nextPage();
        } else if (StringUtil.equalsIgnoreCase(args[0], "previous", "prev", "p")) {
            page = pagination.previousPage();
        } else if (StringUtil.equalsIgnoreCase(args[0], "current", "c")) {
            page = pagination.getCurrentPage();
        } else if (MathUtil.isInteger(args[0])) {
            List<Page> pages = pagination.getPages();
            int index = Integer.parseInt(args[0]) - 1;
            // If the given number is out of range then give the player an error
            if (index < 0 || index >= pages.size()) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.out_of_range", 1, pages.size(), index + 1));
                return false;
            }

            page = pages.get(index);
        } else {
            // If the argument provided isn't valid then give the player the syntax
            BukkitUtil.sendMessage(sender, Message.format("commands.error.incorrect_syntax", getUsage()));
            return false;
        }

        if (page.getHeader() != null) {
            BukkitUtil.sendMessage(player, page.getHeader());
        }
        for (String line : page.getText()) {
            BukkitUtil.sendMessage(sender, Message.format("commands.page.format", line));
        }
        if (page.getFooter() != null) {
            BukkitUtil.sendMessage(player, page.getFooter());
        }
        return true;
    }
}
