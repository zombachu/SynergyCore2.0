package net.synergyserver.synergycore.commands;

import io.sponges.dubtrack4j.framework.User;
import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CommandDeclaration(
        commandName = "dubtrack",
        aliases = {"dub"},
        permission = "syn.online.discord",
        usage = "/online <discord|dubtrack>",
        description = "Lists the users online on a specific service.",
        maxArgs = 0,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE},
        parentCommandName = "online"
)
public class OnlineDubtrackCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags){
        try {
            Map<String, User> users = SynergyCore.getDubtrackRoom().getUsers();
            List<String> usernames = new ArrayList<>();

            for (User user : users.values()) {
                // Add the user name to the list and reset the color to gray for the punctuation
                usernames.add(user.getUsername() + ChatColor.GRAY);
            }

            String online = Message.createFormattedList(
                    usernames,
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.item_color_2"),
                    Message.get("info_colored_lists.grammar_color")
            );

            BukkitUtil.sendMessage(sender, Message.format("commands.online.list", "Dubtrack", online));
            return false;
        } catch (ServiceOfflineException e) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.service_offline", "Dubtrack"));
            return false;
        }
    }
}
