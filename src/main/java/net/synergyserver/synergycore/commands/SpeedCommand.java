package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "speed",
        permission = "syn.speed",
        usage = "/speed <0-10> [player]",
        description = "Sets the speed of the sender or target player.",
        minArgs = 1,
        maxArgs = 2
)
public class SpeedCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // Guve an error if the provided argument is not a number
        if (!MathUtil.isFloat(args[0])){
            BukkitUtil.sendMessage(sender, Message.format("commands.error.not_a_number", args[0]));
            return false;
        }

        float speed = Float.parseFloat(args[0]);

        // Give an error if the speed is out of the accepted range
        if (speed < 0 || speed > 10) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.out_of_range", 0, 10, speed));
            return false;
        }

        Player player;

        // If no player is provided then affect the sender
        if (args.length == 1) {
            // Give an error if the sender isn't a player
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                BukkitUtil.sendMessage(sender, Message.get("commands.error.wrong_sender_type"));
                return false;
            }

            player = (Player) sender;
        } else {
            // Give an error if the sender doesn't have permission to change another player's speed
            if (!sender.hasPermission("syn.speed.others")) {
                BukkitUtil.sendMessage(sender, Message.get("commands.speed.error.no_permission_others"));
                return false;
            }

            UUID pID = PlayerUtil.getUUID(args[1], false, sender.hasPermission("vanish.see"));

            // Give an error if no player was found
            if (pID == null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[1]));
                return false;
            }

            player = Bukkit.getPlayer(pID);
        }

        // Get the player's mode of transport and set their speed for it
        String modeOfTransport;
        if (player.isFlying()) {
            modeOfTransport = "flying";
            player.setFlySpeed(speed / 10);
        } else {
            modeOfTransport = "walking";
            player.setWalkSpeed(speed / 10);
        }

        // Give a different message if the player changed their own speed
        if (sender.equals(player)) {
            BukkitUtil.sendMessage(sender, Message.format("commands.speed.info.self", modeOfTransport, speed));
        } else {
            BukkitUtil.sendMessage(sender, Message.format("commands.speed.info.others", player.getName(), modeOfTransport, speed));
        }

        return true;
    }
}
