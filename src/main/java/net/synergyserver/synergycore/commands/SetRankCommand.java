package net.synergyserver.synergycore.commands;

import net.luckperms.api.LuckPermsProvider;
import net.synergyserver.synergycore.Rank;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "setrank",
        permission = "syn.setrank",
        usage = "/setrank <player> <rank>",
        description = "Sets the rank of a player.",
        minArgs = 2,
        maxArgs = 2
)
public class SetRankCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID pID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));
        if (pID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        Rank newRank = Rank.parseRank(args[1]);
        if (newRank == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.setrank.error.no_rank_found", args[1]));
            return false;
        }

        if (sender instanceof Player) {
            Rank senderRank = PlayerUtil.getPrimaryRankSync((Player) sender);
            if (newRank.ordinal() <= senderRank.ordinal()) {
                BukkitUtil.sendMessage(sender, Message.format("commands.setrank.error.rank_too_high", args[1]));
                return false;
            }
        }

        Player player = Bukkit.getPlayer(pID);
        Rank currentRank = PlayerUtil.getPrimaryRankSync(player);
        LuckPermsProvider.get().getUserManager().modifyUser(pID, user -> {
            user.data().remove(currentRank.toInheritanceNode());
            user.data().add(newRank.toInheritanceNode());
        });

        String messageKey = switch (newRank) {
            case GUEST -> null;
            case HELPER -> "broadcast.info.helper_promotion";
            default -> "broadcast.info.promotion";
        };

        if (messageKey != null) {
            Bukkit.broadcastMessage(Message.format(messageKey, player.getName(), newRank.getDisplay()));
        }
        return true;
    }
}
