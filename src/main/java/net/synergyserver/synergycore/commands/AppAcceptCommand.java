package net.synergyserver.synergycore.commands;

import net.luckperms.api.LuckPermsProvider;
import net.synergyserver.synergycore.BroadcastType;
import net.synergyserver.synergycore.Mail;
import net.synergyserver.synergycore.MemberApplication;
import net.synergyserver.synergycore.PrivateMessageType;
import net.synergyserver.synergycore.Rank;
import net.synergyserver.synergycore.StaffDashboard;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.ChatBroadcastEvent;
import net.synergyserver.synergycore.events.MailSendEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "accept",
        permission = "syn.app.accept",
        usage = "/app accept <index> [-s]",
        description = "Accepts the Member application with the given index.",
        minArgs = 1,
        maxArgs = 1,
        parseCommandFlags = true,
        parentCommandName = "app"
)
public class AppAcceptCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // If the given index is not a number then give the sender a message
        if (!MathUtil.isInteger(args[0])) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.not_a_number", args[0]));
            return false;
        }

        StaffDashboard dashboard = StaffDashboard.getInstance();
        List<MemberApplication> applications = dashboard.getMemberApplications();
        int index = Integer.parseInt(args[0]) - 1;

        // If the given number is not a valid index then give the sender a message
        if (index < 0 || index >= applications.size()) {
            BukkitUtil.sendMessage(
                    sender,
                    Message.format("commands.error.out_of_range", 1, applications.size(), index + 1));
            return false;
        }

        MemberApplication application = applications.get(index);

        // Promote the specified player
        UUID pID = application.getPlayerID();
        LuckPermsProvider.get().getUserManager().modifyUser(pID, user -> {
            user.data().remove(Rank.GUEST.toInheritanceNode());
            user.data().add(Rank.MEMBER.toInheritanceNode());
        });
        // Resolve the application
        dashboard.resolveMemberApplication(pID);

        // Broadcast a message
        String name = PlayerUtil.getName(pID);

        if (!flags.hasFlag("-s", "-silent")) {
            String message = Message.format("broadcast.info.promotion", name, Rank.MEMBER.getDisplay());
            ChatBroadcastEvent event = new ChatBroadcastEvent(BroadcastType.COMMAND, message);
            Bukkit.getPluginManager().callEvent(event);

            // Check if the event was cancelled before broadcasting
            if (!event.isCancelled()) {
                Bukkit.broadcastMessage(message);
            }

        }
        // Give the sender feedback
        BukkitUtil.sendMessage(sender, Message.format("commands.application.info.accept_successful", name));

        // If the player was offline at the time of their promotion, send them mail
        if (!PlayerUtil.isOnline(pID)) {
            // Craft the mail and emit an event
            String mailMessage = Message.format("mail.promotion", Rank.MEMBER.getDisplay());
            Mail mail = new Mail(SynergyCore.SERVER_ID, pID, System.currentTimeMillis(), mailMessage, PrivateMessageType.OFFICIAL);
            MailSendEvent mailEvent = new MailSendEvent(mail);
            Bukkit.getPluginManager().callEvent(mailEvent);

            // Check if the event was cancelled before continuing
            if (mailEvent.isCancelled()) {
                return true;
            }

            // Add the mail to the receiver's profile
            MinecraftProfile mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "ml");
            mcp.addMail(mail);
        }

        return true;
    }
}
