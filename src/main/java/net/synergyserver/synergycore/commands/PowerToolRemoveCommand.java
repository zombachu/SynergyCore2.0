package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.PowerTool;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;

@CommandDeclaration(
        commandName = "remove",
        aliases = "r",
        permission = "syn.powertool.remove",
        usage = "/powertool remove <index>",
        description = "Removes a command from the current held PowerTool.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "powertool"
)
public class PowerToolRemoveCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        // Give an error if the argument provided isn't a number
        if (!MathUtil.isInteger(args[0])) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.not_a_number", args[0]));
            return false;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        ItemStack item = player.getInventory().getItemInMainHand();

        // Give an error if the player isn't holding an item
        if (item == null || item.getType().equals(Material.AIR)) {
            BukkitUtil.sendMessage(player, Message.get("commands.powertool.error.no_item"));
            return false;
        }

        HashMap<String, PowerTool> powerTools = mcp.getPowerTools();
        String powerToolKey = ItemUtil.itemTypeToString(item);
        PowerTool powerTool = powerTools.get(powerToolKey);

        // If the held item isn't a powertool then give them an error
        if (powerTool == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.powertool.error.not_a_powertool"));
            return false;
        }

        List<String> commands = powerTool.getCommands();
        int index = Integer.parseInt(args[0]) - 1;

        // Give an error if the index is out of range
        if (index < 0 || index >= commands.size()) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.out_of_range", 1, commands.size(), index + 1));
            return false;
        }

        String removedCommand = commands.get(index);

        // If no commands will be left, remove the powertool entirely, otherwise just delete the specified command
        if (commands.size() == 1) {
            powerTools.remove(powerToolKey);
        } else {
            commands.remove(index);
        }

        // Update the value in the database
        mcp.setPowerTools(powerTools);

        // Delete a specific command from the current tool
        BukkitUtil.sendMessage(player, Message.format("commands.powertool.remove.info.removed_command", removedCommand));
        return true;
    }
}
