package net.synergyserver.synergycore.commands;

import net.dv8tion.jda.api.entities.Member;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "synmigrate",
        permission = "syn.migrate",
        usage = "/synmigrate",
        description = "Migrates saved data to the newest format.",
        maxArgs = 0
)
public class SynMigrateCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // Log players on discord/dubtrack without a connected account so it can be manually removed later
        // Actually just discord because let's be honest nobody uses dubtrack

        DataManager dm = DataManager.getInstance();
        for (Member user : SynergyCore.getDiscordServer().getMembers()) {
            if (user.getRoles().contains(SynergyCore.getDiscordServer().getRolesByName("Player", false).get(0)) && !dm.isDataEntityInDatabase(DiscordProfile.class, user.getId())) {
                BukkitUtil.sendMessage(Bukkit.getConsoleSender(), "Unconnected Discord account: " + user.getId() + " / " + user.getEffectiveName());
            }
        }

        return false;
    }


}
