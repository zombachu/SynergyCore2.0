package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.WeatherType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


@CommandDeclaration(
        commandName = "weather",
        aliases = {"makeitrain"},
        permission = "syn.weather",
        usage = "/weather <storm|sun|rain> [duration]",
        description = "Sets the weather of the player's world to the specified type for the specified duration of seconds or the natural length of time.",
        maxArgs = 2,
        minArgs = 1,
        validSenders = SenderType.PLAYER
)

public class WeatherCommand extends MainCommand {
    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        World world = player.getWorld();
        WeatherType weatherType = WeatherType.parseWeatherConstant(args[0]);

        // If the weather type was unable to be parsed give the player an error
        if (weatherType == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.weather.error.unrecognizable_key", args[0]));
            return false;
        }

        world.setStorm(weatherType.isStorm());
        world.setThundering(weatherType.isThundering());

        // If the player specified the weather duration then set it
        if (args.length > 1) {
            long duration = Long.parseLong(args[1]);
            int time = (int) TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.SECOND, TimeUtil.TimeUnit.GAME_TICK, duration);
            world.setWeatherDuration(time);

            if (weatherType.isThundering()) {
                world.setThunderDuration(time);
            }
        }

        BukkitUtil.sendMessage(player, Message.format("commands.weather.info.successfully_set", weatherType.getDisplayText()));
        return true;
    }
}
