package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.MemberApplication;
import net.synergyserver.synergycore.StaffDashboard;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;

import java.util.List;

@CommandDeclaration(
        commandName = "list",
        permission = "syn.app.list",
        usage = "/app list",
        description = "Displays all unresolved applications.",
        maxArgs = 0,
        parentCommandName = "app"
)
public class AppListCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        StaffDashboard dashboard = StaffDashboard.getInstance();
        List<MemberApplication> applications = dashboard.getMemberApplications();

        // If there are no applications then give the sender a message
        if (applications == null || applications.isEmpty()) {
            BukkitUtil.sendMessage(sender, Message.get("commands.application.info.no_applications"));
            return true;
        }

        // Give the sender the list of applications
        BukkitUtil.sendMessage(sender, Message.format("commands.application.list.header", applications.size()));
        for (int i = 0; i < applications.size(); i++) {
            MemberApplication application = applications.get(i);
            String applicant = PlayerUtil.getName(application.getPlayerID());
            String timeAgo = Message.getTimeMessage(
                    System.currentTimeMillis() - application.getTimeSent(), 2, 1,
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.grammar_color")
            );
            BukkitUtil.sendMessage(sender, Message.format("commands.application.list.item", i + 1, applicant, timeAgo));
        }
        return true;
    }
}
