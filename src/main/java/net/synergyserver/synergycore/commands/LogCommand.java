package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Page;
import net.synergyserver.synergycore.Pagination;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@CommandDeclaration(
        commandName = "log",
        permission = "syn.log",
        usage = "/log [page]",
        description = "Reads a set of lines from the latest log file.",
        maxArgs = 1,
        validSenders = {SenderType.CONSOLE, SenderType.PLAYER}
)
public class LogCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // > This is where we store the latest log file. Should be updated later to work with GZip files.
        String logLocation = Bukkit.getServer().getWorldContainer().getAbsolutePath() + "/logs/latest.log";

        try {
            // Collect the lines from the file, then convert them to a pagination
            BufferedReader reader = new BufferedReader(new FileReader(logLocation));
            Pagination pagination = new Pagination(reader.lines().collect(Collectors.toList()), null, 25, true);
            List<Page> pages = pagination.getPages();
            // Cleanup
            reader.close();

            // Get the requested page out of the log file
            int page = 0;
            if (args.length > 0) {
                // Make sure the sender supplied a number to us
                if (!MathUtil.isInteger(args[0])) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.error.not_a_number", args[0]));
                    return false;
                }
                // Make sure the page is within the proper range
                page = Integer.parseInt(args[0]) - 1;
                if (page < 0 || page >= pages.size()) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.error.out_of_range", 1, pages.size(), args[0]));
                    return false;
                }
            }

            // If the sender is a player, update their current pagination
            if (SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                PlayerUtil.getProfile((Player) sender).setCurrentPagination(pagination);
            }

            pagination.printCurrentPage(sender, "commands.page.format");

            // Update the current page in the player's cached pagination
            pagination.setCurrentIndex(page);
            return true;
        }

        // The file wasn't found or there was an exception trying to close the connection
        catch (IOException e) {
            BukkitUtil.sendMessage(sender, Message.format("commands.log.error.file_error", e.getMessage()));
            return false;
        }
    }
}
