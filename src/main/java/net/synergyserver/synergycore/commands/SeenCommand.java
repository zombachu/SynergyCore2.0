package net.synergyserver.synergycore.commands;

import com.google.common.net.InetAddresses;
import dev.morphia.query.FindOptions;
import dev.morphia.query.MorphiaCursor;
import dev.morphia.query.experimental.filters.Filters;
import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "seen",
        permission = "syn.seen",
        usage = "/seen <player|ip> [-wg [worldgroup]]",
        description = "Check when a player last logged in or how long they've been on.",
        minArgs = 1,
        maxArgs = 1,
        parseCommandFlags = true
)
public class SeenCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        if (InetAddresses.isInetAddress(args[0])) {
            // Make sure the sender has permission to check by ip
            if (!sender.hasPermission("syn.seen.admin")) {
                BukkitUtil.sendMessage(sender, Message.format("commands.seen.ip.error.no_permission"));
                return false;
            }

            String correctedWorldGroupName = "";

            // Check if a world group was specified
            if (flags.hasFlag("-w", "-wg", "-worldgroup")) {
                String worldGroupName = flags.getFlagValue("-w", "-wg", "-worldgroup");

                // If no flag value was given, then default to the most recent world group of the target
                if (worldGroupName.isEmpty()) {
                    MorphiaCursor<MinecraftProfile> mcpIt = MongoDB.getInstance().getDatastore().find(MinecraftProfile.class)
                            .filter(Filters.eq("il", args[0]))
                            .iterator(new FindOptions().projection().include("li", "lo"));

                    // If no MCPs were found then tell the sender
                    if (!mcpIt.hasNext()) {
                        BukkitUtil.sendMessage(sender, Message.format("commands.seen.ip.info.no_accounts_found", args[0]));
                        return true;
                    }

                    MinecraftProfile mostRecentMCP = mcpIt.next();
                    while (mcpIt.hasNext()) {
                        MinecraftProfile mcp = mcpIt.next();
                        if (mcp.isOnline()) {
                            if (!mostRecentMCP.isOnline()) {
                                mostRecentMCP = mcp;
                            } else {
                                // Get the live representation of both MCPs to compare lastActions
                                long mostRecentMCPLastAction = PlayerUtil.getProfile(mostRecentMCP.getID()).getCurrentWorldGroupProfile().getLastAction();
                                long mcpLastAction = PlayerUtil.getProfile(mcp.getID()).getCurrentWorldGroupProfile().getLastAction();

                                if (mcpLastAction > mostRecentMCPLastAction) {
                                    mostRecentMCP = mcp;
                                }
                            }
                        } else if (mcp.getLastLogOut() > mostRecentMCP.getLastLogOut()) {
                            mostRecentMCP = mcp;
                        }
                    }
                    mcpIt.close();

                    correctedWorldGroupName = mostRecentMCP.getCurrentWorldGroupName();
                } else {
                    // Correct the capitalization for use with other methods
                    correctedWorldGroupName = SynergyCore.getExactWorldGroupName(worldGroupName);;

                    // Check to make sure the world group is valid
                    if (!SynergyCore.hasWorldGroupName(correctedWorldGroupName)) {
                        BukkitUtil.sendMessage(sender, Message.format("commands.error.invalid_world_group_name", worldGroupName));
                        return false;
                    }
                }
            }

            // If the first argument is a valid IP then proceed
            MorphiaCursor<MinecraftProfile> mcpIt;

            // If a world group was specified then only filter for MCPs with data for that world group
            if (!correctedWorldGroupName.isEmpty()) {
                mcpIt = MongoDB.getInstance().getDatastore().find(MinecraftProfile.class)
                        .filter(Filters.eq("il", args[0]), Filters.eq("lw", correctedWorldGroupName))
                        .iterator(new FindOptions().projection().include("n", "li", "lo", "lw"));
            } else {
                mcpIt = MongoDB.getInstance().getDatastore().find(MinecraftProfile.class)
                        .filter(Filters.eq("il", args[0]))
                        .iterator(new FindOptions().projection().include("n", "li", "lo", "lw"));
            }

            // Give a special message if no results were found
            if (!mcpIt.hasNext()) {
                if (correctedWorldGroupName.isEmpty()) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.seen.ip.info.no_accounts_found", args[0]));
                } else {
                    BukkitUtil.sendMessage(sender, Message.format("commands.seen.ip.info.no_accounts_found_world_group", correctedWorldGroupName, args[0]));
                }
                return true;
            }

            // Populate the list of names and color them based on whether or not they're currently online
            List<String> names = new ArrayList<>();

            while (mcpIt.hasNext()) {
                MinecraftProfile mcp = mcpIt.next();
                if (mcp.isOnline()) {
                    names.add(ChatColor.GREEN + mcp.getCurrentName());
                } else {
                    names.add(ChatColor.DARK_RED + mcp.getCurrentName());
                }
            }
            mcpIt.close();

            String nameMessage = Message.createFormattedList(names);

            // Send the sender the message
            if (correctedWorldGroupName.isEmpty()) {
                BukkitUtil.sendMessage(sender, Message.format("commands.seen.ip.info.accounts_found", names.size(), args[0], nameMessage));
            } else {
                BukkitUtil.sendMessage(sender, Message.format("commands.seen.ip.info.accounts_found_world_group", names.size(), correctedWorldGroupName, args[0], nameMessage));
            }
            return true;
        } else {
            // Otherwise treat the first argument as a player name
            UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"), false, true);

            if (pID == null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
                return false;
            }

            MinecraftProfile mcp;
            boolean canSee = PlayerUtil.canSee(sender, pID);

            // Get the live MCP if the player is online
            if (PlayerUtil.isOnline(pID)) {
                mcp = PlayerUtil.getProfile(pID);
            } else {
                mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "n", "nl", "il", "li", "lo", "lw", "wp");
            }

            String correctedWorldGroupName = "";

            // Check if a world group was specified
            if (flags.hasFlag("-w", "-wg", "-worldgroup")) {
                String worldGroupName = flags.getFlagValue("-w", "-wg", "-worldgroup");

                // If no flag value was given, then default to the current world group of the target
                if (worldGroupName.isEmpty()) {
                    correctedWorldGroupName = mcp.getCurrentWorldGroupName();
                } else {
                    // Correct the capitalization for use with other methods
                    correctedWorldGroupName = SynergyCore.getExactWorldGroupName(worldGroupName);;

                    // Check to make sure the world group is valid
                    if (!SynergyCore.hasWorldGroupName(correctedWorldGroupName)) {
                        BukkitUtil.sendMessage(sender, Message.format("commands.error.invalid_world_group_name", worldGroupName));
                        return false;
                    }
                }
            }

            // If a world group was specified then get the info for that profile
            if (!correctedWorldGroupName.isEmpty()) {
                WorldGroupProfile wgp = mcp.getPartialWorldGroupProfile(correctedWorldGroupName, "li", "lo");

                // If the user has no data for that world group then tell the sender
                if (wgp == null) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.info.world_group_no_data_others", mcp.getCurrentName(), correctedWorldGroupName));
                    return false;
                }

                // Get the login/logout time
                long time;
                if (canSee && wgp.isInUse()) {
                    time = wgp.getLastLogIn();
                } else {
                    time = wgp.getLastLogOut();
                }
                String timeMessage = Message.getTimeMessage(
                        System.currentTimeMillis() - time, 3, 1,
                        Message.get("info_colored_lists.item_color_1"),
                        Message.get("info_colored_lists.grammar_color")
                );

                // Display the header
                if (canSee && wgp.isInUse()) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.seen.player.header.info.world_group_in_use", mcp.getCurrentName(), correctedWorldGroupName, timeMessage));
                } else {
                    BukkitUtil.sendMessage(sender, Message.format("commands.seen.player.header.info.world_group_not_in_use", mcp.getCurrentName(), correctedWorldGroupName, timeMessage));
                }
            } else {
                // Get the login/logout time
                long time;
                if (canSee) {
                    time = mcp.getLastLogIn();
                } else {
                    time = mcp.getLastLogOut();
                }
                String timeMessage = Message.getTimeMessage(
                        System.currentTimeMillis() - time, 3, 1,
                        Message.get("info_colored_lists.item_color_1"),
                        Message.get("info_colored_lists.grammar_color")
                );

                // Display the header
                String onlineStatus = canSee ? ChatColor.GREEN + "online" : ChatColor.DARK_RED + "offline";
                BukkitUtil.sendMessage(sender, Message.format("commands.seen.player.header.info.global", mcp.getCurrentName(), onlineStatus, timeMessage));

                // If the player is AFK then include that as a detail
                if (canSee && mcp.isAFK()) {
                    WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
                    String afkTimeMessage = Message.getTimeMessage(
                            System.currentTimeMillis() - wgp.getAfktimeStart(), 3, 1,
                            Message.get("info_colored_lists.item_color_1"),
                            Message.get("info_colored_lists.grammar_color")
                    );
                    // Send the afk info
                    BukkitUtil.sendMessage(sender, Message.format("commands.seen.player.detail.afk", afkTimeMessage));
                }

                // If the sender can see vanish players and the target online then include their vanish status as a detail
                if (mcp.isOnline() && sender.hasPermission("vanish.see")){
                    BukkitUtil.sendMessage(sender, Message.format("commands.seen.player.detail.vanished", canSee ? "not vanished": "vanished"));
                }

                // If the player has changed their names then include past names as a detail
                if (mcp.getKnownNames().size() > 1) {
                    String nameMessage = Message.createFormattedList(mcp.getKnownNames(),
                            Message.getColor("c4"),
                            Message.get("info_colored_lists.grammar_color")
                    );
                    // Send the name info
                    BukkitUtil.sendMessage(sender, Message.format("commands.seen.player.detail.previous_names", nameMessage));
                }

                // Display the player's IP address as a detail if the user has the right permission
                if (sender.hasPermission("syn.seen.admin")) {
                    // Send the user's last known IP address
                    BukkitUtil.sendMessage(sender, Message.format("commands.seen.player.detail.address_info", mcp.getRecentIPs().get(mcp.getRecentIPs().size() - 1)));
                }
            }
            return true;
        }
    }
}
