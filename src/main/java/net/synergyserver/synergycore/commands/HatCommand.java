package net.synergyserver.synergycore.commands;


import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.Arrays;

@CommandDeclaration(
        commandName = "Hat",
        aliases = {"helmet", "wear"},
        permission = "syn.hat",
        usage = "/hat",
        description = "Puts the currently held item on your head, no matter how silly it would be!",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class HatCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags){
        Player player = (Player) sender;
        PlayerInventory inventory = player.getInventory();

        ItemStack mainHand = inventory.getItemInMainHand();
        ItemStack oldHat = inventory.getHelmet();

        if (mainHand.getType().equals(Material.AIR)) {
            // If they're wearing a hat, give it back
            if (oldHat != null && !oldHat.getType().equals(Material.AIR)) {
                inventory.setHelmet(null);
                inventory.setItemInMainHand(oldHat);
                BukkitUtil.sendMessage(player, Message.format("commands.hat.info.old_hat_returned", oldHat.getType().name()));
                return true;
            }
            // Otherwise give an error that they aren't holding an item to use as a helmet
            else {
                BukkitUtil.sendMessage(player, Message.format("commands.hat.error.no_held_item"));
                return false;
            }
        }
        // Otherwise attempt to set their new hat
        else {
            // If the player is wearing a hat and there isn't space to put it in their inventory give an error
            if (oldHat != null && Arrays.stream(inventory.getContents()).noneMatch(itemStack ->
                    itemStack == null ||
                    itemStack.getType() == Material.AIR ||
                    (itemStack.getAmount() < itemStack.getMaxStackSize() && itemStack.isSimilar(oldHat)))) {
                BukkitUtil.sendMessage(player, Message.format("commands.hat.error.no_space_to_return"));
                return false;
            }

            // Create the new hat
            ItemStack newHat = mainHand.clone();
            newHat.setAmount(1);
            mainHand.setAmount(mainHand.getAmount() - 1);
            inventory.setHelmet(newHat);

            // Give back the player's old hat if it exists
            if (oldHat != null) {
                inventory.addItem(oldHat);
            }

            // Give feedback
            BukkitUtil.sendMessage(player, Message.format("commands.hat.info.enjoy_hat", newHat.getType().name()));
            return true;
        }
    }
}