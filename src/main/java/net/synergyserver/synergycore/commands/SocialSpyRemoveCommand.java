package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.UUID;

@CommandDeclaration(
        commandName = "remove",
        aliases = "r",
        permission = "syn.socialspy.manage",
        usage = "/socialspy remove <player|*>",
        description = "Stops spying on the messages of the given player.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "socialspy"
)
public class SocialSpyRemoveCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {

        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If the argument is * disable global socialspy
        if (args[0].equals("*")) {
            mcp.setGlobalSocialSpy(false);
            BukkitUtil.sendMessage(player, Message.format("commands.socialspy.info.global_switched", "disabled"));
            return true;
        }

        UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));
        HashSet<UUID> players = mcp.getSpiedPlayers();

        // Check to make sure the specified player exists
        if (pID == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Return an error if the player isn't in the list
        if (!players.contains(pID)) {
            BukkitUtil.sendMessage(player, Message.format("commands.socialspy.error.not_in_list", PlayerUtil.getName(pID)));
            return false;
        }

        // Add the player to the spy list
        players.remove(pID);
        mcp.setSpiedPlayers(players);
        BukkitUtil.sendMessage(player, Message.format("commands.socialspy.info.removed_player", PlayerUtil.getName(pID)));
        return true;
    }
}
