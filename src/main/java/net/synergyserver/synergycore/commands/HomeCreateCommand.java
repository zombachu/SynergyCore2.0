package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@CommandDeclaration(
        commandName = "create",
        aliases = {"set", "add"},
        permission = "syn.home.create",
        usage = "/home create <name>",
        description = "Creates a new home with the given name and your current location.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "home"
)
public class HomeCreateCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
        YamlConfiguration config = PluginConfig.getConfig(SynergyCore.getPlugin());

        // Get the max number of homes they can set in the current world group
        int max = config.getInt("home.limit.default");

        if (player.hasPermission("syn.home.limit.unlimited")) {
            max = Integer.MAX_VALUE;
        } else {
            Map<String, Object> limits = config.getConfigurationSection("home.limit").getValues(false);
            for (String group : limits.keySet()) {
                if (player.hasPermission("syn.home.limit." + group)) {
                    max = config.getInt("home.limit." + group);
                }
            }
        }

        // Avoid a NPE
        if (wgp.getHomes() == null) {
            wgp.setHomes(new HashMap<>());
        }

        HashMap<String, SerializableLocation> homes = wgp.getHomes();

        // If the next home will make them go over their limit then give them an error
        if (homes.size() + 1 > max) {
            BukkitUtil.sendMessage(player, Message.get("commands.home.create.error.too_many_homes"));
            return false;
        }

        String name = args[0].toLowerCase();

        // If the name given isn't safe to store then give them an error
        if (!StringUtil.isWord(name)) {
            BukkitUtil.sendMessage(player, Message.format("commands.home.create.error.unsafe_name"));
            return false;
        }

        // If they already have a home with the given name then give them an error
        if (homes.containsKey(name)) {
            BukkitUtil.sendMessage(player, Message.format("commands.home.create.error.name_already_exists", name));
            return false;
        }

        // Add the new home and update the field in the database
        homes.put(name, new SerializableLocation(player.getLocation()));
        wgp.setHomes(homes);

        // Give the player feedback and return true
        BukkitUtil.sendMessage(player, Message.format("commands.home.create.info.created_home", name));
        return true;
    }
}
