package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Teleport;
import net.synergyserver.synergycore.TeleportType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.TeleportRequestSendEvent;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "tprall",
        aliases = {"tprhereall", "tprallhere", "tpaall", "tpahereall", "tpaallhere"},
        permission = "syn.tprall",
        usage = "/tprall",
        description = "Requests everyone to teleport to you.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class TPRAllCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        int sent = 0;
        for (Player receiver : Bukkit.getOnlinePlayers()) {
            if (player.equals(receiver)) {
                continue;
            }
            if (!PlayerUtil.canSee(player, receiver.getUniqueId())) {
                continue;
            }

            UUID receiverID = receiver.getUniqueId();

            // Emit an event
            Teleport teleport = new Teleport(player.getUniqueId(), receiverID,
                    player.getLocation(), TeleportType.EVERYONE_TO_SENDER);
            TeleportRequestSendEvent event = new TeleportRequestSendEvent(teleport);
            Bukkit.getPluginManager().callEvent(event);

            // Check if the event was cancelled before continuing
            if (event.isCancelled()) {
                continue;
            }

            // Actually do things now
            PlayerUtil.getProfile(receiverID).setPendingTeleport(event.getTeleport());
            BukkitUtil.sendMessage(receiver, Message.format("commands.teleportation.info.pending_tprhere_request", player.getName()));
            sent++;
        }

        BukkitUtil.sendMessage(player, Message.format("commands.teleportation.info.request_sent_multiple", sent));
        return true;
    }
}
