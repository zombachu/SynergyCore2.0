package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.listeners.PacketListeners;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "baz",
        aliases = {"hello", "boing"},
        permission = "syn.test.foo.baz",
        usage = "/test foo baz {asdf}",
        description = "10/10 best command",
        parentCommandName = "test foo"
)
public class BazCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        if (args[0].equalsIgnoreCase("true")) {
            PacketListeners.entityMetadataDebuggerEnabled = true;
            BukkitUtil.sendMessage(sender, "Set entityMetadataDebuggerEnabled to true");
        } else if (args[0].equalsIgnoreCase("false")) {
            PacketListeners.entityMetadataDebuggerEnabled = false;
            BukkitUtil.sendMessage(sender, "Set entityMetadataDebuggerEnabled to false");
        }

/*
        if (Bukkit.getOnlinePlayers().size() > 0) {
            BukkitUtil.sendMessage(sender, "Error, players are online");
            return false;
        }

        int changed = 0;

        // Populate totalPlaytime and totalAfktime
        for (OfflinePlayer offlinePlayer : Bukkit.getOfflinePlayers()) {
            UUID pID = offlinePlayer.getUniqueId();

            // Get the MCP with the relevant values
            MinecraftProfile mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "wp", "tpt", "tat");

            // If they don't have a MCP for whatever reason then skip them
            if (mcp == null) {
                continue;
            }

            List<WorldGroupProfile> wgps = mcp.getPartialWorldGroupProfiles("li", "lo", "pt", "at");

            // Update the mcp's totalPlaytime
            long totalPlaytime = 0;
            for (WorldGroupProfile wgp : wgps) {
                totalPlaytime += wgp.getPlaytime();
            }
            mcp.setTotalPlaytime(totalPlaytime);

            // Update the mcp's totalAfktime
            long totalAfktime = 0;
            for (WorldGroupProfile wgp : wgps) {
                totalAfktime += wgp.getAfktime();
            }
            mcp.setTotalAfktime(totalAfktime);

            changed++;
        }

        BukkitUtil.sendMessage(sender, "Transitioned " + changed + " profiles.");*/
        return true;
    }
}
