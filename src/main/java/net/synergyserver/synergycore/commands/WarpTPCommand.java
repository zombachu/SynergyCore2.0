package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Warp;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CommandDeclaration(
        commandName = "tp",
        aliases = {"goto", "warp"},
        permission = "syn.warp.tp",
        usage = "/warp tp <warp>",
        description = "Teleports you to a warp. If you have access to multiple warps with the same name, you will need to provide the full owner#warp name of the warp.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "warp"
)
public class WarpTPCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID pID = player.getUniqueId();

        // Attempt to get the targeted warp
        String name = args[0];
        List<Warp> warps = Warp.getWarps(name, player.getUniqueId(), WarpCommand.WarpOwnerType::getTargetPriority, false, false);

        // Return if an error was encountered
        if (warps == null) {
            return false;
        }

        // If no warps were found with the given name then give them an error
        if (warps.size() == 0) {
            BukkitUtil.sendMessage(player, Message.format("commands.warp.error.warp_not_found", name));
            return false;
        }

        // If a warp was found that is owned by the player or if it's the only warp then assume it's the target and teleport the player to it
        Warp highestPriorityWarp = warps.get(0);
        if (warps.size() == 1 || highestPriorityWarp.getOwner().equals(pID)) {
            boolean success = player.teleport(highestPriorityWarp.getLocation(), PlayerTeleportEvent.TeleportCause.COMMAND);
            if (success) {
                BukkitUtil.sendMessage(player, Message.format("commands.warp.tp.info.teleported", highestPriorityWarp.getName()));
                // Increment the times used
                warps.get(0).incrementUses();
            }
            return true;
        } else {
            // Otherwise, since more than one warp was found tell the player to be more specific
            List<String> warpNames = warps.stream().map(Warp::getFullName).collect(Collectors.toList());
            String matchedWarpsMessage = Message.createFormattedList(
                    warpNames,
                    Message.getColor("c5"),
                    Message.get("info_colored_lists.grammar_color")
            );

            BukkitUtil.sendMessage(sender, Message.format("commands.warp.tp.info.matched_multiple", warpNames.size(), name));
            BukkitUtil.sendMessage(sender, Message.format("commands.warp.tp.detail.matched_warps", matchedWarpsMessage));
            return false;
        }
    }
}
