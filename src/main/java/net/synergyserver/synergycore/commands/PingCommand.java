package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_20_R2.entity.CraftPlayer;

import java.util.UUID;

@CommandDeclaration(
        commandName = "ping",
        permission = "syn.ping",
        usage = "/ping [player]",
        description = "Checks yours or another player's latency.",
        maxArgs = 1
)
public class PingCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        boolean isSelf = false;
        CraftPlayer player;

        if (args.length == 0) {
            // If args.length is 0 then that means that the sender is required to be a player checking their own ping
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                BukkitUtil.sendMessage(sender, Message.get("commands.error.sender_type_requires_player"));
                return false;
            }

            isSelf = true;
            // Cast the sender (who has to be a player) to a CraftPlayer
            player = (CraftPlayer) sender;
        } else {
            // Get the referenced player and cast them to a CraftPlayer
            UUID pID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));

            // If no player was found then give the sender an error message
            if (pID == null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
                return false;
            }
            player = (CraftPlayer) Bukkit.getPlayer(pID);
        }

        int ping = player.getPing();

        // If the pinged player is themselves, then give them a special message
        if (isSelf) {
            BukkitUtil.sendMessage(sender, Message.format("commands.ping.info.self", ping));
            return true;
        }

        BukkitUtil.sendMessage(sender, Message.format("commands.ping.info.others", player.getName(), ping));
        return true;
    }

}
