package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

@CommandDeclaration(
        commandName = "delete",
        aliases = {"unset", "remove"},
        permission = "syn.home.delete",
        usage = "/home delete <name>",
        description = "Deletes a previously set home in your current world group.",
        maxArgs = 1,
        minArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "home"
)
public class HomeDeleteCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();

        HashMap<String, SerializableLocation> homes = wgp.getHomes();
        String name = args[0].toLowerCase();

        // If they don't have a home with the given name then give them an error
        if (homes == null || !homes.containsKey(name)) {
            BukkitUtil.sendMessage(player, Message.format("commands.home.delete.error.home_not_found", name));
            return false;
        }

        // Remove the home and update the field in the database
        homes.remove(name);
        wgp.setHomes(homes);

        // Give the player feedback and return true
        BukkitUtil.sendMessage(player, Message.format("commands.home.delete.info.deleted_home", name));
        return true;
    }
}
