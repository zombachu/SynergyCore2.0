package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "echo",
        permission = "syn.echo",
        usage = "/echo [message]",
        description = "Echos back a message."
)
public class EchoCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // If they didn't supply a message then echo back the default one
        if (args.length == 0) {
            BukkitUtil.sendMessage(sender, Message.get("commands.echo.default"));
            return true;
        }

        // Format the message and send it back
        String message = Message.translateCodes(String.join(" ", args), sender, "syn.echo");
        BukkitUtil.sendMessage(sender, message);
        return true;
    }
}
