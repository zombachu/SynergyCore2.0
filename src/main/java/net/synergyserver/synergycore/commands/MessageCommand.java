package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.PrivateMessage;
import net.synergyserver.synergycore.PrivateMessageType;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.PrivateMessageSendEvent;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "msg",
        aliases = {"m", "pm", "t", "tell", "w", "whisper"},
        permission = "syn.message",
        usage = "/msg <player> <message>",
        description = "Sends a private message to the specified player",
        minArgs = 2,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE, SenderType.BLOCK}
)
public class MessageCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID senderID;
        String senderName;
        UUID receiverID;
        String receiverName;

        // Get the UUID of the sender
        if (SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
            Player player = (Player) sender;
            senderID = player.getUniqueId();
            senderName = PlayerUtil.getNameColor(player) + player.getName();
        } else if (SenderType.getSenderType(sender).equals(SenderType.CONSOLE) || SenderType.getSenderType(sender).equals(SenderType.BLOCK)) {
            senderID = SynergyCore.SERVER_ID;
            senderName = Message.get("server.prefix_color") + Message.get("server.name");
        } else {
            BukkitUtil.sendMessage(sender, Message.get("commands.error.wrong_sender_type"));
            return false;
        }

        // Verify that the player specified is valid
        receiverID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"), true);
        if (receiverID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Set receiverName
        if (receiverID.equals(SynergyCore.SERVER_ID)) {
            receiverName = Message.get("server.prefix_color") + Message.get("server.name");
        } else {
            Player receiver = Bukkit.getPlayer(receiverID);
            receiverName = PlayerUtil.getNameColor(receiver) + receiver.getName();
        }

        // Build the message and format it
        String message = String.join(" ", (String[]) ArrayUtils.remove(args, 0));
        message = Message.translateCodes(message, Message.get("commands.message.reset"), sender, "syn.message");

        // Emit an event
        PrivateMessage privateMessage = new PrivateMessage(senderID, receiverID, System.currentTimeMillis(), message, PrivateMessageType.NORMAL);
        PrivateMessageSendEvent event = new PrivateMessageSendEvent(privateMessage);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            return true;
        }

        // Send feedback to the sender and update their lastMessaged if they're a player
        BukkitUtil.sendMessage(sender, Message.format("commands.message.sender", receiverName, message));
        if (sender instanceof Player) {
            PlayerUtil.getProfile(senderID).setLastMessaged(receiverID);
        }

        // Send the message to the receiver and if they're a player, update their lastMessaged
        if (receiverID.equals(SynergyCore.SERVER_ID)) {
            BukkitUtil.sendMessage(Bukkit.getConsoleSender(), Message.format("commands.message.receiver", senderName, message));
        } else {
            Player receiver = Bukkit.getPlayer(receiverID);
            receiver.sendMessage(Message.format("commands.message.receiver", senderName, message));
            PlayerUtil.getProfile(receiverID).setLastMessaged(senderID);
        }

        return true;
    }
}
