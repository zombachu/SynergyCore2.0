package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.UUID;

@CommandDeclaration(
        commandName = "message",
        aliases = {"m", "msg", "chat"},
        permission = "syn.sudo.message",
        usage = "/sudo message <player> <message>",
        description = "Forces a player to send a message.",
        minArgs = 2,
        parentCommandName = "sudo"
)
public class SudoMessageCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // Verify that the player specified is valid
        UUID pID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));
        if (pID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Get the player
        Player sudoedPlayer = Bukkit.getPlayer(pID);

        // Check if the targeted player is unable to be sudoed
        if (sudoedPlayer.hasPermission("syn.sudo.exempt")) {
            BukkitUtil.sendMessage(sender, Message.format("commands.sudo.error.exempt", sudoedPlayer.getName()));
            return false;
        }

        // Assemble the message
        String message = String.join(" ", Arrays.asList(args).subList(1, args.length));

        // Execute the command and give feedback
        sudoedPlayer.chat(message);
        BukkitUtil.sendMessage(sender, Message.format("commands.sudo.message.info.success", sudoedPlayer.getName(), message));
        return true;
    }
}
