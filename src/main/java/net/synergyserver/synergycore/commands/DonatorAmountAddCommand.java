package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.DonatorStatusUpdateEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "add",
        permission = "syn.donator.amount",
        usage = "/donator amount add <player> <amount in dollars>",
        description = "Adds the given amount to the player's total donation amount.",
        minArgs = 2,
        maxArgs = 2,
        parentCommandName = "donator amount"
)
public class DonatorAmountAddCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID donatorID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        // Give the sender an error if the name provided isn't valid
        if (donatorID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Give the sender an error if the amount provided isn't a number
        if (!MathUtil.isDouble(args[1])) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.not_a_number", args[1]));
            return false;
        }

        long amountInCents = Math.round(Double.parseDouble(args[1]) * 100);

        // Get their current MinecraftProfile
        MinecraftProfile mcp;
        if (PlayerUtil.isOnline(donatorID)) {
            mcp = PlayerUtil.getProfile(donatorID);
        } else {
            mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, donatorID, "sid");
        }

        // Add the amount to the player's SynUser profile
        SynUser synUser = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());
        synUser.setAmountDonated(synUser.getAmountDonated() + (int) amountInCents);

        // Call an event
        DonatorStatusUpdateEvent event = new DonatorStatusUpdateEvent(synUser);
        Bukkit.getPluginManager().callEvent(event);

        // Give the sender feedback and broadcast a message
        BukkitUtil.sendMessage(sender, Message.format("commands.donator.amount.add.info.success", args[1], PlayerUtil.getName(donatorID)));
        for (Player p : Bukkit.getOnlinePlayers()) {
            BukkitUtil.sendMessage(p, Message.format("commands.donator.amount.add.info.broadcast", PlayerUtil.getName(donatorID), args[1]));
        }
        BukkitUtil.sendMessage(Bukkit.getConsoleSender(), Message.format("commands.donator.amount.add.info.broadcast", PlayerUtil.getName(donatorID), args[1]));
        return true;
    }
}
