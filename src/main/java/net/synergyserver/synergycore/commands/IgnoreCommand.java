package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "ignore",
        aliases = "block",
        permission = "syn.ignore",
        usage = "/ignore <player>",
        description = "Ignores the specified player, blocking their messages and requests to you.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class IgnoreCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID toIgnore = PlayerUtil.getUUID(args[0], true, player.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (toIgnore == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // If they're trying to ignore themselves then give the sender an error message
        if (toIgnore.equals(player.getUniqueId())) {
            BukkitUtil.sendMessage(player, Message.get("commands.ignore.error.self"));
            return false;
        }

        String toIgnoreName = PlayerUtil.getName(toIgnore);
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        BukkitUtil.acceptOnMainThread(
                SynergyCore.getPlugin(),
                PlayerUtil.hasPermissionAsync(toIgnore, "syn.ignore.exempt"),
                hasPermission -> {
                    // If the specified player cannot be ignored then give the sender an error message
                    if (hasPermission) {
                        BukkitUtil.sendMessage(player, Message.format("commands.ignore.error.exempt", toIgnoreName));
                        return;
                    }

                    // If they're already ignoring the player then give the sender an error message
                    if (mcp.hasIgnored(toIgnore)) {
                        BukkitUtil.sendMessage(player, Message.format("commands.ignore.error.already_ignored", toIgnoreName));
                        return;
                    }

                    // Ignore the player and give feedback
                    mcp.addIgnoredPlayer(toIgnore);
                    BukkitUtil.sendMessage(player, Message.format("commands.ignore.info.ignore_success", toIgnoreName));
                });

        // Treat the command as handled for synchronous return even if it ends up failing
        return true;
    }
}
