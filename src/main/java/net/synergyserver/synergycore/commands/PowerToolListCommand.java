package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.PowerTool;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;

@CommandDeclaration(
        commandName = "list",
        aliases = "l",
        permission = "syn.powertool.list",
        usage = "/powertool list [all]",
        description = "Lists all commands for the currently held PowerTool, or all items that have PowerTool functionality.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "powertool"
)
public class PowerToolListCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If they're listing all powertools
        if (args.length == 1) {
            // Give an error if the argument provided isn't "all"
            if (!args[0].equalsIgnoreCase("all")) {
                BukkitUtil.sendMessage(player, Message.format("commands.error.incorrect_syntax", getUsage()));
                return false;
            }

            HashMap<String, PowerTool> powerTools = mcp.getPowerTools();

            // If they have no powertools then notify then
            if (powerTools.isEmpty()) {
                BukkitUtil.sendMessage(player, Message.format("command.powertool.list.info.no_powertools"));
                return true;
            }

            // List each powertool and the number of commands it has
            BukkitUtil.sendMessage(player, Message.format("commands.powertool.list.all_powertools.header", powerTools.size()));
            powerTools.forEach((key, powerTool) -> {
                BukkitUtil.sendMessage(player, Message.format("commands.powertool.list.all_powertools.detail.item",
                        key,
                        powerTool.getCommands().size()
                ));
            });
            return true;
        }
        // If they're only listing the commands of the current powertool
        else {
            ItemStack item = player.getInventory().getItemInMainHand();

            // Give an error if the player isn't holding an item
            if (item == null || item.getType().equals(Material.AIR)) {
                BukkitUtil.sendMessage(player, Message.get("commands.powertool.error.no_item"));
                return false;
            }

            HashMap<String, PowerTool> powerTools = mcp.getPowerTools();
            String powerToolKey = ItemUtil.itemTypeToString(item);
            PowerTool powerTool = powerTools.get(powerToolKey);

            // If there's no commands on the held item then notify then
            if (powerTool == null) {
                BukkitUtil.sendMessage(player, Message.format("commands.powertool.list.single_powertool.info.no_commands"));
                return true;
            }

            List<String> commands = powerTool.getCommands();

            // List the commands of the currently held powertool
            BukkitUtil.sendMessage(player, Message.format("commands.powertool.list.single_powertool.header", commands.size()));
            for (int i = 0; i < commands.size(); i++) {
                BukkitUtil.sendMessage(player, Message.format("commands.powertool.list.single_powertool.item", i + 1, commands.get(i)));
            }

            return true;
        }
    }
}