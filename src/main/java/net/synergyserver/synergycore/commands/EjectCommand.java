package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "eject",
        permission = "syn.eject",
        usage = "/eject [player] [-silent]",
        description = "Ejects a player that is spectating you. " +
                "If no player is specified then all players spectating you will be ejected.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parseCommandFlags = true
)
public class EjectCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        // Determine whether ejected players should be notified
        boolean isSilent = false;
        if (flags.hasFlag("-s", "-silent")) {
            isSilent = true;
        }

        // If they didn't specify a player, kick everyone riding them out
        if (args.length == 0) {
            int ejected = PlayerUtil.ejectSpectatingPlayers(player, isSilent);

            // Give the player feedback
            BukkitUtil.sendMessage(player, Message.format("commands.eject.info.ejected_all", ejected));
            return true;
        }

        // Otherwise eject the targeted player
        UUID pID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (pID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }
        Player spectator = Bukkit.getPlayer(pID);

        // If the target player has override permissions, give the player an error
        if (spectator.hasPermission("syn.setting.allow-spectators.override")) {
            BukkitUtil.sendMessage(player, Message.format("commands.eject.error.overridden", spectator.getName()));
            return false;
        }

        // If the target player isn't spectating the player, give the player an error
        if (spectator.getSpectatorTarget() == null || !spectator.getSpectatorTarget().equals(player)) {
            BukkitUtil.sendMessage(player, Message.format("commands.eject.error.target_not_spectating", spectator.getName()));
            return false;
        }

        // Eject the target and give them feedback if the operation was not silent
        spectator.setSpectatorTarget(null);
        if (!isSilent) {
            BukkitUtil.sendMessage(spectator, Message.format("events.spectate.info.ejected", player.getName()));
        }

        // Give the player feedback
        BukkitUtil.sendMessage(player, Message.format("commands.eject.info.ejected_single", spectator.getName()));
        return true;
    }
}
