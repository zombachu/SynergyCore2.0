package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "list",
        aliases = "l",
        permission = "syn.snoop.list",
        usage = "/snoop list",
        description = "Lists all players being snooped on.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER,
        parentCommandName = "snoop"
)
public class SnoopListCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player.getUniqueId());

        // Notify the player if they aren't snooping on anyone
        if (mcp.getSnoopedPlayers().isEmpty()) {
            BukkitUtil.sendMessage(player, Message.format("commands.snoop.list.info.no_snooped_players"));
            return true;
        }

        // Create the list of snooped players
        String snoopedList = Message.createList(
                PlayerUtil.getNames(mcp.getSnoopedPlayers()),
                Message.get("info_colored_lists.item_color_1"),
                Message.get("info_colored_lists.item_color_2"),
                Message.get("info_colored_lists.grammar_color")
        );

        // Send the list to the user
        BukkitUtil.sendMessage(player, Message.format("commands.snoop.list.info.snooped_players", snoopedList));
        return true;
    }
}
