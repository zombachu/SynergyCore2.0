package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.Warp;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CommandDeclaration(
        commandName = "info",
        permission = "syn.warp.info",
        usage = "/warp info <warp>",
        description = "Displays information about a warp.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "warp"
)
public class WarpInfoCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID pID = player.getUniqueId();

        // Attempt to get the targeted warp
        String name = args[0];
        List<Warp> warps = Warp.getWarps(name, player.getUniqueId(), WarpCommand.WarpOwnerType::getTargetPriority, false, false);

        // Return if an error was encountered
        if (warps == null) {
            return false;
        }

        // If no warps were found with the given name then give them an error
        if (warps.size() == 0) {
            BukkitUtil.sendMessage(player, Message.format("commands.warp.error.warp_not_found", name));
            return false;
        }

        // If a warp was found that is owned by the player or if it's the only warp then assume it's the target and give the player info about it
        Warp highestPriorityWarp = warps.get(0);
        if (warps.size() == 1 || highestPriorityWarp.getOwner().equals(pID)) {
            // Use the name used (with proper capitalization) in the message header
            String nameUsed = name.contains("#") ? highestPriorityWarp.getFullName() : highestPriorityWarp.getName();
            BukkitUtil.sendMessage(player, Message.format("commands.warp.info.info.header", nameUsed));

            // Send publicly-available details
            BukkitUtil.sendMessage(player, Message.format("commands.warp.info.detail.owner", PlayerUtil.getName(highestPriorityWarp.getOwner())));
            Location loc = highestPriorityWarp.getLocation();
            BukkitUtil.sendMessage(player, Message.format("commands.warp.info.detail.world", loc.getWorld().getName(), SynergyCore.getWorldGroupName(loc.getWorld().getName())));
            BukkitUtil.sendMessage(player, Message.format("commands.warp.info.detail.location", loc.getX(), loc.getY(), loc.getZ()));
            if (player.getWorld().equals(loc.getWorld())) {
                BukkitUtil.sendMessage(player, Message.format("commands.warp.info.detail.distance", player.getLocation().distance(loc)));
            }
            BukkitUtil.sendMessage(player, Message.format("commands.warp.info.detail.privacy", highestPriorityWarp.getPrivacy().name().toLowerCase()));

            // Send details only visible to the owner or those with admin perms
            if (highestPriorityWarp.getOwner().equals(pID) || player.hasPermission("syn.warp.info.admin")) {
                if (highestPriorityWarp.getPrivacy().equals(Warp.PrivacyLevel.PRIVATE) && !highestPriorityWarp.getWhitelist().isEmpty()) {
                    String whitelisted = Message.createFormattedList(
                            highestPriorityWarp.getWhitelist().stream().map(PlayerUtil::getName).collect(Collectors.toList()),
                            Message.getColor("c4"),
                            Message.get("info_colored_lists.grammar_color")
                    );
                    BukkitUtil.sendMessage(player, Message.format("commands.warp.info.detail.whitelisted", whitelisted));
                } else if (highestPriorityWarp.getPrivacy().equals(Warp.PrivacyLevel.PUBLIC) && !highestPriorityWarp.getBlacklist().isEmpty()) {
                    String blacklisted = Message.createFormattedList(
                            highestPriorityWarp.getBlacklist().stream().map(PlayerUtil::getName).collect(Collectors.toList()),
                            Message.getColor("c4"),
                            Message.get("info_colored_lists.grammar_color")
                    );
                    BukkitUtil.sendMessage(player, Message.format("commands.warp.info.detail.blacklisted", blacklisted));
                }
            }

            BukkitUtil.sendMessage(player, Message.format("commands.warp.info.detail.uses", highestPriorityWarp.getUses()));
            return true;
        } else {
            // Otherwise, since more than one warp was found tell the player to be more specific
            List<String> warpNames = warps.stream().map(Warp::getFullName).collect(Collectors.toList());
            String matchedWarpsMessage = Message.createFormattedList(
                    warpNames,
                    Message.getColor("c5"),
                    Message.get("info_colored_lists.grammar_color")
            );

            BukkitUtil.sendMessage(sender, Message.format("commands.warp.info.info.matched_multiple", warpNames.size(), name));
            BukkitUtil.sendMessage(sender, Message.format("commands.warp.info.detail.matched_warps", matchedWarpsMessage));
            return false;
        }
    }
}
