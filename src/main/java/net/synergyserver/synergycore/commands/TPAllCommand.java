package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Teleport;
import net.synergyserver.synergycore.TeleportType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.TeleportDirectEvent;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.UUID;

@CommandDeclaration(
        commandName = "tpall",
        permission = "syn.tpall",
        usage = "/tpall",
        description = "Teleports everyone to you.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class TPAllCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        int teleported = 0;
        for (Player teleporter : Bukkit.getOnlinePlayers()) {
            if (player.equals(teleporter)) {
                continue;
            }
            if (!PlayerUtil.canSee(player, teleporter.getUniqueId())) {
                continue;
            }

            UUID teleporterID = teleporter.getUniqueId();

            // Emit an event
            Teleport teleport = new Teleport(player.getUniqueId(), teleporterID,
                    player.getLocation(), TeleportType.EVERYONE_TO_SENDER);
            TeleportDirectEvent event = new TeleportDirectEvent(teleport);
            Bukkit.getPluginManager().callEvent(event);

            // Check if the event was cancelled before continuing
            if (event.isCancelled()) {
                continue;
            }

            // Actually do things now
            boolean success = teleporter.teleport(player, PlayerTeleportEvent.TeleportCause.COMMAND);
            if (success) {
                BukkitUtil.sendMessage(teleporter, Message.format("commands.teleportation.info.teleporting_teleporter", player.getName()));
                teleported++;
            }
        }

        // Give the teleportee the appropriate message
        BukkitUtil.sendMessage(player, Message.format("commands.teleportation.info.teleporting_multiple", teleported));
        return true;
    }

}
