package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "settings",
        aliases = {"setting", "toggle", "toggles", "flag", "flags"},
        permission = "syn.settings",
        usage = "/settings [set|list|info|gui|preset]",
        description = "Main command for managing your personal settings.",
        validSenders = SenderType.PLAYER,
        executeIfInvalidSubCommand = true
)
public class SettingsCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // Only execute if there's no arguments given
        if (args.length > 0) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.incorrect_syntax", getUsage()));
            return false;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile((Player) sender);
        SettingsGUICommand settingsGUICommand = (SettingsGUICommand) getSubCommands().get("gui");

        // Open the GUI, if the sender has permission to and the setting is active
        if (sender.hasPermission(settingsGUICommand.getPermission()) && CoreToggleSetting.SETTINGS_SHORTCUT.getValue(mcp)) {
            return settingsGUICommand.validate(sender, args);
        }

        // Otherwise give them the correct usage and return false
        BukkitUtil.sendMessage(sender, Message.format("commands.error.incorrect_syntax", getUsage()));
        return false;
    }
}
