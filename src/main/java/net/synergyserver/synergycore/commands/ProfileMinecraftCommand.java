package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.Rank;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CommandDeclaration(
        commandName = "minecraft",
        aliases = "mc",
        permission = "syn.profile.minecraft",
        usage = "/profile minecraft [player] [-wg <worldgroup>]",
        description = "Gets the Minecraft profile for a player.",
        maxArgs = 1,
        parseCommandFlags = true,
        parentCommandName = "profile"
)
public class ProfileMinecraftCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        DataManager dm = DataManager.getInstance();
        MinecraftProfile mcp;
        String correctedWorldGroupName = "";

        if (flags.hasFlag("-worldgroup", "-wg")) {
            String worldGroupName = flags.getFlagValue("-worldgroup", "-wg");

            // Check to make sure the world group is valid
            if (worldGroupName.isEmpty()) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.invalid_world_group_name", ""));
                return false;
            }

            // Correct the capitalization for use with other methods
            correctedWorldGroupName = SynergyCore.getExactWorldGroupName(worldGroupName);

            if (!SynergyCore.hasWorldGroupName(correctedWorldGroupName)) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.invalid_world_group_name", worldGroupName));
                return false;
            }
        }

        if (args.length == 0) {
            // If the sender isn't a player, then an argument is required
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                BukkitUtil.sendMessage(sender, Message.get("commands.error.wrong_sender_type"));
                return false;
            }

            mcp = PlayerUtil.getProfile((Player) sender);
        } else {
            UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"), false, true);

            if (pID == null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
                return false;
            }

            // Get the most current profile
            if (PlayerUtil.isOnline(pID)) {
                mcp = PlayerUtil.getProfile(pID);
            } else {
                mcp = DataManager.getInstance().getDataEntity(MinecraftProfile.class, pID);
            }
        }

        if (!correctedWorldGroupName.isEmpty()) {
            sendWorldGroupProfileInfo(sender, mcp, correctedWorldGroupName);
        } else {
            BukkitUtil.acceptBothOnMainThread(
                    SynergyCore.getPlugin(),
                    PlayerUtil.getPrimaryRankAsync(mcp.getID()),
                    PlayerUtil.getDonatorRankAsync(mcp.getID()),
                    (primaryRank, donatorRank) -> sendMinecraftProfileInfo(sender, mcp, primaryRank, donatorRank));
        }
        return true;
    }

    private void sendWorldGroupProfileInfo(CommandSender sender, MinecraftProfile mcp, String worldGroupName) {
        String color1 = Message.get("info_colored_lists.item_color_1");
        String grammarColor = Message.get("info_colored_lists.grammar_color");
        WorldGroupProfile wgp = mcp.getWorldGroupProfile(worldGroupName);

        // Send the header
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.info.header", mcp.getCurrentName(), worldGroupName));

        // Build and send the online status detail
        String onlineStatus = wgp.isInUse() ? ChatColor.GREEN + "Online" : ChatColor.DARK_RED + "Offline";
        long time = wgp.isInUse() ? wgp.getLastLogIn() : wgp.getLastLogOut();
        String onlineTimeMessage = Message.getTimeMessage(System.currentTimeMillis() - time, 3, 1, color1, grammarColor);
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.world_group.detail.online_status", onlineStatus, onlineTimeMessage));

        // Build and send the homes detail
        if (!wgp.getHomes().isEmpty()) {
            String homesMessage = Message.createFormattedList(
                    wgp.getHomes().keySet(),
                    color1,
                    grammarColor
            );
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.world_group.detail.homes", homesMessage));
        }

        // Build and send the personal time detail
        if (wgp.getPersonalTime() != null) {
            String personalTimeMessage = TimeUtil.formatGameTime(wgp.getPersonalTime());
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.world_group.detail.personal_time", personalTimeMessage));
        }

        // Build and send the personal weather detail
        if (wgp.getPersonalWeather() != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.world_group.detail.personal_weather", wgp.getPersonalWeather().getDisplayText()));
        }
    }

    private void sendMinecraftProfileInfo(CommandSender sender, MinecraftProfile mcp, Rank primaryRank, Rank donatorRank) {
        String color1 = Message.get("info_colored_lists.item_color_1");
        String grammarColor = Message.get("info_colored_lists.grammar_color");
        Player player = mcp.getPlayer();
        SynUser synUser = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());

        // Send the header
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.info.header", mcp.getCurrentName(), "Minecraft"));

        // Send the display name detail if they're online
        if (player != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.display_name", player.getDisplayName()));
        }

        BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.uuid", mcp.getID().toString()));

        // Build and send the online status detail
        String onlineStatus = mcp.isOnline() ? ChatColor.GREEN + "Online" : ChatColor.DARK_RED + "Offline";
        long time = mcp.isOnline() ? mcp.getLastLogIn() : mcp.getLastLogOut();
        String onlineTimeMessage = Message.getTimeMessage(System.currentTimeMillis() - time, 3, 1, color1, grammarColor);
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.online_status", onlineStatus, onlineTimeMessage));

        // Build and send the known names detail
        String knownNamesMessage = Message.createFormattedList(mcp.getKnownNames(), Message.getColor("c4"), grammarColor);
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.previous_names", knownNamesMessage));

        // Build and send the ips detail
        String recentIPsMessage = Message.createFormattedList(mcp.getRecentIPs(), color1, grammarColor);
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.recent_ips", recentIPsMessage));

        // Build and send the possible alts detail
        List<MinecraftProfile> possibleAlts = mcp.getPartialPossibleAlts("n");
        if (possibleAlts.size() > 1) {
            String possibleAltsMessage = Message.createFormattedList(
                    possibleAlts.stream().map(MinecraftProfile::getCurrentName).collect(Collectors.toList()),
                    color1,
                    grammarColor
            );
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.possible_alts", possibleAltsMessage));
        }

        // Send the rank detail
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.current_rank", primaryRank.getDisplay()));

        // Send the vanish detail if they're online
        if (player != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.vanish_status", PlayerUtil.isVanished(player) ? "Vanished" : "Not vanished"));
        }

        // Send the health detail if they're online
        if (player != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.health", player.getHealth(), player.getHealthScale()));
        }

        // Send the health detail if they're online
        if (player != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.hunger", player.getFoodLevel(), player.getSaturation()));
        }

        // Send the exp detail if they're online
        if (player != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.exp", player.getTotalExperience(), player.getLevel()));
        }

        // Send the flying detail if they're online
        if (player != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.flying_status", player.isFlying() ? "Flying" : "Not flying"));
        }

        // Send the walking speed detail if they're online
        if (player != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.walking_speed", player.getWalkSpeed()));
        }

        // Send the flying speed detail if they're online
        if (player != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.flying_speed", player.getFlySpeed()));
        }

        if (player != null){
            String gmString = player.getGameMode().toString();
            String formattedGm = gmString.substring(0,1) + gmString.substring(1).toLowerCase();
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.gamemode", formattedGm));
        }

        // Build and send the last messaged detail if they've messaged someone in the time that they've been online
        if (mcp.getLastMessaged() != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.last_messaged", PlayerUtil.getName(mcp.getLastMessaged())));
        }

        // Build and send the /back location detail, if they're online
        Location loc = mcp.getPartialWorldGroupProfile(mcp.getLastWorldGroup(), "ll").getLastLocation().getLocation();
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.back_location", loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));

        // Build and send the donator details
        if (synUser.getAmountDonated() > 0) {
            int dollarsDonated = synUser.getAmountDonated() / 100;
            int centsDonated = synUser.getAmountDonated() % 100;
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.donator_amount", dollarsDonated, centsDonated));

            if (donatorRank != null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.donator_type", donatorRank.getDisplay()));
            }
        }

        // Build and send the ignored players detail
        if (!mcp.getIgnoredPlayers().isEmpty()) {
            String ignoredPlayersMessage = Message.createFormattedList(
                    mcp.getIgnoredPlayers().stream().map(PlayerUtil::getName).collect(Collectors.toList()),
                    Message.getColor("c4"),
                    grammarColor
            );
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.ignored_players", ignoredPlayersMessage));
        }

        // Build and send the spied players details
        if (!mcp.getSpiedPlayers().isEmpty()) {
            String spiedPlayersMessage = Message.createFormattedList(
                    mcp.getSpiedPlayers().stream().map(PlayerUtil::getName).collect(Collectors.toList()),
                    Message.getColor("c4"),
                    grammarColor
            );
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.spied_players", spiedPlayersMessage));
        }
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.global_socialspy", mcp.hasGlobalSpyingEnabled() ? "Enabled" : "Disabled"));

        // Build and send the snooped players details
        if (!mcp.getSnoopedPlayers().isEmpty()) {
            String snoopedPlayersMessage = Message.createFormattedList(
                    mcp.getSnoopedPlayers().stream().map(PlayerUtil::getName).collect(Collectors.toList()),
                    Message.getColor("c4"),
                    grammarColor
            );
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.snooped_players", snoopedPlayersMessage));
        }

        // Build and send the powertools details
        if (!mcp.getPowerTools().isEmpty()) {
            String powertoolItemsMessage = Message.createFormattedList(
                    mcp.getPowerTools().keySet(),
                    color1,
                    grammarColor
            );
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.minecraft.detail.powertool_items", powertoolItemsMessage));
        }
    }
}
