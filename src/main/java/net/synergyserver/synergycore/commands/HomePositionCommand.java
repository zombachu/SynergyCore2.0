package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;


@CommandDeclaration(
        commandName = "position",
        aliases = {"pos, location, loc"},
        permission = "syn.home.position",
        usage = "/home position <home> [worldgroup]",
        description = "Prints out the physical location of the home specifed.",
        minArgs = 1,
        maxArgs = 2,
        validSenders = SenderType.PLAYER,
        parentCommandName = "home"
)

public class HomePositionCommand extends SubCommand {
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        String name = args[0].toLowerCase();
        boolean matched = false;

        // If no world group is specified, search through all world groups
        if (args.length == 1) {
            ArrayList<String> homeStrings = new ArrayList<>();
            for (WorldGroupProfile wgp : mcp.getPartialWorldGroupProfiles("n", "hs")) {
                HashMap<String, SerializableLocation> homes = wgp.getHomes();

                // Ignore this world group if it doesn't have a home with a matching name
                if (homes != null && homes.containsKey(name)) {
                    SerializableLocation home = homes.get(name);
                    homeStrings.add(Message.format("commands.home.position.info.print_home_world_group", home.getX(), home.getY(), home.getZ(), wgp.getWorldGroupName()));
                    matched = true;
                    continue;
                }
            }

            if (matched) {
                String matchedHomesList = Message.createFormattedList(
                        homeStrings,
                        Message.get("info_colored_lists.item_color_1"),
                        Message.get("info_colored_lists.item_color_2"),
                        Message.get("info_colored_lists.grammar_color")
                );
                BukkitUtil.sendMessage(player, Message.format("commands.home.position.info.print_home_list", name, matchedHomesList));
                return true;
            } else {
                BukkitUtil.sendMessage(player, Message.format("commands.home.position.error.no_world_groups", name));
                return false;
            }
        }

        // Otherwise use the specified world group
        String worldGroupName = SynergyCore.getExactWorldGroupName(args[1]);

        if (!SynergyCore.hasWorldGroupName(worldGroupName)) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.invalid_world_group_name", args[1]));
            return false;
        }

        // If they don't have data for the world group then give the player a message
        if (!mcp.hasWorldGroupProfile(worldGroupName)) {
            BukkitUtil.sendMessage(player, Message.format("commands.home.position.error.home_not_found", name));
            return false;
        }

        WorldGroupProfile wgp = mcp.getPartialWorldGroupProfile(worldGroupName, "hs");
        HashMap<String, SerializableLocation> homes = wgp.getHomes();

        // If they don't have a home with that name then give the player a message
        if (homes == null || !homes.containsKey(name)) {
            BukkitUtil.sendMessage(player, Message.format("commands.home.position.error.home_not_found", name));
            return false;
        }

        BukkitUtil.sendMessage(player, Message.format("commands.home.position.info.print_home", name, homes.get(name).getX(), homes.get(name).getY(), homes.get(name).getZ()));
        return true;
    }
}
