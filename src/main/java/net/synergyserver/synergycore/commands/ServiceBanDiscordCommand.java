package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.DiscordUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;

import java.util.UUID;

@CommandDeclaration(
        commandName = "discord",
        aliases = "dis",
        usage = "/serviceban discord <player>",
        description = "Bans a given user from the Discord server.",
        maxArgs = 1,
        parentCommandName = "serviceban"
)
public class ServiceBanDiscordCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        if (pID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(pID);
        DataManager dm = DataManager.getInstance();
        SynUser synUser = dm.getDataEntity(SynUser.class, mcp.getSynID());
        DiscordProfile discordProfile = synUser.getDiscordProfile();

        if (discordProfile == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.service_ban.error.service_not_connected", mcp.getCurrentName(), "Discord"));
            return false;
        }

        try {
            DiscordUtil.runForMember(synUser.getDiscordID(), (member, guild) -> guild.ban(member, 0).queue());

            discordProfile.setIsBanned(true);
        } catch (ServiceOfflineException e) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.service_offline", "Discord"));
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
