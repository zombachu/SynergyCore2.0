package net.synergyserver.synergycore.commands;

import io.sponges.dubtrack4j.framework.Room;
import io.sponges.dubtrack4j.framework.Song;
import io.sponges.dubtrack4j.framework.User;
import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.profiles.DubtrackProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "dubtrack",
        aliases = {"dub", "dt"},
        permission = "syn.profile.dubtrack",
        usage = "/profile dubtrack [player]",
        description = "Gets the Dubtrack profile for a player.",
        maxArgs = 1,
        parseCommandFlags = true,
        parentCommandName = "profile"
)
public class ProfileDubtrackCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        SynUser synUser;
        MinecraftProfile mcp;

        if (args.length == 0) {
            // If the sender isn't a player, then an argument is required
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                BukkitUtil.sendMessage(sender, Message.get("commands.error.wrong_sender_type"));
                return false;
            }

            mcp = PlayerUtil.getProfile(((Player) sender));
            synUser = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());
        } else {
            UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

            if (pID == null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
                return false;
            }

            // Get the most current profile
            if (PlayerUtil.isOnline(pID)) {
                mcp = PlayerUtil.getProfile(pID);
            } else {
                mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "sid", "n");
            }

            synUser = DataManager.getInstance().getPartialDataEntity(SynUser.class, mcp.getSynID(), "dt");
        }

        String color1 = Message.get("info_colored_lists.item_color_1");
        String grammarColor = Message.get("info_colored_lists.grammar_color");

        // Send the header
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.info.header", mcp.getCurrentName(), "Dubtrack"));

        DubtrackProfile dubtrackProfile = synUser.getDubtrackProfile();
        if (dubtrackProfile != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.dubtrack.detail.display_name", dubtrackProfile.getCurrentName()));
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.dubtrack.detail.id", dubtrackProfile.getID()));

            try {
                User user = dubtrackProfile.getUser();
                Room room = SynergyCore.getDubtrackRoom();

                String onlineStatus = dubtrackProfile.isOnline() ? ChatColor.GREEN + "Online" : ChatColor.DARK_RED + "Offline";
                long time = dubtrackProfile.isOnline() ? dubtrackProfile.getLastLogIn() : dubtrackProfile.getLastLogOut();
                String onlineTimeMessage = Message.getTimeMessage(System.currentTimeMillis() - time, 3, 1, color1, grammarColor);
                BukkitUtil.sendMessage(sender, Message.format("commands.profile.dubtrack.detail.online_status", onlineStatus, onlineTimeMessage));

                BukkitUtil.sendMessage(sender, Message.format("commands.profile.dubtrack.detail.ban_status", dubtrackProfile.isBanned() ? "Banned" : "Not banned"));

                if (user != null) {
                    Song currentSong = room.getCurrentSong();
                    if (currentSong != null && currentSong.getUser() != null && currentSong.getUser().getId().equals(user.getId())) {
                        BukkitUtil.sendMessage(sender, Message.format("commands.profile.dubtrack.detail.currently_playing", currentSong.getSongInfo().getName()));
                    }

                    List<Song> roomQueue = room.getRoomQueue();
                    int queueIndex = -1;
                    for (int i = 0; i < roomQueue.size(); i++) {
                        if (roomQueue.get(i).getUser() != null && roomQueue.get(i).getUser().getId().equals(user.getId())) {
                            queueIndex = i;
                        }
                    }
                    if (queueIndex != -1) {
                        BukkitUtil.sendMessage(sender, Message.format("commands.profile.dubtrack.detail.queue_index", queueIndex  + 1));
                        BukkitUtil.sendMessage(sender, Message.format("commands.profile.dubtrack.detail.queued_song", roomQueue.get(queueIndex).getSongInfo().getName()));
                    }
                }
            } catch (IOException|ServiceOfflineException e) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.service_offline", "Dubtrack"));
            }
        } else {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.error.service_not_connected", mcp.getCurrentName(), "Dubtrack"));
            return false;
        }
        return true;
    }
}
