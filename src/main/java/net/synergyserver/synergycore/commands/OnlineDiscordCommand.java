package net.synergyserver.synergycore.commands;

import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

@CommandDeclaration(
        commandName = "discord",
        aliases = {"dis"},
        permission = "syn.online.dubtrack",
        usage = "/online <discord|dubtrack>",
        description = "Lists the users online on a specific service.",
        maxArgs = 0,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE},
        parentCommandName = "online"
)
public class OnlineDiscordCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        try {
            Guild server = SynergyCore.getDiscordServer();

            List<Member> users = server.getMembers();
            List<String> usernames = new ArrayList<>();

            for (Member user : users) {
                // Only add online users
                if (user.getOnlineStatus() != OnlineStatus.ONLINE) {
                    continue;
                }

                // Add the username to the list and reset the color to gray for the punctuation
                usernames.add(user.getEffectiveName() + ChatColor.GRAY);
            }

            String online = Message.createFormattedList(
                    usernames,
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.item_color_2"),
                    Message.get("info_colored_lists.grammar_color")
            );

            BukkitUtil.sendMessage(sender, Message.format("commands.online.list", "Discord", online));
            return true;
        } catch (ServiceOfflineException e) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.service_offline", "Discord"));
            return false;
        }
    }
}
