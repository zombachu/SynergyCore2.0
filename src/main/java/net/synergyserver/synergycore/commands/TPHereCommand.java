package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Teleport;
import net.synergyserver.synergycore.TeleportType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.TeleportDirectEvent;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.UUID;

@CommandDeclaration(
        commandName = "tphere",
        permission = "syn.tphere",
        usage = "/tphere <player>",
        description = "Teleports a player to you.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class TPHereCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID teleporterID = PlayerUtil.getUUID(args[0], false, player.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (teleporterID == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        Player teleporter = Bukkit.getPlayer(teleporterID);

        // Emit an event
        Teleport teleport = new Teleport(player.getUniqueId(), teleporterID,
                player.getLocation(), TeleportType.TO_SENDER);
        TeleportDirectEvent event = new TeleportDirectEvent(teleport);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            return true;
        }

        // Teleport the player and set the TeleportCause for use with /back
        boolean success = teleporter.teleport(player, PlayerTeleportEvent.TeleportCause.COMMAND);
        if (success) {
            // Give both parties the appropriate feedback
            BukkitUtil.sendMessage(player, Message.format("commands.teleportation.info.teleporting_teleportee", teleporter.getName()));
            BukkitUtil.sendMessage(teleporter, Message.format("commands.teleportation.info.teleporting_teleporter", player.getName()));
        }

        return true;
    }

}
