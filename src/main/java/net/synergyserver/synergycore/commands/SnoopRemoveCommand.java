package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.UUID;

@CommandDeclaration(
        commandName = "remove",
        aliases = "r",
        permission = "syn.snoop.remove",
        usage = "/snoop remove <player>",
        description = "Removes a user from the sender's snoop list.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "snoop"
)
public class SnoopRemoveCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        // Give an error if the player can't be found
        if (pID == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        HashSet<UUID> snoopedPlayers = mcp.getSnoopedPlayers();

        // Give an error if the player isn't being snooped
        if (!snoopedPlayers.contains(pID)) {
            BukkitUtil.sendMessage(player, Message.format("commands.snoop.remove.error.not_snooped", PlayerUtil.getName(pID)));
            return false;
        }

        // Add the new ID to the snoop list
        snoopedPlayers.remove(pID);
        mcp.setSnoopedPlayers(snoopedPlayers);
        BukkitUtil.sendMessage(player, Message.format("commands.snoop.remove.info.removed_player", PlayerUtil.getName(pID)));
        return true;
    }
}
