package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import java.util.UUID;

@CommandDeclaration(
        commandName = "heal",
        permission = "syn.heal",
        usage = "/heal [player]",
        description = "Heals yourself or another player.",
        maxArgs = 1
)
public class HealCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player target;
        // Target the sender if no player was provided
        if (args.length == 0) {
            // If the sender is not a player, then there needs to be a player specified
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                BukkitUtil.sendMessage(sender, Message.get("commands.error.wrong_sender_type"));
                return false;
            }

            target = (Player) sender;
        } else {
            // Give an error if the sender doesn't have permission to heal other players
            if (!sender.hasPermission("syn.heal.others")) {
                BukkitUtil.sendMessage(sender, Message.format("commands.heal.error.no_permission_others"));
                return false;
            }

            UUID targetID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));

            // Give an error if no player was found
            if (targetID == null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
                return false;
            }

            target = Bukkit.getPlayer(targetID);
        }

        // Heal the player
        target.setHealth(20);
        target.setFoodLevel(20);
        target.setSaturation(10);
        target.setFireTicks(0);

        for (PotionEffect effect : target.getActivePotionEffects()) {
            target.removePotionEffect(effect.getType());
        }

        // Give the sender feedback
        if (sender.equals(target)) {
            BukkitUtil.sendMessage(sender, Message.format("commands.heal.info.self"));
        } else {
            BukkitUtil.sendMessage(sender, Message.format("commands.heal.info.others", target.getName()));
        }
        return true;
    }


}
