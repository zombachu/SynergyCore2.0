package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

@CommandDeclaration(
        commandName = "list",
        aliases = "show",
        permission = "syn.home.list",
        usage = "/home list [all]",
        description = "Displays all homes.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "home"
)
public class HomeListCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If they didn't specify that they want all world groups, return only the homes of their current world group
        if (args.length == 0 || !args[0].equalsIgnoreCase("all")) {
            WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();

            HashMap<String, SerializableLocation> homes = wgp.getHomes();

            // If they don't have any homes then give the player a special message
            if (homes == null || homes.size() == 0) {
                BukkitUtil.sendMessage(player, Message.get("commands.home.list.info.no_homes_found"));
                return true;
            }

            // Turn the names of their homes into a list and message the player
            String homeList = Message.createList(
                    homes.keySet(),
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.item_color_2"),
                    Message.get("info_colored_lists.grammar_color")
            );
            BukkitUtil.sendMessage(player, Message.format("commands.home.list.homes", homeList));
            return true;
        }

        // Otherwise return the homes of all world groups
        int worldGroupsWithHomes = 0;

        // Loop over each WGP and print out its homes
        for (WorldGroupProfile wgp : mcp.getPartialWorldGroupProfiles("n", "hs")) {
            HashMap<String, SerializableLocation> homes = wgp.getHomes();

            // Ignore this world group if they don't have any homes in it
            if (homes == null || homes.size() == 0) {
               continue;
            }

            // Print out the world group's name as a header
            BukkitUtil.sendMessage(player, Message.format("commands.home.list.world_group", wgp.getWorldGroupName()));

            // Turn the names of their homes into a list and message the player
            String homeList = Message.createList(
                    homes.keySet(),
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.item_color_2"),
                    Message.get("info_colored_lists.grammar_color")
            );
            BukkitUtil.sendMessage(player, Message.format("commands.home.list.homes", homeList));
            worldGroupsWithHomes++;
        }

        // If no homes were listed then give the player a special message
        if (worldGroupsWithHomes == 0) {
            BukkitUtil.sendMessage(player, Message.get("commands.home.list.info.no_homes_found"));
        }
        return true;
    }
}
