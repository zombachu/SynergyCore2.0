package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.FormattedCommandAlias;
import org.bukkit.command.MultipleCommandAlias;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@CommandDeclaration(
        commandName = "command",
        aliases = {"c", "cmd"},
        permission = "syn.help.command",
        usage = "/help command <command name>",
        minArgs = 1,
        description = "Displays details about a command.",
        parentCommandName = "help"
)
public class HelpCommandCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        CommandManager cm = CommandManager.getInstance();
        String commandLabel = args[0].replaceFirst("/", "").toLowerCase();
        Map<String, Command> commands = cm.getRegisteredCommands();

        // If more than one argument was given then interpret it as a SubCommand
        if (args.length == 1) {
            // Find all matching commands that the sender has access to
            Set<Command> matchedCommands = commands.keySet()
                    .stream()
                    .filter(label -> {
                        // Keep the command if the label matches the requested label
                        // Because Bukkit also registers all aliases, this includes aliases and fallback labels too

                        if (label.equalsIgnoreCase(commandLabel)) {
                            return true;
                        } else {
                            // Strip the fallback prefix
                            String[] labelParts = label.split(":");
                            return labelParts[labelParts.length - 1].equalsIgnoreCase(commandLabel);
                        }
                    })
                    .map(commands::get)
                    .filter(Command::isRegistered)
                    .filter(command ->
                        // Keep the command if the sender has permission to use it
                        command.getPermission() == null || sender.hasPermission(command.getPermission())
                    )
                    .filter(command ->
                        // Skip the command if it's an alias from commands.yml
                        !(command instanceof FormattedCommandAlias || command instanceof MultipleCommandAlias)
                    )
                    .sorted(Comparator
                            .comparing(HelpCommand.HelpOwnerType::parseHelpOwnerType)
                            .thenComparing(cm::getOwnerName)
                            .thenComparing(Command::getName, String.CASE_INSENSITIVE_ORDER))
                    .collect(Collectors.toCollection(LinkedHashSet::new));

            if (matchedCommands.isEmpty()) {
                // Give an error if no commands were found
                BukkitUtil.sendMessage(sender, Message.format("commands.help.command.error.command_not_found", commandLabel));
                return false;
            } else if (matchedCommands.size() > 1) {
                // Tell the sender they need to be exact if more than one command was found
                List<String> matchedCommandsNames = matchedCommands.stream()
                        .sorted(Comparator
                                .comparing(HelpCommand.HelpOwnerType::parseHelpOwnerType)
                                .thenComparing(cm::getOwnerName)
                                .thenComparing(Command::getName, String.CASE_INSENSITIVE_ORDER))
                        .map(command -> "/" + cm.getFullLabel(command))
                        .collect(Collectors.toList());
                String matchedCommandsMessage = Message.createFormattedList(
                        matchedCommandsNames,
                        Message.getColor("c5"),
                        Message.get("info_colored_lists.grammar_color")
                );

                BukkitUtil.sendMessage(sender, Message.format("commands.help.command.info.matched_multiple", matchedCommands.size(), commandLabel));
                BukkitUtil.sendMessage(sender, Message.format("commands.help.command.detail.matched_commands", matchedCommandsMessage));
                return true;
            } else {
                // Display the information for the command
                Command command = matchedCommands.iterator().next();

                BukkitUtil.sendMessage(sender, Message.format("commands.help.command.info.main_header", command.getLabel()));
                String description = command.getDescription();
                if (description != null && !description.isEmpty()) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.help.command.detail.description", description));
                }
                String usage = HelpCommand.formatUsage(command);
                BukkitUtil.sendMessage(sender, Message.format("commands.help.command.detail.usage", usage));
                List<String> aliases = command.getAliases();
                if (aliases != null && !aliases.isEmpty()) {
                    // Prepend the slash and sort alphabetically
                    aliases = aliases.stream()
                            .sorted(String.CASE_INSENSITIVE_ORDER)
                            .map(alias -> "/" + alias)
                            .collect(Collectors.toList());
                    String aliasMessage = Message.createFormattedList(
                            aliases,
                            Message.getColor("c5"),
                            Message.get("info_colored_lists.grammar_color")
                    );
                    BukkitUtil.sendMessage(sender, Message.format("commands.help.command.detail.aliases", aliasMessage));
                }
                String ownerName = cm.getOwnerName(command);
                if (!ownerName.isEmpty()) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.help.command.detail.owned_by", ownerName));
                }
                return true;
            }
        } else {
            // Find all matching MainCommands that the sender has access to
            Set<MainCommand> matchedCommands = commands.keySet()
                    .stream()
                    .filter(label -> {
                        // Keep the command if the label matches the requested label
                        // Because Bukkit also registers all aliases, this includes aliases and fallback labels too

                        if (label.equalsIgnoreCase(commandLabel)) {
                            return true;
                        } else {
                            // Strip the fallback prefix
                            String[] labelParts = label.split(":");
                            return labelParts[labelParts.length - 1].equalsIgnoreCase(commandLabel);
                        }
                    })
                    .map(commands::get)
                    .filter(Command::isRegistered)
                    .filter(command ->
                            // Keep the command if the sender has permission to use it
                            command.getPermission() == null || sender.hasPermission(command.getPermission())
                    )
                    .filter(command ->
                            // Keep the command if it's a MainCommand, as we're checking for a SubCommand
                            command instanceof MainCommand
                    )
                    .map(command -> (MainCommand) command)
                    .sorted(Comparator
                            .comparing(HelpCommand.HelpOwnerType::parseHelpOwnerType)
                            .thenComparing(cm::getOwnerName)
                            .thenComparing(Command::getName, String.CASE_INSENSITIVE_ORDER))
                    .collect(Collectors.toCollection(LinkedHashSet::new));

            String commandString = String.join(" ", args).replaceFirst("/", "").toLowerCase();

            if (matchedCommands.isEmpty()) {
                // Give an error if no commands were found
                BukkitUtil.sendMessage(sender, Message.format("commands.help.command.error.command_not_found", commandString));
                return false;
            } else if (matchedCommands.size() > 1) {
                // Tell the sender they need to be exact if more than one command was found
                List<String> matchedCommandsNames = matchedCommands.stream()
                        .sorted(Comparator
                                .comparing(HelpCommand.HelpOwnerType::parseHelpOwnerType)
                                .thenComparing(cm::getOwnerName)
                                .thenComparing(Command::getName, String.CASE_INSENSITIVE_ORDER))
                        .map(command -> "/" + cm.getFullLabel(command))
                        .collect(Collectors.toList());
                String matchedCommandsMessage = Message.createFormattedList(
                        matchedCommandsNames,
                        Message.getColor("c5"),
                        Message.get("info_colored_lists.grammar_color")
                );

                BukkitUtil.sendMessage(sender, Message.format("commands.help.command.info.matched_multiple", matchedCommands.size(), commandLabel));
                BukkitUtil.sendMessage(sender, Message.format("commands.help.command.detail.matched_commands", matchedCommandsMessage));
                return true;
            } else {
                // Attempt to get the requested SubCommand
                MainCommand mainCommand = matchedCommands.iterator().next();
                SynCommand currentCommand = mainCommand;
                StringBuilder parentNameBuilder = new StringBuilder(mainCommand.getCommandName());

                for (int i = 1; i < args.length; i++) {
                    // Attempt to get the SubCommand
                    currentCommand = currentCommand.getSubCommands().get(args[i].toLowerCase());

                    if (currentCommand == null) {
                        // Give an error if no command was found
                        BukkitUtil.sendMessage(sender, Message.format("commands.help.command.error.command_not_found", commandString));
                        return false;
                    }

                    // Also build the parent name
                    if (i < args.length - 1) {
                        parentNameBuilder.append(" ").append(currentCommand.getCommandName());
                    }
                }
                String parentName = parentNameBuilder.toString();

                // Display the information for the SubCommand
                BukkitUtil.sendMessage(sender, Message.format("commands.help.command.info.sub_header", currentCommand.getCommandName(), parentName));
                String description = currentCommand.getDescription();
                if (description != null && !description.isEmpty()) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.help.command.detail.description", description));
                }
                String usage = HelpCommand.formatUsage(currentCommand);
                BukkitUtil.sendMessage(sender, Message.format("commands.help.command.detail.usage", usage));
                List<String> aliases = currentCommand.getAliases();
                if (aliases != null && !aliases.isEmpty()) {
                    // Prepend the slash and sort alphabetically
                    aliases = aliases.stream()
                            .sorted(String.CASE_INSENSITIVE_ORDER)
                            .map(alias -> (new StringBuilder("/").append(parentName).append(" ").append(alias)).toString())
                            .collect(Collectors.toList());
                    String aliasMessage = Message.createFormattedList(
                            aliases,
                            Message.getColor("c5"),
                            Message.get("info_colored_lists.grammar_color")
                    );
                    BukkitUtil.sendMessage(sender, Message.format("commands.help.command.detail.aliases", aliasMessage));
                }
                return true;
            }
        }
    }
}
