package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Page;
import net.synergyserver.synergycore.Pagination;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.TextConfig;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "rules",
        permission = "syn.rules",
        usage = "/rules",
        description = "Lists the rules of the server.",
        maxArgs = 1,
        validSenders = {SenderType.CONSOLE, SenderType.PLAYER}
)
public class RulesCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // Create a new pagination and set it as the current pagination for the player
        Pagination pagination = new Pagination(TextConfig.getText("rules.txt"), "Rules");

        // Set the current pagination for the user if they are a player
        if (SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
            PlayerUtil.getProfile((Player) sender).setCurrentPagination(pagination);
        }

        // Get the requested page from the pagination
        Page currentPage;
        if (args.length == 1) {
            // Make sure the input is a number
            if (!MathUtil.isInteger(args[0])) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.not_a_number", args[0]));
                return false;
            }
            // Get the requested page index
            int page = Integer.parseInt(args[0]) - 1;
            // Check to make sure the index is valid
            if (page < 0 || page >= pagination.getPages().size()) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.out_of_range", 1, pagination.getPages().size(), args[0]));
                return false;
            }
            // Get the page
            currentPage = pagination.getPages().get(page);
        }
        // Get the current page
        else {
            currentPage = pagination.getCurrentPage();
        }

        pagination.printCurrentPage(sender, "commands.page.format");

        // Update the current page in the player's cached pagination
        pagination.setCurrentIndex(currentPage.getPageIndex());
        return true;
    }
}
