package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.SettingChangeEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.CollectionSetting;
import net.synergyserver.synergycore.settings.FixedOptionsSetting;
import net.synergyserver.synergycore.settings.Setting;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycore.settings.SettingOption;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@CommandDeclaration(
        commandName = "set",
        permission = "syn.settings.set",
        usage = "/settings set <setting> <value> [-global]",
        description = "Assigns a value to a setting.",
        minArgs = 2,
        maxArgs = 2,
        validSenders = SenderType.PLAYER,
        parseCommandFlags = true,
        parentCommandName = "settings"
)
public class SettingsSetCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        SettingManager sm = SettingManager.getInstance();

        // If the given argument is not a valid setting then give the player an error
        Setting setting = sm.getSettings().get(args[0].toLowerCase());
        if (setting == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.settings.error.setting_not_found", args[0]));
            return false;
        }

        // If they don't have access to change that setting, give the player an error
        if (!player.hasPermission(setting.getPermission())) {
            BukkitUtil.sendMessage(player, Message.format("commands.settings.error.setting_no_permission", setting.getIdentifier()));
            return false;
        }

        // Get their current settings to alter
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Parse the value from the argument given
        Object value = args[1];
        if (setting instanceof FixedOptionsSetting) {
            FixedOptionsSetting fixedOptionsSetting = (FixedOptionsSetting) setting;
            value = fixedOptionsSetting.parseOptionValue(args[1]);

            // Check that the provided value is valid
            if (value == null) {
                BukkitUtil.sendMessage(player, Message.format("commands.settings.error.value_not_found", args[1]));
                return false;
            }

            // Check that they have permission to use that value
            SettingOption option = fixedOptionsSetting.getOption(value);
            if (!player.hasPermission(option.getPermission())) {
                BukkitUtil.sendMessage(player, Message.format("commands.settings.error.value_no_permission", option.getIdentifier()));
                return false;
            }
        } else if (setting instanceof CollectionSetting) {
            CollectionSetting collectionSetting = (CollectionSetting) setting;
            Collection collection = collectionSetting.parseValue(args[1]);
            List<String> rejectedValues = new ArrayList<>();

            // If some elements were not accepted then give the player an error
            for (String element : args[1].split(",")) {
                if (!collection.contains(collectionSetting.stringToElement().apply(element))) {
                    rejectedValues.add(element);
                }
            }

            if (!rejectedValues.isEmpty()) {
                String rejectedValuesList = Message.createFormattedList(
                        rejectedValues,
                        Message.getColor("c5"),
                        Message.get("info_colored_lists.grammar_color")
                );
                BukkitUtil.sendMessage(player, Message.format("commands.settings.error.values_rejected", rejectedValues.size(), setting.getIdentifier(), rejectedValuesList));
                return false;
            }

            value = collection;
        }

        // Emit an event
        SettingChangeEvent event = new SettingChangeEvent(player, setting, setting.getValue(mcp), value);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            return true;
        }

        // Change the setting in their current preferences
        SettingPreferences settingPreferences = mcp.getCurrentWorldGroupProfile().getSettingPreferences();
        settingPreferences.setCurrentSetting(setting.getIdentifier(), value);

        // Update their GUI
        mcp.getCurrentWorldGroupProfile().createSettingGUI();

        // If the setting change was flagged to be global then change the setting in all world groups
        if (flags.hasFlag("-global", "-g")) {
            // Change the setting in their non-loaded preferences
            for (WorldGroupProfile wgp : mcp.getPartialWorldGroupProfiles("n", "s")) {
                // Don't modify the currently loaded WGP in this manner
                if (wgp.getWorldGroupName().equals(mcp.getCurrentWorldGroupName())) {
                    continue;
                }

                wgp.loadSettingPreferences();
                wgp.getSettingPreferences().setCurrentSetting(setting.getIdentifier(), value);
            }

            // Give the player feedback
            BukkitUtil.sendMessage(player, Message.format("commands.settings.set.info.setting_changed_global", setting.getIdentifier(), value));
        } else {
            // Give the player feedback
            BukkitUtil.sendMessage(player, Message.format("commands.settings.set.info.setting_changed", setting.getIdentifier(), value));
        }
        return true;
    }
}
