package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;

import java.util.UUID;

@CommandDeclaration(
        commandName = "set",
        permission = "syn.donator.subscription-status",
        usage = "/donator subscriptionstatus set <player> <true|false>",
        description = "Sets the donation subscription status of a player.",
        minArgs = 2,
        maxArgs = 2,
        parentCommandName = "donator subscriptionstatus"
)
public class DonatorSubscriptionStatusSetCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID donatorID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        // Give the sender an error if the name provided isn't valid
        if (donatorID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        boolean subscriptionStatus;

        // Get the argument provided as the new subscription status
        if (args[1].equalsIgnoreCase("true")) {
            subscriptionStatus = true;
        } else if (args[1].equalsIgnoreCase("false")) {
            subscriptionStatus = false;
        } else {
            // Give the sender an error
            BukkitUtil.sendMessage(sender, Message.format("commands.error.incorrect_syntax", getUsage()));
            return false;
        }

        // Get their current MinecraftProfile
        MinecraftProfile mcp;
        if (PlayerUtil.isOnline(donatorID)) {
            mcp = PlayerUtil.getProfile(donatorID);
        } else {
            mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, donatorID, "sid");
        }

        // Get the SynUser and set their subscription status
        SynUser synUser = DataManager.getInstance().getPartialDataEntity(SynUser.class, mcp.getSynID(), "sd");
        synUser.setSubscribedDonator(subscriptionStatus);

        // Give the sender feedback
        BukkitUtil.sendMessage(sender, Message.format("commands.donator.subscription_status.set.info.success", PlayerUtil.getName(donatorID), subscriptionStatus));

        return true;
    }
}
