package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "say",
        permission = "syn.say",
        usage = "/say <message>",
        description = "Prints a specified message to the chat",
        minArgs = 1,
        validSenders = {SenderType.CONSOLE, SenderType.PLAYER}
)

public class SayCommand extends MainCommand {
    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        String display;


        if (sender instanceof ConsoleCommandSender) {
            display = Message.get("server.name");
        } else {
            display = ((Player) sender).getDisplayName();
        }

        String message = String.join(" ", args);

        // Send to all online players
        for (Player p : Bukkit.getOnlinePlayers()) {
            BukkitUtil.sendMessage(p, Message.format("commands.say.format", display, message));
        }

        // Send to console for logging purposes
        BukkitUtil.sendMessage(Bukkit.getConsoleSender(), Message.format("commands.say.format", display, message));
        return true;
    }
}
