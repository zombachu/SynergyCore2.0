package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Warp;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "remove",
        aliases = "undeny",
        permission = "syn.warp.blacklist.remove",
        usage = "/warp blacklist remove <warp> <player>",
        description = "Grants back a player's access to a warp.",
        minArgs = 2,
        maxArgs = 2,
        validSenders = SenderType.PLAYER,
        parentCommandName = "warp blacklist"
)
public class WarpBlacklistRemoveCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID targetID = PlayerUtil.getUUID(args[1], true, player.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (targetID == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", args[1]));
            return false;
        }

        // Attempt to get the targeted warp
        String name = args[0];
        List<Warp> warps = Warp.getWarps(name, player.getUniqueId());

        // Return if an error was encountered
        if (warps == null) {
            return false;
        }

        // If they don't have a warp with the given name then give them an error
        if (warps.size() == 0) {
            BukkitUtil.sendMessage(player, Message.format("commands.warp.error.warp_not_found", name));
            return false;
        }

        Warp warp = warps.get(0);

        // Warn the player if the warp's privacy level is PRIVATE, which means the blacklist isn't currently active
        if (warp.getPrivacy().equals(Warp.PrivacyLevel.PRIVATE)) {
            BukkitUtil.sendMessage(player, Message.format("commands.warp.access_list.warning.not_active", warp.getName(), Warp.PrivacyLevel.PRIVATE.name().toLowerCase(), "blacklist"));
            return false;
        }

        // Attempt to unblacklist the targeted player
        boolean success = warp.removeBlacklistedPlayer(targetID);
        if (success) {
            // Give the player the success feedback
            BukkitUtil.sendMessage(player, Message.format("commands.warp.access_list.info.success", PlayerUtil.getName(targetID), "removed from", "blacklist", warp.getName()));
            return true;
        } else {
            // Since the player was unable to be removed, assume they weren't added and give the player an error
            BukkitUtil.sendMessage(player, Message.format("commands.warp.access_list.error.no_change", PlayerUtil.getName(targetID), "not added", "blacklist", warp.getName()));
            return true;
        }
    }
}
