package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Warp;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "remove",
        aliases = "uninvite",
        permission = "syn.warp.whitelist.remove",
        usage = "/warp whitelist remove <warp> <player>",
        description = "Revokes a player's access from a warp.",
        minArgs = 2,
        maxArgs = 2,
        validSenders = SenderType.PLAYER,
        parentCommandName = "warp whitelist"
)
public class WarpWhitelistRemoveCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID targetID = PlayerUtil.getUUID(args[1], true, player.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (targetID == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", args[1]));
            return false;
        }

        // Attempt to get the targeted warp
        String name = args[0];
        List<Warp> warps = Warp.getWarps(name, player.getUniqueId());

        // Return if an error was encountered
        if (warps == null) {
            return false;
        }

        // If they don't have a warp with the given name then give them an error
        if (warps.size() == 0) {
            BukkitUtil.sendMessage(player, Message.format("commands.warp.error.warp_not_found", name));
            return false;
        }

        Warp warp = warps.get(0);

        // Warn the player if the warp's privacy level is PUBLIC, which means the whitelist isn't currently active
        if (warp.getPrivacy().equals(Warp.PrivacyLevel.PUBLIC)) {
            BukkitUtil.sendMessage(player, Message.format("commands.warp.access_list.warning.not_active", warp.getName(), Warp.PrivacyLevel.PUBLIC.name().toLowerCase(), "whitelist"));
            return false;
        }

        // Attempt to unwhitelist the targeted player
        boolean success = warp.removeWhitelistedPlayer(targetID);
        if (success) {
            // Give the player the success feedback
            BukkitUtil.sendMessage(player, Message.format("commands.warp.access_list.info.success", PlayerUtil.getName(targetID), "removed from", "whitelist", warp.getName()));
            return true;
        } else {
            // Since the player was unable to be removed, assume they weren't added and give the player an error
            BukkitUtil.sendMessage(player, Message.format("commands.warp.access_list.error.no_change", PlayerUtil.getName(targetID), "not added", "whitelist", warp.getName()));
            return true;
        }
    }
}
