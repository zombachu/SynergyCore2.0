package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.TextConfig;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "faq",
        permission = "syn.faq",
        usage = "/faq",
        description = "Shows the FAQ of the server.",
        maxArgs = 0
)
public class FAQCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        for (String line : TextConfig.getText("faq.txt")) {
            BukkitUtil.sendMessage(sender, line);
        }
        return true;
    }
}
