package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Warp;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

@CommandDeclaration(
        commandName = "privacy",
        aliases = {"access"},
        permission = "syn.warp.privacy",
        usage = "/warp privacy <warp> <private|public>",
        description = "Changes the privacy of a warp. \"Private\" only allows whitelisted players to access the warp " +
                "while \"Public\" allows everyone but blacklisted players to access the warp.",
        minArgs = 2,
        maxArgs = 2,
        validSenders = SenderType.PLAYER,
        parentCommandName = "warp"
)
public class WarpPrivacyCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        // Attempt to parse the new privacy level
        Warp.PrivacyLevel privacyLevel;
        if (args[1].equalsIgnoreCase(Warp.PrivacyLevel.PRIVATE.name())) {
            privacyLevel = Warp.PrivacyLevel.PRIVATE;
        } else if (args[1].equalsIgnoreCase(Warp.PrivacyLevel.PUBLIC.name())) {
            privacyLevel = Warp.PrivacyLevel.PUBLIC;
        } else {
            // Give the player an error
            BukkitUtil.sendMessage(player, Message.format("commands.warp.privacy.error.invalid_privacy_type", args[1]));
            return false;
        }

        // Attempt to get the targeted warp
        String name = args[0];
        List<Warp> warps = Warp.getWarps(name, player.getUniqueId());

        // Return if an error was encountered
        if (warps == null) {
            return false;
        }

        // If they don't have a warp with the given name then give them an error
        if (warps.size() == 0) {
            BukkitUtil.sendMessage(player, Message.format("commands.warp.error.warp_not_found", name));
            return false;
        }

        Warp warp = warps.get(0);

        // Set the new privacy level and give the player feedback
        warp.setPrivacy(privacyLevel);
        String activeList = privacyLevel.equals(Warp.PrivacyLevel.PRIVATE) ? "whitelist" : "blacklist";
        BukkitUtil.sendMessage(player, Message.format("commands.warp.privacy.info.success", warp.getName(), privacyLevel.name().toLowerCase(), activeList));
        return true;
    }
}
