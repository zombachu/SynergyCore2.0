package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.PowerTool;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

@CommandDeclaration(
        commandName = "clear",
        aliases = "c",
        permission = "syn.powertool.clear",
        usage = "/powertool clear",
        description = "Removes all commands from the currently held PowerTool.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER,
        parentCommandName = "powertool"
)
public class PowerToolClearCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        ItemStack item = player.getInventory().getItemInMainHand();

        // Give an error if the player isn't holding an item
        if (item == null || item.getType().equals(Material.AIR)) {
            BukkitUtil.sendMessage(player, Message.get("commands.powertool.error.no_item"));
            return false;
        }

        String powerToolKey = ItemUtil.itemTypeToString(item);
        HashMap<String, PowerTool> powerTools = mcp.getPowerTools();

        // If the held item isn't a powertool then give them an error
        if (powerTools.get(powerToolKey) == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.powertool.error.not_a_powertool"));
            return false;
        }

        // Remove all powertool functionality and notify the player
        powerTools.remove(powerToolKey);
        mcp.setPowerTools(powerTools);
        BukkitUtil.sendMessage(player, Message.get("commands.powertool.clear.info.removed_all_commands"));
        return true;
    }
}
