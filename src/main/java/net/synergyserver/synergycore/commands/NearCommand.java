package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

@CommandDeclaration(
        commandName = "near",
        permission = "syn.near",
        usage = "/near [radius]",
        description = "Returns a list of nearby players.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class NearCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        int radius = 100;

        if (args.length == 1) {
            // Check if the argument is a valid number
            if (!MathUtil.isInteger(args[0])) {
                BukkitUtil.sendMessage(player, Message.format("commands.error.not_a_number", args[0]));
                return false;
            }

            radius = Integer.parseInt(args[0]);
            // If the number is negative then give the player an error
            if (radius < 0) {
                BukkitUtil.sendMessage(player, Message.format("commands.error.out_of_range", 0, "∞", radius));
                return false;
            }
        }

        List<String> playersInRange = new ArrayList<>();
        // Get players within the radius
        for (Player p : player.getWorld().getPlayers()) {
            // Ignore players the player can't see
            if (!PlayerUtil.canSee(player, p.getUniqueId())) {
                continue;
            }

            double distance = p.getLocation().distance(player.getLocation());

            // Add the player to the list if they're in the radius and not the command sender
            if (distance <= radius && !p.equals(player)) {
                playersInRange.add(Message.format("commands.near.format", p.getName(), distance));
            }
        }

        // Send the message to the player
        if (playersInRange.isEmpty()) {
            BukkitUtil.sendMessage(player, Message.format("commands.near.info.no_players", radius));
        } else {
            String listMessage = Message.createFormattedList(playersInRange, "", Message.get("info_colored_lists.grammar_color"));
            BukkitUtil.sendMessage(player, Message.format("commands.near.info.nearby_players", playersInRange.size(), radius, listMessage));
        }
        return true;
    }
}
