package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.UUID;

@CommandDeclaration(
        commandName = "add",
        aliases = "a",
        permission = "syn.socialspy.manage",
        usage = "/socialspy add <player|*>",
        description = "Starts spying on the messages of the given player.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "socialspy"
)
public class SocialSpyAddCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If the argument is * enable global socialspy
        if (args[0].equals("*")) {
            mcp.setGlobalSocialSpy(true);
            BukkitUtil.sendMessage(player, Message.format("commands.socialspy.info.global_switched", "enabled"));
            return true;
        }

        UUID targetID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        // Give an error if the player could not be found
        if (targetID == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        HashSet<UUID> spiedIDs = mcp.getSpiedPlayers();

        // Give an error if the player is already in the list
        if (spiedIDs.contains(targetID)) {
            BukkitUtil.sendMessage(player, Message.format("commands.socialspy.add.error.already_in_list", PlayerUtil.getName(targetID)));
            return false;
        }

        // Add the player to the list of spied players and give feedback
        spiedIDs.add(targetID);
        mcp.setSpiedPlayers(spiedIDs);
        BukkitUtil.sendMessage(player, Message.format("commands.socialspy.add.info.added_player", PlayerUtil.getName(targetID)));
        return true;
    }
}
