package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyPlugin;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginIdentifiableCommand;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Represents a <code>MainCommand</code>, which executes code whenever
 * its primary name or aliases is entered as a command in chat.
 */
public abstract class MainCommand extends Command implements SynCommand, PluginIdentifiableCommand {

    private SynergyPlugin plugin;
    private int minArgs;
    private int maxArgs;
    private List<SenderType> validSenders;
    private boolean parseCommandFlags;
    private boolean executeIfInvalidSubCommand;
    private HashMap<String, SubCommand> subCommands;

    /**
     * Creates a new <code>MainCommand</code> but does not populate any fields.
     * Call <code>populate()</code> after in order for this command to be fully functional.
     */
    public MainCommand() {
        super("");
        this.subCommands = new HashMap<>();
    }

    public void populate(SynergyPlugin plugin) {
        this.plugin = plugin;

        CommandDeclaration commandDeclaration = getClass().getAnnotation(CommandDeclaration.class);
        super.setName(commandDeclaration.commandName());
        super.setLabel(commandDeclaration.commandName());
        super.setAliases(Arrays.asList(commandDeclaration.aliases()));
        super.setPermission(commandDeclaration.permission());
        super.setUsage(commandDeclaration.usage());
        super.setDescription(commandDeclaration.description());
        this.minArgs = commandDeclaration.minArgs();
        this.maxArgs = commandDeclaration.maxArgs();
        this.validSenders = Arrays.asList(commandDeclaration.validSenders());
        this.parseCommandFlags = commandDeclaration.parseCommandFlags();
        this.executeIfInvalidSubCommand = commandDeclaration.executeIfInvalidSubCommand();
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public void registerSubCommand(SubCommand subCommand) {
        subCommands.put(subCommand.getCommandName(), subCommand);

        for (String alias : subCommand.getAliases()) {
            subCommands.put(alias, subCommand);
        }
    }

    public String getCommandName() {
        return super.getName();
    }

    public String getDescription() {
        return description;
    }

    public int getMinArgs() {
        return minArgs;
    }

    public int getMaxArgs() {
        return maxArgs;
    }

    public List<SenderType> getValidSenders() {
        return validSenders;
    }

    public boolean shouldParseCommandFlags() {
        return parseCommandFlags;
    }

    public boolean isExecutedIfInvalidSubCommand() {
        return executeIfInvalidSubCommand;
    }

    public HashMap<String, SubCommand> getSubCommands() {
        return subCommands;
    }

    public boolean execute(CommandSender sender, String label, String[] args) {
        return validate(sender, args);
    }

    public boolean validate(CommandSender sender, String[] args) {
        // SenderType check
        if (!validSenders.contains(SenderType.getSenderType(sender))) {
            BukkitUtil.sendMessage(sender, Message.get("commands.error.wrong_sender_type"));
            return false;
        }
        // Permission check
        if (!sender.hasPermission(getPermission())) {
            BukkitUtil.sendMessage(sender, Message.get("commands.error.no_permission"));
            return false;
        }

        // Parse and strip flags
        CommandFlags flags = null;
        String[] newArgs = args;

        if (parseCommandFlags) {
            flags = new CommandFlags(args);
            newArgs = CommandFlags.stripFlags(args);
        }

        // If this command is the parent of other commands then execute the code of those
        if (!subCommands.isEmpty()) {
            // Check if they entered in a valid SubCommand
            if (args.length > 0 && subCommands.containsKey(args[0])) {
                return subCommands.get(args[0]).validate(sender, (String[]) ArrayUtils.remove(args, 0));
            } else if (executeIfInvalidSubCommand) {
                // Check the number of arguments
                if (newArgs.length < minArgs || newArgs.length > maxArgs) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.error.incorrect_syntax", getUsage()));
                    return false;
                }

                // If the SubCommand was invalid but the MainCommand should still execute then do it
                return execute(sender, newArgs, flags);
            }

            // Otherwise give the sender an incorrect syntax error
            BukkitUtil.sendMessage(sender, Message.format("commands.error.incorrect_syntax", getUsage()));
            return false;
        }

        // Argument count check
        if (newArgs.length < minArgs || newArgs.length > maxArgs) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.incorrect_syntax", getUsage()));
            return false;
        }

        // Otherwise, execute the code in this command
        return execute(sender, newArgs, flags);
    }
}