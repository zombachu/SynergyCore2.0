package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "explode",
        permission = "syn.explode",
        usage = "/explode [player]",
        description = "aspi2!H(OPFHAsndixhry9s#O$asfjoahz.",
        maxArgs = 1
)
public class ExplodeCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player;

        if (args.length == 0) {
            // If the sender is not a player, then there needs to be a player specified
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                BukkitUtil.sendMessage(sender, Message.get("commands.error.sender_type_requires_player"));
                return false;
            }
            // Otherwise set the player to the command sender
            player = (Player) sender;
        } else {
            UUID pID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));

            // If no player was found then give the sender an error message
            if (pID == null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
                return false;
            }
            player = Bukkit.getPlayer(pID);

            // Check if the sender has permission to explode others
            if (!sender.hasPermission("syn.explode.others") && !player.equals(sender)) {
                BukkitUtil.sendMessage(sender, Message.get("commands.explode.error.no_permission_others"));
                return false;
            }
        }

        // Create the explosion
        Location loc = player.getLocation();
        loc.getWorld().createExplosion(loc.getX(), loc.getY() + 1, loc.getZ(), 0, false, false);

        // Kill the player
        PlayerUtil.killPlayer(player, "death_message.explode");
        return true;
    }
}
