package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Teleport;
import net.synergyserver.synergycore.TeleportType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.TeleportDirectEvent;
import net.synergyserver.synergycore.settings.CoreToggleSetting;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.UUID;

@CommandDeclaration(
        commandName = "tp",
        permission = "syn.tp",
        usage = "/tp <player>",
        description = "Teleports you to a player.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class TPCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID teleporteeID = PlayerUtil.getUUID(args[0], false, player.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (teleporteeID == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        Player teleportee = Bukkit.getPlayer(teleporteeID);

        // Emit an event
        Teleport teleport = new Teleport(player.getUniqueId(), teleporteeID,
                teleportee.getLocation(), TeleportType.TO_RECEIVER);
        TeleportDirectEvent event = new TeleportDirectEvent(teleport);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            return true;
        }

        // Teleport the player and set the TeleportCause for use with /back
        boolean success = player.teleport(teleportee, PlayerTeleportEvent.TeleportCause.COMMAND);
        if (success) {
            // Give both parties the appropriate feedback
            BukkitUtil.sendMessage(player, Message.format("commands.teleportation.info.teleporting_teleporter", teleportee.getName()));

            // Notify the teleportee if they can see the player and notify_tp_target is set to true by the player
            if (PlayerUtil.canSee(teleportee, player.getUniqueId()) && CoreToggleSetting.NOTIFY_TP_TARGET.getValue(PlayerUtil.getProfile(player))) {
                BukkitUtil.sendMessage(teleportee, Message.format("commands.teleportation.info.teleporting_teleportee", player.getName()));
            }
        }

        return true;
    }
}
