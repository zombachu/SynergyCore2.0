package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Mail;
import net.synergyserver.synergycore.MemberApplication;
import net.synergyserver.synergycore.PrivateMessageType;
import net.synergyserver.synergycore.StaffDashboard;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.MailSendEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "deny",
        permission = "syn.app.deny",
        usage = "/app deny <index> <reason>",
        description = "Denies the Member application with the given index and sends the applicant mail with the reason why.",
        minArgs = 2,
        parentCommandName = "app"
)
public class AppDenyCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // If the given index is not a number then give the sender a message
        if (!MathUtil.isInteger(args[0])) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.not_a_number", args[0]));
            return false;
        }

        StaffDashboard dashboard = StaffDashboard.getInstance();
        List<MemberApplication> applications = dashboard.getMemberApplications();
        int index = Integer.parseInt(args[0]) - 1;

        // If the given number is not a valid index then give the sender a message
        if (index < 0 || index >= applications.size()) {
            BukkitUtil.sendMessage(
                    sender,
                    Message.format("commands.error.out_of_range", 1, applications.size(), index + 1));
            return false;
        }

        MemberApplication application = applications.get(index);
        UUID pID = application.getPlayerID();
        String reason = String.join(" ", Arrays.copyOfRange(args, 1, args.length));

        // Deny the application
        dashboard.resolveMemberApplication(pID);

        // Give the sender feedback
        BukkitUtil.sendMessage(
                sender,
                Message.format("commands.application.info.deny_successful", PlayerUtil.getName(pID)));
        // If the player is online send them a message, otherwise send them mail
        if (PlayerUtil.isOnline(pID)) {
            BukkitUtil.sendMessage(
                    Bukkit.getPlayer(pID),
                    Message.format("commands.application.info.applicant_denied", reason));
        } else {
            // Craft the mail and emit an event
            String mailMessage = Message.format("mail.application_denied", reason);
            Mail mail = new Mail(SynergyCore.SERVER_ID, pID, System.currentTimeMillis(), mailMessage, PrivateMessageType.OFFICIAL);
            MailSendEvent mailEvent = new MailSendEvent(mail);
            Bukkit.getPluginManager().callEvent(mailEvent);

            // Check if the event was cancelled before continuing
            if (mailEvent.isCancelled()) {
                return true;
            }

            // Add the mail to the receiver's profile
            MinecraftProfile mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "ml");
            mcp.addMail(mail);
        }
        return true;
    }
}
