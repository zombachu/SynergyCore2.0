package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.guis.SettingGUI;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

@CommandDeclaration(
        commandName = "gui",
        permission = "syn.settings.gui",
        usage = "/settings gui",
        description = "Opens the GUI for managing your settings.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "settings"
)
public class SettingsGUICommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        SettingGUI settingGUI = PlayerUtil.getProfile(player).getCurrentWorldGroupProfile().getSettingGUI();
        // Open the GUI
        Inventory inv = settingGUI.getView().getInventory();
        if (inv != null) {
            player.openInventory(inv);
        }
        return true;
    }
}
