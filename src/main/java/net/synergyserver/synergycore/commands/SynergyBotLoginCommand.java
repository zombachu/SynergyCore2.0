package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "login",
        aliases = {"join", "start"},
        permission = "syn.synergybot.login",
        usage = "/synergybot login <all|discord|dubtrack>",
        description = "Logs in SynergyBot on the specified service.",
        minArgs = 1,
        maxArgs = 1,
        parentCommandName = "synergybot"
)
public class SynergyBotLoginCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        String service = args[0];

        // If all SynergyBots should be logged in
        if (service.equalsIgnoreCase("all")) {
            // Log into Discord
            SynergyCore.loginDiscordBot(success -> {
                if (success) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.login.info.success", "Discord"));
                } else {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.login.error.encountered_error", "Discord"));
                }
            });
            // Log into Dubtrack
            SynergyCore.loginDubtrackBot(success -> {
                if (success) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.login.info.success", "Dubtrack"));
                } else {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.login.error.encountered_error", "Dubtrack"));
                }
            });
            return true;
        }
        // If the Discord bot should be logged in
        else if (StringUtil.equalsIgnoreCase(service, "discord", "dis", "dc")) {
            SynergyCore.loginDiscordBot(success -> {
                if (success) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.login.info.success", "Discord"));
                } else {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.login.error.encountered_error", "Discord"));
                }
            });
            return true;
        }
        // If the Dubtrack bot should be logged in
        else if (StringUtil.equalsIgnoreCase(service, "dubtrack", "dub", "dt")) {
            SynergyCore.loginDubtrackBot(success -> {
                if (success) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.login.info.success", "Dubtrack"));
                } else {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.login.error.encountered_error", "Dubtrack"));
                }
            });
            return true;
        }
        // If they entered an invalid option
        else {
            BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.login.error.invalid_service", service));
            return false;
        }
    }
}
