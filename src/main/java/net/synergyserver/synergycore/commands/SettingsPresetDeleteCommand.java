package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.settings.SettingPreset;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedHashMap;

@CommandDeclaration(
        commandName = "delete",
        aliases = {"del", "remove"},
        permission = "syn.settings.preset.delete",
        usage = "/settings preset delete <name>",
        description = "Deletes a previously created preset.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "settings preset"
)
public class SettingsPresetDeleteCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
        SettingPreferences sp = wgp.getSettingPreferences();

        LinkedHashMap<String, SettingPreset> presets = sp.getPresets();
        String name = args[0].toLowerCase();

        // If they don't have a preset with the given name then give them an error
        if (presets == null || !presets.containsKey(name)) {
            BukkitUtil.sendMessage(player, Message.format("commands.settings.preset.delete.error.preset_not_found", name));
            return false;
        }

        // Remove the home and update the field in the database
        presets.remove(name);
        sp.setPresets(presets);

        // Give the player feedback and return true
        BukkitUtil.sendMessage(player, Message.format("commands.settings.preset.delete.info.deleted_preset", name));
        return true;
    }
}
