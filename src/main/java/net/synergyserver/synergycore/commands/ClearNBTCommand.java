package net.synergyserver.synergycore.commands;

import com.mojang.authlib.GameProfile;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ClientInformation;
import net.minecraft.server.level.EntityPlayer;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_20_R2.CraftServer;
import org.bukkit.craftbukkit.v1_20_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_20_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@CommandDeclaration(
        commandName = "clearnbt",
        permission = "syn.clearnbt",
        usage = "/clearnbt <player> [-all]",
        description = "Clears items with excessive NBT causing player to be NBT banned or all items if flagged.",
        minArgs = 1,
        maxArgs = 1,
        parseCommandFlags = true,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE}
)
public class ClearNBTCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        if (pID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(pID);

        Player player;

        if (offlinePlayer.isOnline()) {
            player = offlinePlayer.getPlayer();
        } else {
            player = loadPlayer(offlinePlayer);
        }
        if (player == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        int replacedCount = 0;
        ItemStack[] stacks = player.getInventory().getContents();
        if (flags.hasFlag("-a", "-all")) {
            replacedCount = stacks.length;
            player.getInventory().clear();
        } else {
            for (int x = 0; x < stacks.length; x++) {
                net.minecraft.world.item.ItemStack itemStack = CraftItemStack.asNMSCopy(stacks[x]);
                NBTTagCompound nbt = itemStack.v();
                if (nbt != null) {
                    int sizeInBytes = nbt.a();
                    if (sizeInBytes > 2097152) {
                        replacedCount++;
                        stacks[x] = new ItemStack(Material.AIR);
                    }
                }
            }
            player.getInventory().setContents(stacks);
        }

        player.saveData();

        if (replacedCount == 0) {
            BukkitUtil.sendMessage(sender, Message.format("commands.clearnbt.info.none", args[0]));
        } else {
            BukkitUtil.sendMessage(sender, Message.format("commands.clearnbt.info.count", replacedCount, args[0]));
        }
        return true;
    }

    public Player loadPlayer(@NotNull final OfflinePlayer offlinePlayer) {
        // Ensure player has data
        if (!offlinePlayer.hasPlayedBefore()) {
            return null;
        }

        // Create a profile and entity to load the player data
        // See net.minecraft.server.PlayerList#attemptLogin
        GameProfile profile = new GameProfile(offlinePlayer.getUniqueId(),
                offlinePlayer.getName() != null ? offlinePlayer.getName() : offlinePlayer.getUniqueId().toString());
        CraftWorld world = (CraftWorld) Bukkit.getWorld("world");

        if (world == null) {
            return null;
        }

        MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
        EntityPlayer entity = new EntityPlayer(server, world.getHandle(), profile, ClientInformation.a());

        // Get the bukkit entity
        Player target = entity.getBukkitEntity();
        if (target != null) {
            // Load data
            target.loadData();
        }
        // Return the entity
        return target;
    }
}