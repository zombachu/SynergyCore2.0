package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

@CommandDeclaration(
        commandName = "back",
        permission = "syn.back",
        usage = "/back",
        description = "Teleports you to your last location.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class BackCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        // Teleport the player and give them feedback
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        SerializableLocation serializableLocation = mcp.getPartialWorldGroupProfile(mcp.getLastWorldGroup(), "ll").getLastLocation();

        // If they don't have a last location then teleport them to their current position
        if (serializableLocation == null) {
            boolean success = player.teleport(player, PlayerTeleportEvent.TeleportCause.COMMAND);

            if (success) {
                BukkitUtil.sendMessage(player, Message.get("commands.teleportation.info.back"));
            }
            return true;
        }

        Location location = serializableLocation.getLocation();
        boolean success = player.teleport(location, PlayerTeleportEvent.TeleportCause.COMMAND);
        if (success) {
            BukkitUtil.sendMessage(player, Message.get("commands.teleportation.info.back"));
        }
        return true;
    }
}
