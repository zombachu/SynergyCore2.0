package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.DubtrackProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "list",
        aliases = "l",
        permission = "syn.connection.list",
        usage = "/connection list",
        description = "Lists the connected services of the sender.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER,
        parentCommandName = "connection"
)
public class ConnectionListCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        SynUser user = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());
        DiscordProfile discordProfile = user.getDiscordProfile();
        DubtrackProfile dubtrackProfile = user.getDubtrackProfile();

        if (discordProfile == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.connection.info.no_connection", "Discord"));
        } else {
            BukkitUtil.sendMessage(player, Message.format("commands.connection.list.connection", "Discord", discordProfile.getCurrentName()));
        }

        if (dubtrackProfile == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.connection.info.no_connection", "Dubtrack"));
        } else {
            BukkitUtil.sendMessage(player, Message.format("commands.connection.list.connection", "Dubtrack", dubtrackProfile.getCurrentName()));
        }

        return false;
    }
}
