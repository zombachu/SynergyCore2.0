package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "check",
        aliases = {"c", "get", "progress", "show"},
        permission = "syn.playtime.check",
        usage = "/playtime check [player] [-raw] [-worldgroup <worldgroup>]",
        description = "Checks the playtime of yourself or another player.",
        maxArgs = 1,
        parseCommandFlags = true,
        parentCommandName = "playtime"
)
public class PlaytimeCheckCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        boolean isSelf = false;
        UUID pID;
        MinecraftProfile mcp;

        if (args.length == 0) {
            // If args.length is 0 then that means that the sender is required to be a player checking their own playtime
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                BukkitUtil.sendMessage(sender, Message.get("commands.error.sender_type_requires_player"));
                return false;
            }

            isSelf = true;
            // Get the UUID of the sender
            pID = ((Player) sender).getUniqueId();
        } else {
            // Get the referenced player
            pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

            // If no player was found then give the sender an error message
            if (pID == null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
                return false;
            }
        }

        String pName = PlayerUtil.getName(pID);

        // First check flags to see if afktime should not be subtracted, or if it should be limited to one world group
        boolean isRaw = false;
        boolean singleWorldGroup = false;
        String worldGroupName = null;
        if (flags.hasFlag("-r", "-raw")) {
            isRaw = true;
        }
        if (flags.hasFlag("-w", "-world", "-wg", "-worldgroup")) {
            singleWorldGroup = true;
            worldGroupName = flags.getFlagValue("-w", "-world", "-wg", "-worldgroup");
        }

        // Check if the world group name provided is valid
        if (singleWorldGroup && worldGroupName.isEmpty()) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.invalid_world_group_name", ""));
            return false;
        }

        String correctedWorldGroupName = null;
        if (singleWorldGroup) {
            correctedWorldGroupName = SynergyCore.getExactWorldGroupName(worldGroupName);
        }
        if (singleWorldGroup && !SynergyCore.hasWorldGroupName(correctedWorldGroupName)) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.invalid_world_group_name", worldGroupName));
            return false;
        }


        if (PlayerUtil.isOnline(pID)) {
            mcp = PlayerUtil.getProfile(pID);
        } else {
            mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "wp", "tpt", "tat");
        }

        // Check if the player has data for the specified world group, if it was specified
        if (singleWorldGroup && !mcp.hasWorldGroupProfile(correctedWorldGroupName)) {
            // If the player specified is the sender, then give them a special message
            if (isSelf) {
                BukkitUtil.sendMessage(sender, Message.format("commands.info.world_group_no_data_self", correctedWorldGroupName));
                return true;
            }
            BukkitUtil.sendMessage(sender, Message.format("commands.info.world_group_no_data_others", pName, correctedWorldGroupName));
            return true;
        }

        long playtime;
        // If a world group was specified, get the playtime from there
        if (singleWorldGroup) {
            // If should return raw playtime value
            if (isRaw) {
                // Update and get playtime
                WorldGroupProfile wgp = mcp.getPartialWorldGroupProfile(correctedWorldGroupName, "li", "lo", "pt");
                mcp.updateTotalPlaytime();
                playtime = wgp.getPlaytime();
            } else {
                // Update and get playtime
                WorldGroupProfile wgp = mcp.getPartialWorldGroupProfile(correctedWorldGroupName, "li", "lo", "pt", "at");
                mcp.updateTotalPlaytime();
                mcp.updateTotalAfktime();
                playtime = wgp.getCalculatedPlaytime();
            }
        } else {
            // If should return raw playtime value
            if (isRaw) {
                mcp.updateTotalPlaytime();
                playtime = mcp.getTotalPlaytime();
            } else {
                mcp.updateTotalPlaytime();
                mcp.updateTotalAfktime();
                playtime = mcp.getTotalCalculatedPlaytime();
            }
        }

        // Make the message to replace the placeholder
        String playtimeMessage = Message.getTimeMessage(
                playtime, 3, 1,
                Message.get("info_colored_lists.item_color_1"),
                Message.get("info_colored_lists.grammar_color")
        );

        // Add modifiers to the message if needed
        String modifier = "";
        if (singleWorldGroup) {
            modifier += Message.format("commands.playtime.single_world_group_modifier", correctedWorldGroupName);
        }
        if (isRaw) {
            modifier += Message.format("commands.playtime.raw_modifier");
        }

        // If the player specified is the sender, then give them special feedback
        if (isSelf) {
            BukkitUtil.sendMessage(sender, Message.format("commands.playtime.check.info.self", playtimeMessage, modifier));
            return true;
        } else {
            BukkitUtil.sendMessage(sender, Message.format("commands.playtime.check.info.others", pName, playtimeMessage, modifier));
            return true;
        }
    }

}
