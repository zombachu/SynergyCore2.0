package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "logout",
        aliases = {"quit", "stop"},
        permission = "syn.synergybot.logout",
        usage = "/synergybot logout <all|discord|dubtrack>",
        description = "Logs out SynergyBot on the specified service.",
        minArgs = 1,
        maxArgs = 1,
        parentCommandName = "synergybot"
)
public class SynergyBotLogoutCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        String service = args[0];

        // If all SynergyBots should be logged out
        if (service.equalsIgnoreCase("all")) {
            // Log out of Discord
            SynergyCore.logoutDiscordBot(success -> {
                if (success) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.logout.info.success", "Discord"));
                } else {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.logout.error.encountered_error", "Discord"));
                }
            });

            // Log out of Dubtrack
            SynergyCore.logoutDubtrackBot(success -> {
                if (success) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.logout.info.success", "Dubtrack"));
                } else {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.logout.error.encountered_error", "Dubtrack"));
                }
            });
            return true;
        }
        // If the Discord bot should be logged out
        else if (StringUtil.equalsIgnoreCase(service, "discord", "dis", "dc")) {
            SynergyCore.logoutDiscordBot(success -> {
                if (success) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.logout.info.success", "Discord"));
                } else {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.logout.error.encountered_error", "Discord"));
                }
            });
            return true;
        }
        // If the Dubtrack bot should be logged out
        else if (StringUtil.equalsIgnoreCase(service, "dubtrack", "dub", "dt")) {
            SynergyCore.logoutDubtrackBot(success -> {
                if (success) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.logout.info.success", "Dubtrack"));
                } else {
                    BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.logout.error.encountered_error", "Dubtrack"));
                }
            });
            return true;
        }
        // If they entered an invalid option
        else {
            BukkitUtil.sendMessage(sender, Message.format("commands.synergybot.logout.error.invalid_service", service));
            return false;
        }
    }
}
