package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Page;
import net.synergyserver.synergycore.Pagination;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.Warp;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@CommandDeclaration(
        commandName = "list",
        aliases = "search",
        permission = "syn.warp.list",
        usage = "/warp list [page] [-name <name>] [-owner <owner>] [-worldgroup <worldgroup>]",
        description = "Lists all warps you have access to.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parseCommandFlags = true,
        parentCommandName = "warp"
)
public class WarpListCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID pID = player.getUniqueId();

        // Attempt to parse the page number, if given
        int page = 0;

        if (args.length == 1) {
            // If the given index is not a number then give the sender an error
            if (!MathUtil.isInteger(args[0])) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.not_a_number", args[0]));
                return false;
            }

            page = Integer.parseInt(args[0]) - 1;
        }

        // Get the flag to filter warp names by, if given
        String nameFilter = null;
        if (flags.hasFlag("-name", "-n", "-id")) {
            nameFilter = flags.getFlagValue("-name", "-n", "-id");

            // Give an error if no value was provided with the flag
            if (nameFilter.isEmpty()) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.missing_flag_value", flags.getFlag("-name", "-n", "-id")));
                return false;
            }
        }

        // Get the flag to filter owner names by, if given
        String ownerFilter = null;
        if (flags.hasFlag("-owner", "-o", "-player", "-p")) {
            ownerFilter = flags.getFlagValue("-owner", "-o", "-player", "-p");

            // Give an error if no value was provided with the flag
            if (ownerFilter.isEmpty()) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.missing_flag_value", flags.getFlag("-owner", "-o", "-player", "-p")));
                return false;
            }
        }

        // Get the flag to filter world groups by, if given
        String worldGroupFilter = null;
        if (flags.hasFlag("-w", "-wg", "-worldgroup")) {
            String worldGroupName = flags.getFlagValue("-w", "-wg", "-worldgroup");

            // Correct the capitalization for use with other methods
            worldGroupFilter = SynergyCore.getExactWorldGroupName(worldGroupName);;

            // Check to make sure the world group is valid
            if (!SynergyCore.hasWorldGroupName(worldGroupFilter)) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.invalid_world_group_name", worldGroupName));
                return false;
            }
        }
        String finalOwnerFilter = ownerFilter;
        String finalWorldGroupFilter = worldGroupFilter;


        // Find matching warps
        List<Warp> warps = Warp.getWarps(nameFilter, player.getUniqueId(), WarpCommand.WarpOwnerType::getListOrder, true, false);

        // Return if an error was encountered
        if (warps == null) {
            return false;
        }

        // Apply the remaining filters to the list of returned warps and group them by owner type
        Map<WarpCommand.WarpOwnerType, List<Warp>> filteredWarps = warps.stream()
                .filter(warp ->
                        // Keep the command if the owner matches the filter
                        finalOwnerFilter == null || StringUtil.containsIgnoreCase(PlayerUtil.getName(warp.getOwner()), finalOwnerFilter))
                .filter(warp ->
                        // Keep the command if the world group matches the filter
                        finalWorldGroupFilter == null || SynergyCore.getWorldGroupName(warp.getLocation().getWorld().getName()).equals(finalWorldGroupFilter))
                .collect(Collectors.groupingBy(
                        warp -> WarpCommand.WarpOwnerType.parseWarpOwnerType(warp, pID),
                        LinkedHashMap::new,
                        Collectors.toList()
                ));

        // Keep track of how many commands were found
        int count = 0;

        // Format the text
        List<String> text = new ArrayList<>();
        for (Map.Entry<WarpCommand.WarpOwnerType, List<Warp>> entry : filteredWarps.entrySet()) {
            // Build the section header
            if (!text.isEmpty()) {
                text.add(" ");
            }

            String ownerName = "";
            switch (entry.getKey()) {
                case SERVER:
                    ownerName = "Server";
                    break;
                case OWN:
                    ownerName = "Your";
                    break;
                case OTHER:
                    ownerName = "Other player's";
                    break;
                default: break;
            }
            text.add(Message.format("commands.warp.list.owner_section_header", ownerName));

            // Build warp information
            for (Warp warp : entry.getValue()) {
                String name = entry.getKey().equals(WarpCommand.WarpOwnerType.OTHER) ? warp.getFullName() : warp.getName();
                Location loc = warp.getLocation();
                text.add(Message.format("commands.warp.list.entry", name, loc.getX(), loc.getY(), loc.getZ(), loc.getWorld().getName()));

                // Increase the counter
                count++;
            }
        }

        // Build the modifiers
        String nameModifier = "";
        if (nameFilter != null) {
            nameModifier = Message.format("commands.warp.list.name_modifier", nameFilter);
        }
        String ownerModifier = "";
        if (ownerFilter != null) {
            ownerModifier = Message.format("commands.warp.list.owner_modifier", ownerFilter);
        }
        String worldGroupModifier = "";
        if (worldGroupFilter != null) {
            worldGroupModifier = Message.format("commands.warp.list.world_group_modifier", worldGroupFilter);
        }

        // If no commands were found then give the sender a special message
        if (count == 0) {
            BukkitUtil.sendMessage(sender, Message.format("commands.warp.list.info.none_found", nameModifier, ownerModifier, worldGroupModifier));
            return true;
        }

        // Build the pagination
        Pagination pagination = new Pagination(text, "Warps");

        // Update the player's current pagination
        PlayerUtil.getProfile(player).setCurrentPagination(pagination);

        // Check to make sure the index is valid
        if (page < 0 || page >= pagination.getPages().size()) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.out_of_range", 1, pagination.getPages().size(), args[0]));
            return false;
        }

        // Get the requested page from the pagination
        Page currentPage;
        if (args.length == 1) {
            currentPage = pagination.getPages().get(page);
        } else {
            currentPage = pagination.getCurrentPage();
        }

        // Send the summary if the page is the first
        if (page == 0) {
            BukkitUtil.sendMessage(sender, Message.format("commands.warp.list.info.summary", count, nameModifier, ownerModifier, worldGroupModifier));
        }

        pagination.printCurrentPage(player, "commands.page.format");

        // Update the current page in the player's cached pagination
        pagination.setCurrentIndex(currentPage.getPageIndex());
        return true;
    }
}
