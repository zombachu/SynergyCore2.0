package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "suicide",
        aliases = {"kms"},
        permission = "syn.suicide",
        usage = "/suicide",
        description = "kils urslef.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class SuicideCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        PlayerUtil.killPlayer((Player) sender, "death_message.suicide");
        return true;
    }
}
