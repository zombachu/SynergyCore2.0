package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.settings.SettingPreset;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedHashMap;

@CommandDeclaration(
        commandName = "list",
        permission = "syn.settings.preset.list",
        usage = "/settings preset list",
        description = "Displays all setting presets.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "settings preset"
)
public class SettingsPresetListCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
        SettingPreferences sp = wgp.getSettingPreferences();
        LinkedHashMap<String, SettingPreset> presets = sp.getPresets();

        // If they don't have any presets then give the player a special message
        if (presets == null || presets.size() == 0) {
            BukkitUtil.sendMessage(player, Message.get("commands.settings.preset.list.info.no_presets_found"));
            return true;
        }

        // Turn the names of their presets into a list and message the player
        String presetList = Message.createList(
                presets.keySet(),
                Message.get("info_colored_lists.item_color_1"),
                Message.get("info_colored_lists.item_color_2"),
                Message.get("info_colored_lists.grammar_color")
        );
        BukkitUtil.sendMessage(player, Message.format("commands.settings.preset.list.presets", presetList));
        return true;
    }
}
