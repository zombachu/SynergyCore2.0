package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "kickall",
        aliases = {"bopall", "ordersixtysix"},
        permission = "syn.kickall",
        usage = "/kickall [reason] [-includestaff]",
        description = "Kicks all players on the server, including staff if flagged.",
        parseCommandFlags = true,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE}
)

public class KickAllCommand extends MainCommand {
    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // Set reason to passed in reason, default is "Everyone was kicked from synergy"
        String reason;

        if (args.length > 0) {
            reason = String.join(" ", args);
        } else {
            reason = Message.get("commands.kickall.info.default_message");
        }

        boolean includeStaff = flags.hasFlag("-includestaff", "-staff", "-s");

        for (Player p : Bukkit.getOnlinePlayers()){
            if (!p.equals(sender) && (includeStaff || !p.hasPermission("syn.kickall.exempt"))) {
                p.kickPlayer(reason);
            }
        }

        BukkitUtil.sendMessage(sender, Message.get("commands.kickall.info.kicked_all"));
        return true;
    }
}
