package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.settings.CoreMultiOptionSetting;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "vanish",
        aliases = "v",
        permission = "syn.vanish",
        usage = "/vanish [on|off] [-silent|-fakemessage]",
        description = "Changes your vanish state.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parseCommandFlags = true
)
public class VanishCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Determine what the new vanished state of the player is
        boolean newVanishState = !PlayerUtil.isVanished(player);
        if (args.length == 1) {
            if (StringUtil.equalsIgnoreCase(args[0], "on", "leave", "hide")) {
                newVanishState = true;
            } else if (StringUtil.equalsIgnoreCase(args[0], "off", "join", "show")) {
                newVanishState = false;
            } else {
                // If no state could be matched, give the player an error
                BukkitUtil.sendMessage(player, Message.format("commands.vanish.error.invalid_state", args[0]));
                return false;
            }
        }

        // Determine whether or not the vanish should be silent
        String vanishBehavior = CoreMultiOptionSetting.VANISH_BEHAVIOR.getValue(mcp);
        boolean isSilent = true;
        if (flags.hasFlag("-s", "-silent")) {
            isSilent = true;
        } else if (flags.hasFlag("-f", "-m", "-fm", "-fake", "-loud", "-fakemessage")) {
            isSilent = false;
        } else if (vanishBehavior.equals("fake_messages")) {
            isSilent = false;
        } else if (vanishBehavior.equals("silent")) {
            isSilent = true;
        } else if (vanishBehavior.equals("legacy") && !newVanishState && !mcp.hasUnvanished()) {
            isSilent = false;
        }

        // Set the vanished state of the player
        PlayerUtil.setVanished(player, newVanishState, isSilent);
        return true;
    }
}
