package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Mail;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "clear",
        aliases = "c",
        permission = "syn.mail.clear",
        usage = "/mail clear [-p <player>]",
        description = "Clears messages from your mailbox.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER,
        parseCommandFlags = true,
        parentCommandName = "mail"
)
public class MailClearCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        List<Mail> mailbox = mcp.getMail();

        // Give a special message if there's no mail
        if (mailbox.isEmpty()) {
            BukkitUtil.sendMessage(player, Message.format("commands.mail.read.info.no_mail"));
            return true;
        }

        int initialSize = mailbox.size();
        UUID filteredPlayerID = null;

        // If a player was specified then only delete messages from them
        if (flags.hasFlag("-p", "-player")) {
            String filteredPlayer = flags.getFlagValue("-p", "-player");
            // Give an error if no player was provided
            if (filteredPlayer.isEmpty()) {
                BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", ""));
                return false;
            }

            filteredPlayerID = PlayerUtil.getUUID(filteredPlayer, true, sender.hasPermission("vanish.see"));

            // Give an error if no player was found
            if (filteredPlayerID == null) {
                BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", filteredPlayer));
                return false;
            }
        }

        // Update the player's saved mail and give the player feedback
        for (Mail mail : mailbox) {
            if (filteredPlayerID == null || mail.getSender().equals(filteredPlayerID)) {
                mcp.deleteMail(mail);
            }
        }

        BukkitUtil.sendMessage(player, Message.format("commands.mail.clear.info.success", initialSize - mcp.getMailIDs().size()));
        return true;
    }
}
