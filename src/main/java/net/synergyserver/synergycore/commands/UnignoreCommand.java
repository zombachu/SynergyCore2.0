package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "unignore",
        aliases = "unblock",
        permission = "syn.ignore",
        usage = "/unignore <player>",
        description = "Unignores the specified player, if they were previously ignored by you.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class UnignoreCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID toUnignore = PlayerUtil.getUUID(args[0], true, player.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (toUnignore == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        String toUnignoreName = PlayerUtil.getName(toUnignore);
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If they're not ignoring the player then give the sender an error message
        if (!mcp.hasIgnored(toUnignore)) {
            BukkitUtil.sendMessage(player, Message.format("commands.ignore.error.not_ignored", toUnignoreName));
            return false;
        }

        // Unignore the player and give feedback
        mcp.removeIgnoredPlayer(toUnignore);
        BukkitUtil.sendMessage(player, Message.format("commands.ignore.info.unignore_success", toUnignoreName));
        return true;
    }
}
