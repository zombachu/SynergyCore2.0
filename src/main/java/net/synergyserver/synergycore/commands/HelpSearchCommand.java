package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Page;
import net.synergyserver.synergycore.Pagination;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@CommandDeclaration(
        commandName = "search",
        aliases = {"filter", "s"},
        permission = "syn.help.search",
        usage = "/help search [page] [-c <command name>] [-p <plugin name>]",
        description = "Searches accessible commands.",
        maxArgs = 1,
        parseCommandFlags = true,
        parentCommandName = "help"
)
public class HelpSearchCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // Attempt to parse the page number, if given
        int page = 0;

        if (args.length == 1) {
            // If the given index is not a number then give the sender an error
            if (!MathUtil.isInteger(args[0])) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.not_a_number", args[0]));
                return false;
            }

            page = Integer.parseInt(args[0]) - 1;
        }

        CommandManager cm = CommandManager.getInstance();

        // Get the flag to filter the command owner by, if given
        String ownerFilter = null;
        if (flags.hasFlag("-plugin", "-p", "-owner")) {
            ownerFilter = flags.getFlagValue("-plugin", "-p", "-owner");

            // Give an error if no value was provided with the flag
            if (ownerFilter.isEmpty()) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.missing_flag_value", flags.getFlag("-plugin", "-p", "-owner")));
                return false;
            }

            // If the name provided matches a known constant then keep it, otherwise match it to the plugin list
            if (StringUtil.equalsIgnoreCase(ownerFilter, "bukkit")) {
                ownerFilter = "Bukkit";
            } else if (StringUtil.equalsIgnoreCase(ownerFilter, "spigot")) {
                ownerFilter = "Spigot";
            } else if (StringUtil.equalsIgnoreCase(ownerFilter, "mc", "minecraft")) {
                ownerFilter = "Minecraft";
            } else {
                Plugin plugin = BukkitUtil.parsePlugin(ownerFilter);

                // If no plugin was found then give the sender an error
                if (plugin == null) {
                    BukkitUtil.sendMessage(sender, Message.format("commands.help.search.error.plugin_not_found", ownerFilter));
                    return false;
                }
            }
        }

        // Get the flag to filter commands by, if given
        String commandFilter = null;
        if (flags.hasFlag("-command", "-c")) {
            commandFilter = flags.getFlagValue("-command", "-c");

            // Give an error if no value was provided with the flag
            if (commandFilter.isEmpty()) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.missing_flag_value", flags.getFlag("-command", "-c")));
                return false;
            }

            commandFilter = commandFilter.toLowerCase();
        }

        String finalOwnerFilter = ownerFilter;
        String finalCommandFilter = commandFilter;

        Map<String, Command> commands = cm.getRegisteredCommands();
        Map<String, Set<Command>> filteredCommands = commands.keySet()
                .stream()
                .filter(label ->
                        // Keep the command if the name of the command matches the command filter
                        // Because Bukkit also registers all aliases, this includes aliases and fallback labels too
                        finalCommandFilter == null || label.contains(finalCommandFilter)
                )
                .map(commands::get)
                .filter(Command::isRegistered)
                .filter(command ->
                        // Keep the command if the sender has permission to use it
                        command.getPermission() == null || sender.hasPermission(command.getPermission())
                )
                .filter(command ->
                        // Keep the command if the owner matches the plugin filter
                        finalOwnerFilter == null || StringUtil.containsIgnoreCase(cm.getOwnerName(command), finalOwnerFilter)
                )
                .filter(command ->
                        // Skip the command if's WorldEdit's fallback because WorldEdit registers commands twice
                        !command.getLabel().toLowerCase().startsWith("worldedit")
                )
                .sorted(Comparator
                        .comparing(HelpCommand.HelpOwnerType::parseHelpOwnerType)
                        .thenComparing(cm::getOwnerName)
                        .thenComparing(Command::getName, String.CASE_INSENSITIVE_ORDER))
                .collect(Collectors.groupingBy(
                        cm::getOwnerName,
                        LinkedHashMap::new,
                        Collectors.toCollection(LinkedHashSet::new)
                ));

        // Keep track of how many commands were found
        int count = 0;

        // Format the text
        List<String> text = new ArrayList<>();
        for (Map.Entry<String, Set<Command>> entry : filteredCommands.entrySet()) {
            // Build the section header
            if (!text.isEmpty()) {
                text.add(" ");
            }
            String ownerName = !entry.getKey().isEmpty() ? entry.getKey() : "Other";
            text.add(Message.format("commands.help.search.owner_section_header", ownerName));

            // Build command information
            for (Command command : entry.getValue()) {
                String mainUsage = HelpCommand.formatUsage(command);
                String mainDescription = command.getDescription();
                if (mainDescription == null || mainDescription.isEmpty()) {
                    text.add(Message.format("commands.help.search.entry_no_description", mainUsage));
                } else {
                    text.add(Message.format("commands.help.search.entry_description", mainUsage, mainDescription));
                }

                // Also include SubCommands if the command has any
                if (command instanceof MainCommand) {
                    HashMap<String, SubCommand> subCommands = ((MainCommand) command).getSubCommands();

                    for (SubCommand subCommand : new LinkedHashSet<>(subCommands.values())) {
                        String subUsage = HelpCommand.formatUsage(subCommand);
                        String subDescription = subCommand.getDescription();
                        if (subDescription == null || subDescription.isEmpty()) {
                            text.add(Message.format("commands.help.search.entry_no_description", subUsage));
                        } else {
                            text.add(Message.format("commands.help.search.entry_description", subUsage, subDescription));
                        }

                        // Increment the counter
                        count++;
                    }
                }

                // Increment the counter
                count++;
            }
        }

        // Build the modifiers
        String pluginModifier = "";
        if (ownerFilter != null) {
            pluginModifier = Message.format("commands.help.search.plugin_modifier", ownerFilter);
        }
        String nameModifier = "";
        if (commandFilter != null) {
            nameModifier = Message.format("commands.help.search.command_name_modifier", commandFilter);
        }

        // If no commands were found then give the sender a special message
        if (count == 0) {
            BukkitUtil.sendMessage(sender, Message.format("commands.help.search.info.none_found", pluginModifier, nameModifier));
            return true;
        }

        // Build the pagination
        Pagination pagination = new Pagination(text, "Help");

        // If the sender is a player then update their current pagination
        if (SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
            PlayerUtil.getProfile((Player) sender).setCurrentPagination(pagination);
        }

        // Check to make sure the index is valid
        if (page < 0 || page >= pagination.getPages().size()) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.out_of_range", 1, pagination.getPages().size(), args[0]));
            return false;
        }

        // Get the requested page from the pagination
        Page currentPage;
        if (args.length == 1) {
            currentPage = pagination.getPages().get(page);
        } else {
            currentPage = pagination.getCurrentPage();
        }

        // Send the summary if the page is the first
        if (page == 0) {
            BukkitUtil.sendMessage(sender, Message.format("commands.help.search.info.summary", count, pluginModifier, nameModifier));
        }

        pagination.printCurrentPage(sender, "commands.page.format");

        // Update the current page in the player's cached pagination
        pagination.setCurrentIndex(currentPage.getPageIndex());
        return true;
    }
}
