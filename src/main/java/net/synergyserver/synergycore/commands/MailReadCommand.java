package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Mail;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "read",
        aliases = "r",
        permission = "syn.mail.read",
        usage = "/mail read [-p <player>]",
        description = "Displays messages from your mailbox.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER,
        parseCommandFlags = true,
        parentCommandName = "mail"
)
public class MailReadCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        List<Mail> mailbox = mcp.getMail();

        // Give a special message if there's no mail
        if (mailbox.isEmpty()) {
            BukkitUtil.sendMessage(player, Message.format("commands.mail.read.info.no_mail"));
            return true;
        }

        UUID filteredPlayerID = null;

        // If a player was specified then only delete messages from them
        if (flags.hasFlag("-p", "-player")) {
            String filteredPlayer = flags.getFlagValue("-p", "-player");
            // Give an error if no player was provided
            if (filteredPlayer.isEmpty()) {
                BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", ""));
                return false;
            }

            filteredPlayerID = PlayerUtil.getUUID(filteredPlayer, true, sender.hasPermission("vanish.see"));

            // Give an error if no player was found
            if (filteredPlayerID == null) {
                BukkitUtil.sendMessage(player, Message.format("commands.error.player_not_found", filteredPlayer));
                return false;
            }
        }

        LinkedHashMap<Integer, Mail> filteredMail = new LinkedHashMap<>();

        for (int i = 0; i < mailbox.size(); i++) {
            Mail mail = mailbox.get(i);
            // Filter the mail if a player was specified
            if (filteredPlayerID != null && !mail.getSender().equals(filteredPlayerID)) {
                continue;
            }
            filteredMail.put(i, mail);
        }

        // Print the mail to the player
        if (filteredPlayerID == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.mail.read.header", filteredMail.size()));
        } else {
            BukkitUtil.sendMessage(player, Message.format("commands.mail.read.header_filtered", filteredMail.size(), PlayerUtil.getName(filteredPlayerID)));
        }
        for (int i : filteredMail.keySet()) {
            Mail mail = filteredMail.get(i);
            String timeMessage = Message.getTimeMessage(System.currentTimeMillis() - mail.getTimeSent(), 2, 1,
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.grammar_color")
            );
            BukkitUtil.sendMessage(player, Message.format("commands.mail.read.item", i + 1, mail.getSenderName(), timeMessage, mail.getMessage()));
        }
        return true;
    }
}
