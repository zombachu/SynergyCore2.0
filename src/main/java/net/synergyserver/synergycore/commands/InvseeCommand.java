package net.synergyserver.synergycore.commands;

import com.onarandombox.multiverseinventories.MultiverseInventories;
import com.onarandombox.multiverseinventories.profile.PlayerProfile;
import com.onarandombox.multiverseinventories.profile.ProfileTypes;
import com.onarandombox.multiverseinventories.profile.container.ProfileContainer;
import com.onarandombox.multiverseinventories.share.Sharables;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

@CommandDeclaration(
        commandName = "invsee",
        aliases = "inv",
        permission = "syn.invsee",
        usage = "/invsee [player] [-worldgroup <worldgroup>]",
        description = "Lets you see and edit another player's inventory.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parseCommandFlags = true
)
public class InvseeCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID targetID = player.getUniqueId();

        // If a player was specified then check that they are valid
        if (args.length == 1) {
            targetID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

            // If no player was found then give an error
            if (targetID == null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
                return false;
            }
        }

        String correctedWorldGroupName = null;
        if (flags.hasFlag("-w", "-world", "-wg", "-worldgroup")) {
            String worldGroupName = flags.getFlagValue("-w", "-world", "-wg", "-worldgroup");

            // If they did not provide a name after the flag then give an error
            if (worldGroupName.isEmpty()) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.invalid_world_group_name", ""));
                return false;
            }

            // Correct the capitalization of the world group name
            correctedWorldGroupName = SynergyCore.getExactWorldGroupName(worldGroupName);

            // Give an error if no world group could be found
            if (!SynergyCore.hasWorldGroupName(correctedWorldGroupName)) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.invalid_world_group_name", worldGroupName));
                return false;
            }
        }

        // Retrieve the target's Multiverse-Inventories data
        MultiverseInventories mvi = (MultiverseInventories) Bukkit.getPluginManager().getPlugin("Multiverse-Inventories");
        Player target = Bukkit.getPlayer(targetID);
        // Determine which world to use
        String worldName;
        if (target.isOnline()) {
            worldName = target.getWorld().getName();
        } else {
            worldName = player.getWorld().getName();
        }
        // Get the profile for the given world
        ProfileContainer worldProfile = mvi.getWorldProfileContainerStore().getContainer(worldName);
        PlayerProfile mviProfile = worldProfile.getPlayerData(ProfileTypes.SURVIVAL, Bukkit.getOfflinePlayer(targetID));

        // Getting an inventory
        ItemStack[] inventory = mviProfile.get(Sharables.INVENTORY);

        // Updating an inventory
        mviProfile.set(Sharables.INVENTORY, inventory);
        worldProfile.addPlayerData(mviProfile);

        return true;
    }
}
