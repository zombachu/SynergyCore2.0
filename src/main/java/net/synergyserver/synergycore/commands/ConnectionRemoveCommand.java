package net.synergyserver.synergycore.commands;

import io.sponges.dubtrack4j.DubtrackAPI;
import io.sponges.dubtrack4j.framework.Room;
import io.sponges.dubtrack4j.util.Role;
import net.synergyserver.synergycore.ServiceType;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.DiscordUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.IOException;

@CommandDeclaration(
        commandName = "remove",
        aliases = {"a", "d", "disconnect"},
        permission = "syn.connection.remove",
        usage = "/connection remove <discord|dubtrack>",
        description = "Allows the sender to disconnect from a supported service.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "connection"
)
public class ConnectionRemoveCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        YamlConfiguration config = PluginConfig.getConfig(SynergyCore.getPlugin());

        if (args.length == 0) {
            BukkitUtil.sendMessage(player, Message.format("commands.connection.error.undefined"));
            return false;
        }

        ServiceType serviceType = ServiceType.getServiceType(args[0]);

        if (serviceType == null) {
            BukkitUtil.sendMessage(player, Message.format("commands.connection.error.invalid", args[0]));
            return false;
        }

        SynUser synUser = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());

        if (serviceType.equals(ServiceType.DISCORD)) {
            try {
                String discordID = synUser.getDiscordID();

                if (discordID == null) {
                    BukkitUtil.sendMessage(player, Message.format("commands.connection.info.no_connection", "Discord"));
                    return false;
                }

                DiscordUtil.runForMember(synUser.getDiscordID(), (member, guild) -> {
                    if (!DiscordUtil.isStaff(member, guild)) {
                        guild.removeRoleFromMember(member, guild.getRolesByName("Player", true).get(0)).queue();
                        guild.modifyNickname(member, "").queue();
                    }
                });

                DataManager.getInstance().deleteDataEntity(synUser.getDiscordProfile());
                synUser.setDiscordID(null);
                BukkitUtil.sendMessage(player, Message.format("commands.connection.disconnect.info.success", "Discord"));
                return true;
            } catch (ServiceOfflineException e) {
                BukkitUtil.sendMessage(player, Message.format("commands.error.service_offline", "Discord"));
                return false;
            }
        } else if (serviceType.equals(ServiceType.DUBTRACK)) {
            try {
                DubtrackAPI dubClient = SynergyCore.getDubtrackClient();
                String dubtrackID = synUser.getDubtrackID();

                if (dubtrackID == null) {
                    BukkitUtil.sendMessage(player, Message.format("commands.connection.info.no_connection", "Dubtrack"));
                    return false;
                }

                try {
                    String roomID = config.getString("dubtrack.room");
                    Room room = dubClient.getRoom(roomID);
                    room.unsetRole(room.getUserById(dubtrackID), Role.RESIDENT_DJ);

                    DataManager.getInstance().deleteDataEntity(synUser.getDubtrackProfile());
                    synUser.setDubtrackID(null);
                    BukkitUtil.sendMessage(player, Message.format("commands.connection.disconnect.info.success", "Dubtrack"));
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            } catch (ServiceOfflineException e) {
                BukkitUtil.sendMessage(player, Message.format("commands.error.service_offline", "Dubtrack"));
                return false;
            }
        }

        return false;
    }
}
