package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.TextConfig;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "motd",
        permission = "syn.motd",
        usage = "/motd",
        description = "Shows the MOTD of the server, which is also visible on login.",
        maxArgs = 0
)
public class MOTDCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        for (String line : TextConfig.getText("motd.txt")) {
            BukkitUtil.sendMessage(sender, line.replaceAll("\\{username}", sender.getName()));
        }
        return true;
    }
}
