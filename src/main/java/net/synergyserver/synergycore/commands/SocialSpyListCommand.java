package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "list",
        aliases = "l",
        permission = "syn.socialspy.list",
        usage = "/socialspy list",
        description = "Lists all players being spied on.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER,
        parentCommandName = "socialspy"
)
public class SocialSpyListCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player.getUniqueId());

        // Give special feedback if global spying is enabled
        if (mcp.hasGlobalSpyingEnabled()) {
            BukkitUtil.sendMessage(player, Message.format("commands.socialspy.list.info.global_enabled"));
            return true;
        }

        // Notify the player if they aren't spying on anyone
        if (mcp.getSpiedPlayers().isEmpty()) {
            BukkitUtil.sendMessage(player, Message.format("commands.socialspy.list.info.no_spied_players"));
            return true;
        }

        // Create the list of spied players
        String spiedList = Message.createFormattedList(
                PlayerUtil.getNames(mcp.getSpiedPlayers()),
                Message.getColor("c4"),
                Message.get("info_colored_lists.grammar_color")
        );

        // Send the list to the user
        BukkitUtil.sendMessage(player, Message.format("commands.socialspy.list.info.spied_players", spiedList));
        return true;
    }
}
