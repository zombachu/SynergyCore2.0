package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;

import java.util.UUID;

@CommandDeclaration(
        commandName = "get",
        permission = "syn.donator.subscription-status",
        usage = "/donator subscriptionstatus get <player>",
        description = "Gets the donation subscription status of a player.",
        minArgs = 1,
        maxArgs = 1,
        parentCommandName = "donator subscriptionstatus"
)
public class DonatorSubscriptionStatusGetCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID donatorID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        // Give the sender an error if the name provided isn't valid
        if (donatorID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Get their current MinecraftProfile
        MinecraftProfile mcp;
        if (PlayerUtil.isOnline(donatorID)) {
            mcp = PlayerUtil.getProfile(donatorID);
        } else {
            mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, donatorID, "sid");
        }

        // Get the SynUser
        SynUser synUser = DataManager.getInstance().getPartialDataEntity(SynUser.class, mcp.getSynID(), "sd");

        // Give the sender the message
        if (synUser.isSubscribedDonator()) {
            BukkitUtil.sendMessage(sender, Message.format("commands.donator.subscription_status.get.info.is_subscribed", PlayerUtil.getName(donatorID)));
        } else {
            BukkitUtil.sendMessage(sender, Message.format("commands.donator.subscription_status.get.info.is_not_subscribed", PlayerUtil.getName(donatorID)));
        }

        return true;
    }
}
