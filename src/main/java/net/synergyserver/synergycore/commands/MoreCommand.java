package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@CommandDeclaration(
        commandName = "more",
        permission = "syn.more",
        usage = "/more",
        description = "Maxes out the stack size for the currently held item.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class MoreCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        ItemStack item = player.getInventory().getItemInMainHand();

        // Return an error if the player isn't holding an item
        if (item == null || item.getType().equals(Material.AIR)) {
            BukkitUtil.sendMessage(player, Message.get("commands.more.error.no_item"));
            return false;
        }

        // Set the amount to the max stack size for this item.
        item.setAmount(item.getMaxStackSize());
        BukkitUtil.sendMessage(player, Message.format("commands.more.info.item_stacked", item.getType()));
        return true;
    }
}
