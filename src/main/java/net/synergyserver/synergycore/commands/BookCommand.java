package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.BookMeta;

@CommandDeclaration(
        commandName = "book",
        permission = "syn.book",
        usage = "/book",
        description = "Makes the currently held written book able to be edited.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class BookCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        PlayerInventory inv = player.getInventory();
        ItemStack book = inv.getItemInMainHand();

        // Give an error if the player isn't holding a written book
        if (book == null || !book.getType().equals(Material.WRITTEN_BOOK)) {
            BukkitUtil.sendMessage(player, Message.format("commands.book.error.book_not_held"));
            return false;
        }

        BookMeta meta = (BookMeta) book.getItemMeta();

        // Give an error if they don't have permission to edit the book
        if (meta.getAuthor() != null && !meta.getAuthor().equalsIgnoreCase(player.getName())) {
            // If they have bypass perms then let the action go through with a warning
            if (player.hasPermission("syn.book.bypass")) {
                BukkitUtil.sendMessage(player, Message.format("commands.book.warning.bypassed_authorship"));
            } else {
                BukkitUtil.sendMessage(player, Message.format("commands.book.error.not_author"));
            }
        }

        // Create a book and quill with the existing book's metadata
        ItemStack newBook = new ItemStack(Material.WRITABLE_BOOK, book.getAmount());
        newBook.setItemMeta(meta);
        inv.setItemInMainHand(newBook);
        BukkitUtil.sendMessage(
                player,
                Message.format("commands.book.info.reopened_book", ((BookMeta) book.getItemMeta()).getAuthor()));
        return true;
    }
}
