package net.synergyserver.synergycore.commands;

import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Role;
import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.DiscordUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CommandDeclaration(
        commandName = "discord",
        aliases = "dis",
        permission = "syn.profile.discord",
        usage = "/profile discord [player]",
        description = "Gets the Discord profile for a player.",
        maxArgs = 1,
        parentCommandName = "profile"
)
public class ProfileDiscordCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        SynUser synUser;
        MinecraftProfile mcp;

        if (args.length == 0) {
            // If the sender isn't a player, then an argument is required
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                BukkitUtil.sendMessage(sender, Message.get("commands.error.wrong_sender_type"));
                return false;
            }

            mcp = PlayerUtil.getProfile(((Player) sender));
            synUser = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());
        } else {
            UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

            if (pID == null) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
                return false;
            }

            // Get the most current profile
            if (PlayerUtil.isOnline(pID)) {
                mcp = PlayerUtil.getProfile(pID);
            } else {
                mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "sid", "n");
            }

            synUser = DataManager.getInstance().getPartialDataEntity(SynUser.class, mcp.getSynID(), "d");
        }

        String color1 = Message.get("info_colored_lists.item_color_1");
        String grammarColor = Message.get("info_colored_lists.grammar_color");

        // Send the header
        BukkitUtil.sendMessage(sender, Message.format("commands.profile.info.header", mcp.getCurrentName(), "Discord"));

        DiscordProfile discordProfile = synUser.getDiscordProfile();
        if (discordProfile != null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.discord.detail.display_name", discordProfile.getCurrentName()));
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.discord.detail.id", discordProfile.getID()));

            try {
                DiscordUtil.runForMember(synUser.getDiscordID(), (member, guild) -> {
                    String onlineStatus = discordProfile.isOnline(member) ? ChatColor.GREEN + "Online" : ChatColor.DARK_RED + "Offline";
                    long time = discordProfile.isOnline(member) ? discordProfile.getLastLogIn() : discordProfile.getLastLogOut();

                    String onlineTimeMessage = Message.getTimeMessage(System.currentTimeMillis() - time, 3, 1, color1, grammarColor);

                    BukkitUtil.sendMessage(sender, Message.format("commands.profile.discord.detail.online_status", onlineStatus, onlineTimeMessage));

                    BukkitUtil.sendMessage(sender, Message.format("commands.profile.discord.detail.ban_status", discordProfile.isBanned() ? "Banned" : "Not banned"));

                    List<String> roles = member.getRoles().stream().map(Role::getName).collect(Collectors.toList());
                    String rolesMessage = Message.createFormattedList(roles, color1, grammarColor);
                    BukkitUtil.sendMessage(sender, Message.format("commands.profile.discord.detail.roles", rolesMessage));

                    GuildVoiceState voiceChannel = member.getVoiceState();
                    if (voiceChannel != null && voiceChannel.getChannel() != null) {
                        BukkitUtil.sendMessage(sender, Message.format("commands.profile.discord.detail.voice_channel", voiceChannel.getChannel().getName()));
                    }
                });
            } catch (ServiceOfflineException e) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.service_offline", "Discord"));
            }
        } else {
            BukkitUtil.sendMessage(sender, Message.format("commands.profile.error.service_not_connected", mcp.getCurrentName(), "Discord"));
            return false;
        }

        return true;
    }
}
