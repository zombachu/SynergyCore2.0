package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Mail;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

@CommandDeclaration(
        commandName = "delete",
        aliases = {"d", "remove", "r"},
        permission = "syn.mail.delete",
        usage = "/mail delete <index>",
        description = "Deletes a message from your mailbox.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "mail"
)
public class MailDeleteCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        List<Mail> mailbox = mcp.getMail();

        // Give an error if there is no mail to delete
        if (mailbox.isEmpty()) {
            BukkitUtil.sendMessage(player, Message.format("commands.mail.delete.error.no_mail"));
            return false;
        }

        if (args.length > 0) {
            // Give an error if the given index is not a number
            if (!MathUtil.isInteger(args[0])) {
                BukkitUtil.sendMessage(sender, Message.format("commands.error.not_a_number", args[0]));
                return false;
            }

            int index = Integer.parseInt(args[0]) - 1;

            // Return an error if the requested value is outside of the list
            if (index < 0 || index >= mailbox.size()) {
                BukkitUtil.sendMessage(player, Message.format("commands.error.out_of_range", 1, mailbox.size(), index + 1));
                return false;
            }

            // Delete the message and give the player feedback
            Mail deletedMail = mailbox.get(index);
            mcp.deleteMail(deletedMail);
            BukkitUtil.sendMessage(player, Message.format("commands.mail.delete.info.success", deletedMail.getMessage()));
            
            return true;
        } else {
            BukkitUtil.sendMessage(player, Message.format("commands.mail.delete.error.no_index"));
            return false;
        }
    }
}
