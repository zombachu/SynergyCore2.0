package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Teleport;
import net.synergyserver.synergycore.TeleportType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.TeleportRequestDenyEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "tpdeny",
        aliases = {"tprdeny", "tpno"},
        permission = "syn.tpdeny",
        usage = "/tpdeny",
        description = "Denies a pending teleport request.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class TPDenyCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If no pending teleport request was found then give the player an error
        if (!mcp.hasPendingTeleportRequest()) {
            BukkitUtil.sendMessage(player, Message.get("commands.teleportation.error.no_pending_requests"));
            return false;
        }

        Teleport teleport = mcp.getPendingTeleport();

        // Emit an event
        TeleportRequestDenyEvent event = new TeleportRequestDenyEvent(teleport);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            return true;
        }

        // Resolve the request
        mcp.resolvePendingTeleportRequest();

        // Give people feedback
        BukkitUtil.sendMessage(player, Message.format("commands.teleportation.info.request_denied_receiver",
                PlayerUtil.getName(teleport.getSender())));

        // If the sender isn't offline and the request isn't from /tprall, send them a sad message
        Player requestSender = Bukkit.getPlayer(teleport.getSender());
        if (requestSender != null && !teleport.getType().equals(TeleportType.EVERYONE_TO_SENDER)) {
            BukkitUtil.sendMessage(requestSender, Message.format("commands.teleportation.info.request_denied_sender", player.getName()));
        }

        return true;
    }
}
