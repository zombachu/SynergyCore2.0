package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;

import java.util.UUID;

@CommandDeclaration(
        commandName = "get",
        permission = "syn.donator.amount.get",
        usage = "/donator amount get <player>",
        description = "Gets the amount a player has donated to the server.",
        minArgs = 1,
        maxArgs = 1,
        parentCommandName = "donator amount"
)
public class DonatorAmountGetCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID donatorID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

        // Give the sender an error if the name provided isn't valid
        if (donatorID == null) {
            BukkitUtil.sendMessage(sender, Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Get their current MinecraftProfile
        MinecraftProfile mcp;
        if (PlayerUtil.isOnline(donatorID)) {
            mcp = PlayerUtil.getProfile(donatorID);
        } else {
            mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, donatorID, "sid");
        }

        // Get the SynUser
        SynUser synUser = DataManager.getInstance().getPartialDataEntity(SynUser.class, mcp.getSynID(), "ad");

        // Send a the corresponding message depending on whether or not they have donated
        if (synUser.hasDonated()) {
            double amountDonated = synUser.getAmountDonated() / 100D;
            BukkitUtil.sendMessage(sender, Message.format("commands.donator.amount.get.info.has_donated", PlayerUtil.getName(donatorID), amountDonated));
        } else {
            BukkitUtil.sendMessage(sender, Message.format("commands.donator.amount.get.info.has_not_donated", PlayerUtil.getName(donatorID)));
        }

        return true;
    }
}
