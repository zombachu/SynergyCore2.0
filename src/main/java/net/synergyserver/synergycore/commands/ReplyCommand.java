package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.PrivateMessage;
import net.synergyserver.synergycore.PrivateMessageType;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.PrivateMessageSendEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "r",
        aliases = "reply",
        permission = "syn.message",
        usage = "/r <message>",
        description = "Shorthand for replying to the player you last messaged.",
        minArgs = 1,
        validSenders = SenderType.PLAYER
)
public class ReplyCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile senderProfile = PlayerUtil.getProfile(player);

        // If they don't have a lastMessaged then give the player an error
        if (senderProfile.getLastMessaged() == null) {
            BukkitUtil.sendMessage(player, Message.get("commands.message.error.no_last_messaged"));
            return false;
        }

        UUID receiverID = senderProfile.getLastMessaged();
        // If the last messaged is currently offline, give the player an error
        if (!PlayerUtil.isOnline(receiverID)) {
            BukkitUtil.sendMessage(player, Message.get("commands.message.error.last_messaged_offline"));
            return false;
        }

        // Create the message
        UUID senderID = player.getUniqueId();
        String senderName = PlayerUtil.getNameColor(player) + player.getName();

        String receiverName;
        if (receiverID.equals(SynergyCore.SERVER_ID)) {
            receiverName = Message.get("server.prefix_color") + Message.get("server.name");
        } else {
            Player receiver = Bukkit.getPlayer(receiverID);
            receiverName = PlayerUtil.getNameColor(receiver) + receiver.getName();
        }

        // Build the message and format it
        String message = String.join(" ", args);
        message = Message.translateCodes(message, Message.get("commands.message.reset"), sender, "syn.message");

        // Emit an event
        PrivateMessage privateMessage = new PrivateMessage(senderID, receiverID, System.currentTimeMillis(), message, PrivateMessageType.NORMAL);
        PrivateMessageSendEvent event = new PrivateMessageSendEvent(privateMessage);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            return true;
        }

        // Send messages and update lastMessageds
        BukkitUtil.sendMessage(sender, Message.format("commands.message.sender", receiverName, message));
        senderProfile.setLastMessaged(receiverID);

        // Send the message to the receiver and if they're a player, update their lastMessaged
        if (receiverID.equals(SynergyCore.SERVER_ID)) {
            BukkitUtil.sendMessage(Bukkit.getConsoleSender(), Message.format("commands.message.receiver", senderName, message));
        } else {
            Player receiver = Bukkit.getPlayer(receiverID);
            BukkitUtil.sendMessage(receiver, Message.format("commands.message.receiver", senderName, message));
            PlayerUtil.getProfile(receiverID).setLastMessaged(senderID);
        }

        return true;
    }
}
