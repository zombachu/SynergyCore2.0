package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.settings.SettingPreset;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.StringUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.util.LinkedHashMap;
import java.util.Map;

@CommandDeclaration(
        commandName = "create",
        aliases = {"add", "set"},
        permission = "syn.settings.preset.create",
        usage = "/settings preset create <name>",
        description = "Creates a new setting preset with your currently set settings.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "settings preset"
)
public class SettingsPresetCreateCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
        SettingPreferences sp = wgp.getSettingPreferences();
        YamlConfiguration config = PluginConfig.getConfig(SynergyCore.getPlugin());

        // Get the max number of presets they can set in the current world group
        int max = config.getInt("preset.limit.default");

        if (player.hasPermission("syn.preset.limit.unlimited")) {
            max = Integer.MAX_VALUE;
        } else {
            Map<String, Object> limits = config.getConfigurationSection("preset.limit").getValues(false);
            for (String group : limits.keySet()) {
                if (player.hasPermission("syn.preset.limit." + group)) {
                    max = config.getInt("preset.limit." + group);
                }
            }
        }

        // Avoid a NPE
        if (sp.getPresets() == null) {
            sp.setPresets(new LinkedHashMap<>());
        }

        LinkedHashMap<String, SettingPreset> presets = sp.getPresets();

        // If the next preset will make them go over their limit then give them an error
        if (presets.size() + 1 > max) {
            BukkitUtil.sendMessage(player, Message.get("commands.settings.preset.create.error.too_many_presets"));
            return false;
        }

        String name = args[0].toLowerCase();

        // If the name given isn't safe to store then give them an error
        if (!StringUtil.isWord(name)) {
            BukkitUtil.sendMessage(player, Message.format("commands.settings.preset.create.error.unsafe_name"));
            return false;
        }

        // If they already have a home with the given name then give them an error
        if (presets.containsKey(name)) {
            BukkitUtil.sendMessage(player, Message.format("commands.settings.preset.error.name_already_exists", name));
            return false;
        }

        // Add the new home and update the field in the database
        presets.put(name, new SettingPreset(name, sp.getCurrentSettings()));
        sp.setPresets(presets);

        // Give the player feedback and return true
        BukkitUtil.sendMessage(player, Message.format("commands.settings.preset.create.info.created_preset", name));
        return true;
    }
}
