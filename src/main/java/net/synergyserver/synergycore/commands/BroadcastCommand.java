package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.BroadcastType;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.PlayerChatBroadcastEvent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "broadcast",
        aliases = "bc",
        permission = "syn.broadcast",
        usage = "/broadcast <message>",
        description = "Broadcasts a message without added formatting.",
        minArgs = 1
)
public class BroadcastCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        UUID broadcaster;
        String message = Message.translateCodes(String.join(" ", args));

        // Get the UUID of the sender
        if (SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
            broadcaster = ((Player) sender).getUniqueId();
        } else {
            broadcaster = SynergyCore.SERVER_ID;
        }

        // Emit an event
        PlayerChatBroadcastEvent event = new PlayerChatBroadcastEvent(BroadcastType.COMMAND, message, broadcaster);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before broadcasting
        if (!event.isCancelled()) {
            Bukkit.broadcastMessage(message);
        }
        return true;
    }
}
