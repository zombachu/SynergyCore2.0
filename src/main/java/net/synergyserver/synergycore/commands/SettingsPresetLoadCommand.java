package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.SettingChangeEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.Setting;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.settings.SettingPreset;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedHashMap;

@CommandDeclaration(
        commandName = "load",
        permission = "syn.settings.preset.load",
        usage = "/settings preset load <name>",
        description = "Loads a setting preset, overwriting your current settings.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "settings preset"
)
public class SettingsPresetLoadCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);
        WorldGroupProfile wgp = mcp.getCurrentWorldGroupProfile();
        SettingPreferences sp = wgp.getSettingPreferences();
        LinkedHashMap<String, SettingPreset> presets = sp.getPresets();
        String name = args[0].toLowerCase();

        // If they don't have a preset with that name then give the player a message
        if (presets == null || !presets.containsKey(name)) {
            BukkitUtil.sendMessage(player, Message.format("commands.settings.preset.load.error.preset_not_found", name));
            return false;
        }

        // Set each of the changed settings
        SettingPreset preset = presets.get(name);
        SettingManager sm = SettingManager.getInstance();
        for (String key : preset.getDiffs().keySet()) {
            Setting setting = sm.getSettings().get(key);
            Object oldValue = setting.getValue(mcp);
            Object newValue = setting.getValue(preset, player);

            SettingChangeEvent event = new SettingChangeEvent(player, setting, oldValue, newValue);
            Bukkit.getPluginManager().callEvent(event);

            // Check if the event was cancelled before continuing
            if (event.isCancelled()) {
                return true;
            }

            // Set the new value of the changed setting
            sp.setCurrentSetting(setting.getIdentifier(), newValue);
        }

        // Update the GUI
        wgp.createSettingGUI();

        // Give the player feedback
        BukkitUtil.sendMessage(player, Message.format("commands.settings.preset.load.info.success", name));
        return false;
    }
}
