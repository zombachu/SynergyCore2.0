package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Teleport;
import net.synergyserver.synergycore.TeleportType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.TeleportRequestAcceptEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.UUID;

@CommandDeclaration(
        commandName = "tpaccept",
        aliases = {"tpraccept", "tpyes"},
        permission = "syn.tpaccept",
        usage = "/tpaccept",
        description = "Accepts a pending teleport request.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class TPAcceptCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If no pending teleport request was found then give the player an error
        if (!mcp.hasPendingTeleportRequest()) {
            BukkitUtil.sendMessage(player, Message.get("commands.teleportation.error.no_pending_requests"));
            return false;
        }

        // If the sender has since logged off then give the player an error
        Teleport teleport = mcp.getPendingTeleport();
        UUID senderID = teleport.getSender();
        if (!PlayerUtil.canSee(player, senderID)) {
            String messageBit = "";
            switch (teleport.getType()) {
                case TO_RECEIVER:
                    messageBit = "teleported";
                    break;
                case TO_SENDER:
                case EVERYONE_TO_SENDER:
                    messageBit = "teleported to";
                    break;
                default:
                    break;
            }

            BukkitUtil.sendMessage(player, Message.format("commands.teleportation.error.sender_offline",
                    PlayerUtil.getName(senderID), messageBit));
            mcp.resolvePendingTeleportRequest();
            return false;
        }

        // Emit a event
        TeleportRequestAcceptEvent event = new TeleportRequestAcceptEvent(teleport);
        Bukkit.getPluginManager().callEvent(event);

        // Check if the event was cancelled before continuing
        if (event.isCancelled()) {
            // Even though we're exiting this method early, technically the command was still successful.
            return true;
        }

        // Teleport the teleporter to the location and resolve the request
        Player teleporter = Bukkit.getPlayer(teleport.getTeleporter());
        boolean success = teleporter.teleport(teleport.getTeleportLocation(), PlayerTeleportEvent.TeleportCause.COMMAND);
        if (success) {
            mcp.resolvePendingTeleportRequest();

            Player requestSender = Bukkit.getPlayer(teleport.getSender());

            // Give both parties the appropriate feedback
            BukkitUtil.sendMessage(player, Message.format("commands.teleportation.info.request_accepted_receiver", requestSender.getName()));
            // Don't spam the request sender on /tpaall
            if (!teleport.getType().equals(TeleportType.EVERYONE_TO_SENDER)) {
                BukkitUtil.sendMessage(requestSender, Message.format("commands.teleportation.info.request_accepted_sender", player.getName()));
            }

            // Send teleporting feedback
            Player teleportee = Bukkit.getPlayer(teleport.getTeleportee());
            BukkitUtil.sendMessage(teleporter, Message.format("commands.teleportation.info.teleporting_teleporter", teleportee.getName()));
            // Don't spam the request sender on /tpaall
            if (!teleport.getType().equals(TeleportType.EVERYONE_TO_SENDER)) {
                teleportee.sendMessage(Message.format("commands.teleportation.info.teleporting_teleportee", teleporter.getName()));
            }
        }

        return true;
    }
}
