package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.BroadcastType;
import org.bukkit.event.HandlerList;

import java.util.UUID;

/**
 * This event is called whenever a player initiates a broadcast.
 */
public class PlayerChatBroadcastEvent extends ChatBroadcastEvent {

    private UUID broadcaster;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>PlayerChatBroadcastEvent</code> with the given parameters.
     *
     * @param broadcastType The <code>BroadcastType</code> of this broadcast.
     * @param message The message of the broadcast.
     * @param broadcaster The UUID of the player who initiated the broadcast.
     */
    public PlayerChatBroadcastEvent(BroadcastType broadcastType, String message, UUID broadcaster) {
        super(broadcastType, message);
        this.broadcaster = broadcaster;
    }

    /**
     * Gets the UUID of the player who initiated the broadcast.
     *
     * @return The UUID of the broadcaster.
     */
    public UUID getBroadcaster() {
        return broadcaster;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
