package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.Teleport;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;

/**
 * Represents an event that involves a <code>Teleport</code>.
 */
public abstract class TeleportEvent extends Event implements Cancellable {

    private Teleport teleport;
    private boolean cancelled;

    /**
     * Creates a new <code>TeleportEvent</code> with the given <code>Teleport</code>.
     *
     * @param teleport The <code>Teleport</code> of this event.
     */
    public TeleportEvent(Teleport teleport) {
        this.teleport = teleport;
        this.cancelled = false;
    }

    /**
     * Gets the <code>Teleport</code> that was sent.
     *
     * @return This event's <code>Teleport</code>.
     */
    public Teleport getTeleport() {
        return teleport;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

}
