package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.profiles.MinecraftProfile;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called whenever a player's AFK status changes.
 */
public class AFKStatusChangeEvent extends Event {

    private MinecraftProfile player;
    private boolean newAFKStatus;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>AFKStatusChangeEvent</code> with the given parameters.
     *
     * @param isAsync Whether this event is asynchronous.
     * @param player The <code>MinecraftProfile</code> of the player in this event.
     * @param newAFKStatus True if the player became AFK.
     */
    public AFKStatusChangeEvent(boolean isAsync, MinecraftProfile player, boolean newAFKStatus) {
        super(isAsync);
        this.player = player;
        this.newAFKStatus = newAFKStatus;
    }

    /**
     * Gets the <code>MinecraftProfile</code> of the player of this event.
     *
     * @return The player's <code>MinecraftProfile</code>.
     */
    public MinecraftProfile getPlayer() {
        return player;
    }

    /**
     * Gets the new AFK status of the player of this event.
     *
     * @return The new AFK status.
     */
    public boolean getNewAFKStatus() {
        return newAFKStatus;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
