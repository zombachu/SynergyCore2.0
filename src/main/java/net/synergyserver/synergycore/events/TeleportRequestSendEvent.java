package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.Teleport;
import org.bukkit.event.HandlerList;

/**
 * This event is called when a new <code>Teleport</code> is sent to a player.
 */
public class TeleportRequestSendEvent extends TeleportRequestEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>TeleportRequestSendEvent</code> with the given <code>Teleport</code>.
     *
     * @param teleport The <code>Teleport</code> of this event.
     */
    public TeleportRequestSendEvent(Teleport teleport) {
        super (teleport);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
