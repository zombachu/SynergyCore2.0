package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.Teleport;
import org.bukkit.event.HandlerList;

/**
 * This event is called when a <code>Teleport</code> is accepted by a player.
 */
public class TeleportRequestAcceptEvent extends TeleportRequestEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>TeleportRequestAcceptEvent</code> with the given <code>Teleport</code>.
     *
     * @param teleport The <code>Teleport</code> of this event.
     */
    public TeleportRequestAcceptEvent(Teleport teleport) {
        super(teleport);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
