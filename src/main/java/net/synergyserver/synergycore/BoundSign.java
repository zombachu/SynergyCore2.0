package net.synergyserver.synergycore;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import net.synergyserver.synergycore.database.DataEntity;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Location;

/**
 * Represents a sign that has bound text that displays in chat when the sign is right-clicked.
 */
@Entity(value = "boundsigns")
public class BoundSign implements DataEntity {

    @Id
    private String signID;
    @Property("t")
    private String[] boundText;

    @Transient
    private DataManager dm = DataManager.getInstance();

    /**
     * Required constructor for Morphia to work.
     */
    public BoundSign() {}

    /**
     * Creates a new <code>BoundSign</code> with the given parameters.
     *
     * @param location The location of the sign.
     * @param boundText The text that is bound to it.
     */
    public BoundSign(Location location, String[] boundText) {
        this.signID = BukkitUtil.blockLocationToString(location);
        this.boundText = boundText;
    }

    @Override
    public String getID() {
        return signID;
    }

    /**
     * Gets the text that is bound to this sign.
     *
     * @return The bound text.
     */
    public String[] getBoundText() {
        return boundText;
    }

    /**
     * Sets the text that is bound to this sign.
     *
     * @param boundText The new bound text.
     */
    public void setBoundText(String[] boundText) {
        this.boundText = boundText;
        dm.updateField(this, BoundSign.class, "t", boundText);
    }
}
