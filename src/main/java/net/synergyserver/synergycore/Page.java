package net.synergyserver.synergycore;

import java.util.List;

/**
 * Represents a single page in a pagination.
 */
public class Page {

    private final String[] text;
    private final int pageIndex;
    private String header, footer;

    /**
     * Constructs a new page with a given set of data and a page number.
     *
     * @param text The text to make this page from.
     * @param pageIndex The index of the page in its <code>Pagination</code>.
     */
    public Page(List<String> text, int pageIndex) {
        this.text = text.toArray(new String[text.size()]);
        this.pageIndex = pageIndex;
    }

    /**
     * Gets the text of this page.
     *
     * @return An array where each line of text is an element.
     */
    public String[] getText() {
        return text;
    }

    /**
     * Gets this page's index in its <code>Pagination</code>.
     *
     * @return The index of this page.
     */
    public int getPageIndex() {
        return pageIndex;
    }

    /**
     * Gets the header for this page.
     *
     * @return The page header.
     */
    public String getHeader() {
        return header;
    }

    /**
     * Sets the header for this page.
     *
     * @param header The new page header.
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * Gets the footer for this page.
     *
     * @return The page footer.
     */
    public String getFooter() {
        return footer;
    }

    /**
     * Sets the footer for this page.
     *
     * @param footer The new page footer.
     */
    public void setFooter(String footer) {
        this.footer = footer;
    }
}
