package net.synergyserver.synergycore.database;

import dev.morphia.Datastore;
import dev.morphia.query.CountOptions;
import dev.morphia.query.FindOptions;
import dev.morphia.query.Query;
import dev.morphia.query.experimental.filters.Filters;
import dev.morphia.query.experimental.updates.UpdateOperators;
import net.synergyserver.synergycore.SynergyCore;
import org.bukkit.Bukkit;

import java.util.List;

/**
 * Represents a <code>DataManager</code>, which registers and retrieves data from the database.
 */
public class DataManager {

    private static DataManager instance;
    private Datastore ds;

    /**
     * Creates a new <code>DataManager</code> object.
     */
    private DataManager() {
        ds = MongoDB.getInstance().getDatastore();
    }

    /**
     * Returns the object representing this <code>DataManager</code>.
     *
     * @return The object of this class.
     */
    public static DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }

    /**
     * Gets a <code>DataEntity</code> by its ID.
     *
     * @param clazz The class of the <code>DataEntity</code> to get.
     * @param id The ID of the <code>DataEntity</code> to get.
     * @return The requested <code>DataEntity</code>, if found, or null if no documents matched in the database.
     */
    public <T extends DataEntity> T getDataEntity(Class<T> clazz, Object id) {
        return ds.find(clazz).filter(Filters.eq("_id", id)).first();
    }

    /**
     * Gets a list of <code>DataEntities</code> by their IDs.
     *
     * @param clazz The class of the <code>DataEntities</code> to get.
     * @param ids The IDs of the <code>DataEntities</code> to get.
     * @return The requested <code>DataEntities</code>, if found, or null if no documents matched in the database.
     */
    public <T extends DataEntity> List<T> getDataEntities(Class<T> clazz, Iterable<?> ids) {
        return ds.find(clazz).filter(Filters.in("_id", ids)).stream().toList();
    }

    /**
     * Gets a <code>DataEntity</code> by its ID and limits the fields retrieved to only those given.
     *
     * @param clazz The class of the <code>DataEntity</code> to get.
     * @param id The ID of the <code>DataEntity</code> to get.
     * @param fields The names of the fields to retrieve.
     * @return The requested <code>DataEntity</code>, if found, or null if no documents matched in the database.
     */
    public <T extends DataEntity> T getPartialDataEntity(Class<T> clazz, Object id, String... fields) {
        return ds.find(clazz).filter(Filters.eq("_id", id)).first(new FindOptions().projection().include(fields));
    }

    /**
     * Gets a list <code>DataEntities</code> by their IDs and limits the fields retrieved to only those given.
     *
     * @param clazz The class of the <code>DataEntities</code> to get.
     * @param ids The IDs of the <code>DataEntities</code> to get.
     * @param fields The names of the fields to retrieve.
     * @return The requested <code>DataEntities</code>, if found, or null if no documents matched in the database.
     */
    public <T extends DataEntity> List<T> getPartialDataEntities(Class<T> clazz, Iterable<?> ids, String... fields) {
        return ds.find(clazz).filter(Filters.in("_id", ids))
                .stream(new FindOptions().projection().include(fields)).toList();
    }

    /**
     * Saves a <code>DataEntity</code> to the database.
     *
     * @param dataEntity The <code>DataEntity</code> to save.
     */
    public void saveDataEntity(DataEntity dataEntity) {
        ds.save(dataEntity);
    }

    /**
     * Deletes a <code>DataEntity</code> from the database.
     *
     * @param dataEntity The <code>DataEntity</code> to delete.
     */
    public void deleteDataEntity(DataEntity dataEntity) {
        ds.delete(dataEntity);
    }

    /**
     * Checks if a <code>DataEntity</code> with the given ID is present in the database.
     *
     * @param clazz The class of the <code>DataEntity</code> to get.
     * @param id The ID to check.
     * @return True if a <code>DataEntity</code> was found that had the given ID.
     */
    public <T extends DataEntity> boolean isDataEntityInDatabase(Class<T> clazz, Object id) {
        return ds.find(clazz).filter(Filters.eq("_id", id)).count(new CountOptions().limit(1)) > 0;
    }

    /**
     * Updates a field of the <code>DataEntity</code> in the database.
     *
     * @param dataEntity The <code>DataEntity</code> to update the field of.
     * @param fieldName The name of the field to update.
     * @param value The value to set the field to.
     */
    public <T extends DataEntity> void updateField(T dataEntity, Class<T> clazz, String fieldName, Object value) {
        // TODO: Make thread safe
        Bukkit.getScheduler().runTaskAsynchronously(SynergyCore.getPlugin(), task -> {
            Query<T> query = ds.find(clazz).filter(Filters.eq("_id", dataEntity.getID()));

            if (value == null) {
                query.update(UpdateOperators.unset(fieldName)).execute();
            } else {
                query.update(UpdateOperators.set(fieldName, value)).execute();
            }
        });
    }
}
