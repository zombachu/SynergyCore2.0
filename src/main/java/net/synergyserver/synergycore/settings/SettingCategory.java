package net.synergyserver.synergycore.settings;

import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.guis.Itemizable;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a category of <code>Setting</code>s, grouped by their features. There should never be more than 7
 * categories or else the <code>SettingGUI</code> will not render it correctly.
 */
public enum SettingCategory implements Itemizable {

    CREATIVE("Creative Settings", "Settings that help out with the mundane things in redstoning and building.", Material.REDSTONE),
    SURVIVAL("Survival Settings", "Settings that act as morphine for the pain of survival.", Material.WHEAT_SEEDS),
    TELEPORTATION("Teleportation Settings", "Settings that alter the behavior of teleportation.", Material.ENDER_PEARL),
    CHAT("Chat Settings", "Settings that alter the look or behavior of your personal chat.", Material.OAK_SIGN),
    SECRET("Secret Settings", "Settings that only special people can access.", Material.STONE),
    OTHER("Miscellaneous Settings", "Settings that don't quite fit into the other categories.", Material.EGG);

    private String displayName;
    private String description;
    private ItemStack item;

    SettingCategory(String displayName, String description, Material itemType) {
        this.displayName = displayName;
        this.description = description;
        this.item = new ItemStack(itemType, 1);
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDescription() {
        return description;
    }

    /**
     * Gets the item of this <code>SettingCategory</code> to use in an item-based GUI.
     *
     * @return The item of this <code>SettingCategory</code>.
     */
    public ItemStack getItem() {
        return item.clone();
    }

    public ItemStack getItem(Object parameter) {
        ItemStack item = getItem();

        ItemMeta im = item.getItemMeta();
        List<String> lore = new ArrayList<>();

        im.setDisplayName(ChatColor.WHITE + getDisplayName());
        lore.add(ChatColor.GRAY + getDescription());

        im.setLore(lore);
        item.setItemMeta(im);
        return item;
    }
}
