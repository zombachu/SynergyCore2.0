package net.synergyserver.synergycore.settings;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import net.synergyserver.synergycore.database.DataEntity;
import net.synergyserver.synergycore.database.DataManager;
import org.bson.types.ObjectId;

import java.util.LinkedHashMap;

/**
 * Represents a collection of <code>SettingDiff</code>s that
 * includes a player's current settings and presets they have made.
 */
@Entity(value = "settingpreferences")
public class SettingPreferences implements DataEntity {

    @Id
    private ObjectId prefID;

    @Property("c")
    private SettingDiffs currentSettings;
    @Property(value = "p", concreteClass = java.util.LinkedHashMap.class)
    private LinkedHashMap<String, SettingPreset> presets;

    @Transient
    private DataManager dm = DataManager.getInstance();

    /**
     * Creates a new SettingPreferences.
     */
    public SettingPreferences() {
        this.prefID = new ObjectId();
        this.currentSettings = new SettingDiffs();
        this.presets = new LinkedHashMap<>();
    }

    @Override
    public ObjectId getID() {
        return prefID;
    }

    /**
     * Gets the <code>SettingDiffs</code> containing the current
     * <code>Setting</code>s for this <code>SettingPreferences</code>.
     *
     * @return The current <code>SettingDiffs</code>.
     */
    public SettingDiffs getCurrentSettings() {
        return currentSettings;
    }

    /**
     * Sets the <code>SettingDiffs</code> containing the current
     * <code>Setting</code>s for this <code>SettingPreferences</code>.
     *
     * @param currentSettings The new <code>SettingDiffs</code>.
     */
    public void setCurrentSettings(SettingDiffs currentSettings) {
        this.currentSettings = currentSettings;
        dm.updateField(this, SettingPreferences.class, "c", currentSettings);
    }

    /**
     * Convenience method for changing the value of this <code>SettingPreferences</code>' current settings.
     *
     * @param id The identifier of the setting.
     * @param value The changed value of the setting.
     */
    public void setCurrentSetting(String id, Object value) {
        currentSettings.setDiff(id, value);
        dm.updateField(this, SettingPreferences.class, "c", currentSettings);
    }

    /**
     * Gets the LinkedHashMap containing saved <code>SettingPreset</code>s for this <code>SettingPreferences</code>.
     *
     * @return The presets of this <code>SettingPreferences</code>.
     */
    public LinkedHashMap<String, SettingPreset> getPresets() {
        return presets;
    }

    /**
     * Sets the <code>SettingPreset</code>s for this <code>SettingPreferences</code>.
     *
     * @param presets The new LinkedHashMap of presets.
     */
    public void setPresets(LinkedHashMap<String, SettingPreset> presets) {
        this.presets = presets;
        dm.updateField(this, SettingPreferences.class, "p", presets);
    }
}
