package net.synergyserver.synergycore.settings;

import net.synergyserver.synergycore.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Represents a <code>Setting</code> that has a predefined list of valid options.
 */
public abstract class FixedOptionsSetting extends Setting {

    private Map<Object, SettingOption> options;

    /**
     * Creates a new <code>FixedOptionsSetting</code> with the given parameters.
     *
     * @param id The identifier of this setting.
     * @param category The category of this setting.
     * @param description The description of this setting.
     * @param permission The permission to use this setting.
     * @param defaultValue The default value of this setting.
     * @param defaultValueNoPermission The default value of this setting, if the user
     *                                 does not have permission to access this setting.
     */
    protected FixedOptionsSetting(String id, SettingCategory category, String description, String permission,
                                  Object defaultValue, Object defaultValueNoPermission) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission);
    }

    /**
     * Gets the Map the contains valid <code>SettingOption</code> for this <code>FixedOptionsSetting</code>.
     *
     * @return A Map containing <code>SettingOption</code>s representing valid options for this setting.
     */
    public Map<Object, SettingOption> getOptions() {
        return options;
    }

    /**
     * Sets the valid options for this <code>FixedOptionsSetting</code>. This method should
     * only be called once per setting, and while the setting is being constructed.
     *
     * @param options A Map containing <code>SettingOption</code>s representing valid options for this setting.
     */
    protected void setOptions(Map<Object, SettingOption> options) {
        this.options = options;
    }

    /**
     * Parses the value of a <code>SettingOption</code> that is stored in the HashMap of options as the key.
     * If no <code>SettingOption</code> could be matched, this returns null.
     *
     * @param toParse The object to parse the value from.
     * @return The value that is used as the key in the options map.
     */
    public abstract Object parseOptionValue(Object toParse);

    /**
     * Convenience method to get a <code>SettingOption</code> by its key.
     * Uses <code>parseOptionValue</code> to get the correct key.
     *
     * @param value The value to get the <code>SettingOption</code> of.
     * @return The <code>SettingOption</code>, if found, or null.
     */
    public SettingOption getOption(Object value) {
        return options.get(parseOptionValue(value));
    }

    public ItemStack getItem(Object value) {
        SettingOption option = getOptions().get(value);
        ItemStack item = option.getItem();

        ItemMeta im = item.getItemMeta();
        List<String> lore = new ArrayList<>();

        im.setDisplayName(ChatColor.WHITE + getDisplayName());
        lore.add(ChatColor.GRAY + getDescription());

        im.setLore(lore);
        item.setItemMeta(im);
        return item;
    }

    /**
     * Gets the value of the next option for this <code>MultiOptionSetting</code> or wraps
     * around to the first value if the current value is the last in the Map. Used primarily
     * by UIs that interact only by clicking with the mouse to cycle through options.
     *
     * @param currentValue The value to base the next value off of.
     * @return The value of the next valid <code>SettingOption</code>, or null if an invalid value was provided.
     */
    public Object getNextValue(Object currentValue) {
        if (!getOptions().containsKey(currentValue)) {
            return null;
        }

        List<Object> values = new ArrayList<>(getOptions().keySet());
        int currentIndex = values.indexOf(currentValue);
        if (currentIndex == values.size() - 1) {
            return values.get(0);
        }
        return values.get(currentIndex + 1);
    }
}