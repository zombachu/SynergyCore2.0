package net.synergyserver.synergycore.settings;

import net.synergyserver.synergycore.ChatColor;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permissible;

import java.util.HashMap;
import java.util.List;

/**
 * Represents a <code>FixedOptionsSetting</code> that only has an "on" and "off" state.
 */
public abstract class ToggleSetting extends FixedOptionsSetting {

    /**
     * Creates a new <code>ToggleSetting</code> with the given parameters. This should only be used by subclasses of
     * this class, and only by fields marked as public, static, and final.
     *
     * @param id The ID of this toggle, used to reference it in official documents.
     * @param category The category of this toggle, used to group settings together by their features.
     * @param description The description of what this toggle does.
     * @param permission The permission node required to use this toggle.
     * @param defaultValue The default value of this toggle.
     * @param defaultValueNoPermission The default value of this setting, if the user
     *                                 does not have permission to access this setting.
     * @param itemType The item to display in a GUI.
     */
    protected ToggleSetting(String id, SettingCategory category, String description, String permission,
                            boolean defaultValue, boolean defaultValueNoPermission, Material itemType) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission);

        // Create SettingOptions and put them in the HashMap
        HashMap<Object, SettingOption> options = new HashMap<>(2);
        options.put(false, new SettingOption("disabled", permission, "", itemType));
        options.put(true, new SettingOption("enabled", permission, "", itemType));
        setOptions(options);
    }

    @Override
    public Boolean getDefaultValue() {
        return (boolean) super.getDefaultValue();
    }

    @Override
    public Boolean getDefaultValueNoPermission() {
        return (boolean) super.getDefaultValueNoPermission();
    }

    @Override
    public Object parseOptionValue(Object toParse) {
        if (toParse instanceof String) {
            toParse = ((String) toParse).toLowerCase();
        }

        if (toParse.equals(true) || toParse.equals("true") || toParse.equals("enabled") || toParse.equals("on")) {
            return true;
        } else if (toParse.equals(false) || toParse.equals("false") || toParse.equals("disabled") || toParse.equals("off")) {
            return false;
        }
        return null;
    }

    @Override
    public ItemStack getItem(Object value) {
        ItemStack item = super.getItem(value);
        ItemMeta im = item.getItemMeta();
        List<String> lore = im.getLore();

        ChatColor valueColor;
        boolean valueBool = (boolean) value;
        if (valueBool) {
            valueColor = ChatColor.GREEN;
            im.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
        } else {
            valueColor = ChatColor.RED;
        }

        lore.add(ChatColor.GRAY + "Currently set to " + valueColor + value);

        im.setLore(lore);
        item.setItemMeta(im);
        return item;
    }

    @Override
    public Boolean getValue(MinecraftProfile minecraftProfile) {
        return (boolean) super.getValue(minecraftProfile);
    }

    @Override
    public Boolean getValue(SettingDiffs diffs, Permissible permissible) {
        return (boolean) super.getValue(diffs, permissible);
    }
}
