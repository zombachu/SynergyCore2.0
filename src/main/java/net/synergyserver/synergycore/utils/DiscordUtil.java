package net.synergyserver.synergycore.utils;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.synergyserver.synergycore.ServiceType;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.function.BiConsumer;

/**
 * Utility that handles Discord related things.
 */
public class DiscordUtil {

    /**
     * Checks if the user is staff in the specified guild.
     *
     * @param member The user to be checked.
     * @param guild The guild to check against.
     * @return True if the user is staff.
     */
    public static boolean isStaff(Member member, Guild guild) {
        return member != null && (member.getRoles().contains(guild.getRolesByName("Admin", true).get(0)) || member.getRoles().contains(guild.getRolesByName("Helper", true).get(0)));
    }

    /**
     * Sends a given message in a specified channel.
     *
     * @param channel The <code>IChannel</code> to send the message in.
     * @param message The <code>String</code> sent to the channel.
     */
    public static void sendMessage(TextChannel channel, String message) {
        channel.sendMessage(message).queue();
    }

    /**
     * Checks if the given user is online.
     *
     * @param member The member to check.
     * @return True if the user is online.
     */

    public static boolean isOnline(Member member) {
        return member != null && member.getOnlineStatus() == OnlineStatus.ONLINE;
    }

    /**
     * Runs a piece of code for a given member of the discord server.
     * Will always fetch the member.
     *
     * @param discordID discord member to run against.
     * @param consumer the code to run.
     */
    public static void runForMember(String discordID, BiConsumer<Member, Guild> consumer) {
        JDA disClient = SynergyCore.getDiscordClient();
        YamlConfiguration config = PluginConfig.getConfig(SynergyCore.getPlugin());
        Guild guild = disClient.getGuildById(config.getLong("discord.server"));

        if(guild == null) {
            throw new ServiceOfflineException(ServiceType.DISCORD);
        }
        guild.retrieveMemberById(discordID).queue(member -> {
            if(member != null) {
                consumer.accept(member, guild);
            }
        });
    }

    /**
     * Runs a piece of code for any discord user, not one necessarily in the discord server.
     * Will always fetch the user.
     *
     * @param discordID discord user to run against
     * @param consumer the code to run.
     */
    public static void runForUser(String discordID, BiConsumer<User, Guild> consumer) {
        JDA disClient = SynergyCore.getDiscordClient();
        YamlConfiguration config = PluginConfig.getConfig(SynergyCore.getPlugin());
        Guild guild = disClient.getGuildById(config.getLong("discord.server"));

        if(guild == null) {
            throw new ServiceOfflineException(ServiceType.DISCORD);
        }
        disClient.retrieveUserById(discordID).queue(user -> {
            if(user != null) {
                consumer.accept(user, guild);
            }
        });
    }
}
