package net.synergyserver.synergycore.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Utility that handles lists.
 */
public class ListUtil {

    /**
     * Turns a group of objects, given as parameters, into a mutable list of those objects.
     *
     * @param items A group of objects to turn into a list.
     * @return A list containing the given objects.
     */
    @SafeVarargs
    public static <T> List<T> makeList(T... items) {
        return new ArrayList<>(Arrays.asList(items));
    }

    /**
     * Checks if any of the given list's elements contain the given string. This method is case-insensitive.
     *
     * @param list The list of strings to check.
     * @param str The string to check against the list's elements.
     * @return True if one or more of the list's elements contains the given string.
     */
    public static boolean deepContains(List<String> list, String str) {
        String lowerStr = str.toLowerCase();
        for (String element : list) {
            if (element.toLowerCase().contains(lowerStr)) {
                return true;
            }
        }
        return false;
    }

    public static boolean containsIgnoreCase(Collection<String> list, String str) {
        return containsIgnoreCase(list.toArray(new String[list.size()]), str);
    }

    /**
     * Checks if the given array contains the given string, ignoring case.
     *
     * @param array The array of strings to look through.
     * @param str The string to search with.
     * @return True if a match was found.
     */
    public static boolean containsIgnoreCase(String[] array, String str) {
        for (String element : array) {
            if (str.equalsIgnoreCase(element)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the index of the given element in the given array, or -1 if the element was not found.
     *
     * @param array The array to check.
     * @param element The element to check for.
     * @return The index of the element in the array, or -1 if it wasn't found.
     */
    public static int indexOf(Object[] array, Object element) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }
}
