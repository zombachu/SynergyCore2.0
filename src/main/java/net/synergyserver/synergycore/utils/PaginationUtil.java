package net.synergyserver.synergycore.utils;

import java.util.List;

/**
 * Utility that handles pagination.
 */
public class PaginationUtil {

    /**
     * Gets a page out of a list of objects.
     *
     * @param list The list to create a page from.
     * @param pageIndex The index of the page, zero-indexed.
     * @param pageSize The number of entries per page.
     * @return The paged sublist.
     */
    public static <T> List<T> getPage(List<T> list, int pageIndex, int pageSize) {
        int startIndex = pageIndex * pageSize;
        int endIndex = startIndex + pageSize;

        if (endIndex > list.size()) {
            endIndex = list.size();
        }

        return list.subList(startIndex, endIndex);
    }

    /**
     * Gets the max number of pages possible with the given list and page size.
     *
     * @param list The list to check.
     * @param pageSize The number of entries per page.
     * @return The max number of pages.
     */
    public static <T> int getMaxPages(List<T> list, int pageSize) {
        return list.size() / pageSize + 1;
    }
}
