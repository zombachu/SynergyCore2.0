package net.synergyserver.synergycore.utils;

import net.minecraft.nbt.NBTTagCompound;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_20_R2.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Utility to assist with operations involving <code>ItemStack</code>s.
 */
public class ItemUtil {

    /**
     * Sets the display name of the given <code>ItemStack</code>.
     *
     * @param item The <code>ItemStack</code> to rename.
     * @param name The name to set.
     */
    public static void setName(ItemStack item, String name) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
    }

    /**
     * Sets a custom string of metadata on an <code>ItemStack</code>'s NBT data.
     *
     * @param item The <code>ItemStack</code> to give data to.
     * @param key The key of the data.
     * @param value The value of the data.
     * @return A copy of the <code>ItemStack</code> with the given key-value pair added.
     */
    public static ItemStack setNBTString(ItemStack item, String key, String value) {
        net.minecraft.world.item.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

        // NMS: If the item has NBT data already then get it, otherwise create a new NBTTagCompound
        NBTTagCompound nbtData;
        if (nmsItem.u()) { // hasTag
            nbtData = nmsItem.v(); // getTag
        } else {
            nbtData = new NBTTagCompound();
        }

        // Set the key-value pair
        nbtData.a(key, value); // setString
        nmsItem.c(nbtData); // setTag

        return CraftItemStack.asCraftMirror(nmsItem);
    }

    /**
     * Gets a previously set custom string on a <code>ItemStack</code>'s NBT data.
     *
     * @param item The <code>ItemStack</code> to retrieve data from.
     * @param key The key of the data.
     * @return The retrieved string if one was found, or an empty string.
     */
    public static String getNBTString(ItemStack item, String key) {
        net.minecraft.world.item.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

        // NMS: If the item has NBT data already then get it, otherwise create a new NBTTagCompound
        NBTTagCompound nbtData;
        if (nmsItem.u()) { // hasTag
            nbtData = nmsItem.v(); // getTag
        } else {
            nbtData = new NBTTagCompound();
        }

        // Get the value
        return nbtData.l(key); // getString
    }

    /**
     * Sets a custom int of metadata on an <code>ItemStack</code>'s NBT data.
     *
     * @param item The <code>ItemStack</code> to give data to.
     * @param key The key of the data.
     * @param value The value of the data.
     * @return A copy of the <code>ItemStack</code> with the given key-value pair added.
     */
    public static ItemStack setNBTInt(ItemStack item, String key, int value) {
        net.minecraft.world.item.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

        // NMS: If the item has NBT data already then get it, otherwise create a new NBTTagCompound
        NBTTagCompound nbtData;
        if (nmsItem.u()) { // hasTag
            nbtData = nmsItem.v(); // getTag
        } else {
            nbtData = new NBTTagCompound();
        }

        // Set the key-value pair
        nbtData.a(key, value); // setInt
        nmsItem.c(nbtData); // setTag

        return CraftItemStack.asCraftMirror(nmsItem);
    }

    /**
     * Gets a previously set custom int on a <code>ItemStack</code>'s NBT data.
     *
     * @param item The <code>ItemStack</code> to retrieve data from.
     * @param key The key of the data.
     * @return The retrieved int if one was found, or 0.
     */
    public static int getNBTInt(ItemStack item, String key) {
        net.minecraft.world.item.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

        // NMS: If the item has NBT data already then get it, otherwise create a new NBTTagCompound
        NBTTagCompound nbtData;
        if (nmsItem.t()) { // hasTag
            nbtData = nmsItem.v(); // getTag
        } else {
            nbtData = new NBTTagCompound();
        }

        // Get the value
        return nbtData.h(key); // getInt
    }

    /**
     * Sets a custom boolean of metadata on an <code>ItemStack</code>'s NBT data.
     *
     * @param item The <code>ItemStack</code> to give data to.
     * @param key The key of the data.
     * @param value The value of the data.
     * @return A copy of the <code>ItemStack</code> with the given key-value pair added.
     */
    public static ItemStack setNBTBoolean(ItemStack item, String key, boolean value) {
        net.minecraft.world.item.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

        // NMS: If the item has NBT data already then get it, otherwise create a new NBTTagCompound
        NBTTagCompound nbtData;
        if (nmsItem.t()) { // hasTag
            nbtData = nmsItem.v(); // getTag
        } else {
            nbtData = new NBTTagCompound();
        }

        // Set the key-value pair
        nbtData.a(key, value); //setBoolean
        nmsItem.c(nbtData); //setTag

        return CraftItemStack.asCraftMirror(nmsItem);
    }

    /**
     * Gets a previously set custom boolean on a <code>ItemStack</code>'s NBT data.
     *
     * @param item The <code>ItemStack</code> to retrieve data from.
     * @param key The key of the data.
     * @return The retrieved boolean if one was found, or 0.
     */
    public static boolean getNBTBoolean(ItemStack item, String key) {
        net.minecraft.world.item.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

        // NMS: If the item has NBT data already then get it, otherwise create a new NBTTagCompound
        NBTTagCompound nbtData;
        if (nmsItem.t()) { // hasTag
            nbtData = nmsItem.v(); // getTag
        } else {
            nbtData = new NBTTagCompound();
        }

        // Get the value
        return nbtData.q(key); // getBoolean
    }

    /**
     * Gets the given <code>ItemStack</code>'s type as a string,
     * based on its<code>Material</code> and damage value.
     *
     * @param item The <code>ItemStack</code> to get the type of.
     * @return The <code>ItemStack</code>'s type.
     */
    public static String itemTypeToString(ItemStack item) {
        return item.getType().name().toLowerCase() + ":" + item.getDurability();
    }

    /**
     * Checks if the given <code>Material</code> is a type of air.
     *
     * @param mat The material to check.
     * @return True if the material is air.
     */
    public static boolean isAir(Material mat) {
        return mat.equals(Material.AIR) || mat.equals(Material.CAVE_AIR) || mat.equals(Material.VOID_AIR);
    }

}
